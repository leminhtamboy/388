<?php
include_once("Article.php");
include_once("Menu.php");
include_once("Attribute.php");
include_once("AttributeValue.php");
include_once("Config.php");
$idEntity=$_REQUEST["id"];
$menu  = new Menu("menu","id");
$config= new ConfigGlobal("config","config_id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$article = new Article("article","id_article");
$attributeValue= new AttributeValue("entity_attribute_value_varchar","id_eav_varchar");
$dataArticle=$article->load($idEntity);
?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<?php include_once ("head.php")?>
<body>
<div class="container">
    <?php include_once("header.php"); ?>
    <div class="page-path row">
        <a href=""><?php echo $dataArticle->gettitle(); ?></a>
    </div>
    <div id="content" class="row">
        <div class="page-title">
            <h2><?php echo $dataArticle->gettitle(); ?></h2>
        </div>
        <div class="row class-info two-column-content" id="tt-event-detail-page">
            <div class="col-md-12 col-sm-12 full-info">
                <div class="">
                    <div class="product-img-box">
                        <p class="product-image">

                        </p><div class="flexslider contentSlider">
                            <ul class="slides">
                                <li class="background-image flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2; background-image: url(&quot;<?php echo $dataArticle->getimage(); ?>&quot;);">
                                    <div class="slide-content">
                                    </div>
                                </li>
                            </ul>
                            <ol class="flex-control-nav flex-control-paging"></ol></div>

                        <p></p>
                    </div>
                    <div class="description row">
                        <div class="col-sm-12">
                                <?php $shortDescription=$attributeValue->getValueAttribuetOfEntityByAttributeCode($idEntity,"short_description")[0]->getvalue();  ?>
                            <p><?php echo $shortDescription ?></p>
                                <?php $longDescription=$attributeValue->getValueAttribuetOfEntityByAttributeCode($idEntity,"long_description")[0]->getvalue();  ?>
                            <p><?php echo $longDescription ?></p>
                            <div class="testimonials">
                            </div>
                        </div>
                        <!--<div class="col-sm-4">
                            <div class="course-info">
                                <h3>Giảng viên của khóa học </h3>
                                <img class="img-responsive instructor-img" src="http://toatau.com/wp-content/uploads/2016/06/12243288_10204472897937170_8640673562266066659_n-640x640.jpg">
                                <b>Thùy An</b>
                                <p>Thường gọi là Chim Xanh. Cô đã theo học ngành Thiết Kế Thời Trang trước khi trở thành họa sĩ vẽ tranh minh họa và giảng viên lớp Khám Phá Hình Ảnh tại Toa Tàu. Muốn biết cô Chim Xanh đã lấy lòng trẻ nhỏ bằng tranh màu nước như thế nào, mời bạn xem tranh cô đã vẽ cho tác phẩm Làng quan họ của nhà xuất bản Kim Đồng tại www.behance.net/gallery/31346305/Lang-quan-h</p>
                                <br>


                                <h3>Thời gian</h3>
                                <table class="table no-border table-condensed">
                                    <tbody><tr>
                                        <td>Ngày bắt đầu</td>
                                        <td>05/07/2016</td>
                                    </tr>
                                    <tr>
                                        <td>Ngày kết thúc</td>
                                        <td>28/07/2016</td>
                                    </tr>
                                    <tr>
                                        <td>Thời lượng</td>
                                        <td>8 buổi</td>
                                    </tr>
                                    <tr>
                                        <td>Thời gian</td>
                                        <td><p>Từ 9:00 – 11:00</p>
                                            <p>Sáng thứ 3 và sáng thứ 5</p>
                                        </td>
                                    </tr>
                                    </tbody></table>

                                <h3>Địa điểm</h3>

                                <p></p><p>Toa Tàu</p>
                                <p>632 Điện Biên Phủ, Bình Thạnh</p>
                                <p>Hotline: 0917-961-071</p>
                                <p>Email: info@toatau.com</p>
                                <p></p>
                            </div>
                        </div>-->
                    </div>

                </div>
            </div>
            <!--<div class="col-md-3 col-sm-12 right-info">
                <h3 class="header">Thông tin đăng ký</h3>

                <div class="register-info">
                    <h3 class="fee">2,000,000 VNĐ </h3>
                    <p><b>Ngày bắt đầu: </b> 05/07/2016</p>

                    <p><b>Thời lượng: </b> 8 buổi</p>

                    <p class="solid-line"></p>

                    <p>Hiện đăng ký đã đóng. <br><br>Bạn vui lòng liên hệ trực tiếp với <a href="tel://091-7961-071"> Hotline Toa Tàu: 091-7961-071                                </a> để được tư vấn thêm. Cảm ơn bạn.</p>
                </div>
            </div>-->
        </div>
        <div class="row">
            <div class="product-collateral">
                <h3>Các Bài Viết Khác</h3>
                <ul class="you-might-also-like">
                    <?php
                    $currentCategoryOfCurrentArticle=$dataArticle->gethas_category();
                    $dataRelatedArticle=$article->loadByAttribute("has_category","eq",$currentCategoryOfCurrentArticle,0);
                    ?>
                    <?php foreach($dataRelatedArticle as $relaArticle){ ?>
                        <?php $linkSeo=$attributeValue->getValueAttribuetOfEntityByAttributeCode($relaArticle->getid_article(),"link_seo")[0]->getvalue();  ?>
                    <li class="item">
                        <a href="/article/<?php echo $linkSeo ?>-<?php echo  $relaArticle->getid_article(); ?>.html" class="product-image">
                            <img class="img-responsive" src="<?php echo $relaArticle->getimage(); ?>" alt="Chuyện Của Bố Cục (15) – 11/7">
                        </a>

                        <div class="product-name">
                            <a href="/article/<?php echo $linkSeo ?>-<?php echo  $relaArticle->getid_article(); ?>.html">
                                <span><?php echo $relaArticle->gettitle(); ?></span>
                            </a>
                        </div>
                        <div class="actions">
                            <a href="/article/<?php echo $linkSeo ?>-<?php echo  $relaArticle->getid_article(); ?>.html">Xem thêm</a>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <?php include_once("footer.php"); ?>
</div>
</body>
</html>
