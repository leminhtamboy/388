<?php
    $xanhColor = "#228b22";
?>
<head>
    <title>Web chơi cá cược bóng đá, cá độ thể thao online số 1 Châu Á</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="vi" />
    <link rel="canonical" href="<?php echo ConfigGlobal::$realPath; ?>" />
    <link rel="shortcut icon" href="<?php echo ConfigGlobal::$realPath; ?>/image/<?php echo strip_tags($logo->getvalue()); ?>" type="image/x-icon">
    <meta name="title" content="Web chơi cá cược bóng đá, cá độ thể thao online số 1 Châu Á" />
    <meta name="description" content="88Cuoc là website chơi cá độ bóng đá trực tuyến, cá cược thể thao trên mạng hợp pháp và uy tín. Cung cấp tỷ lệ kèo cá cược bóng đá hôm nay FREE" />
    <meta name="keywords" content="ca cuoc bong da, ca cuoc the thao, ca do bong da" />
    <meta property="og:title" content="Web chơi cá cược bóng đá, cá độ thể thao online số 1 Châu Á" />
    <meta property="og:description" content="88Cuoc là website chơi cá độ bóng đá trực tuyến, cá cược thể thao trên mạng hợp pháp và uy tín. Cung cấp tỷ lệ kèo cá cược bóng đá hôm nay FREE">
    <meta property="og:url" content="http://www.88cuoc.com/" />
    <meta property="og:image" content="http://www.88cuoc.com/" />
    <!--<link rel="shortcut icon" href="https://static.388bet.com/images/icon.png">-->
    <!--<meta name="viewport" content="width=1084" />-->
    <link type="text/css" href="<?php echo ConfigGlobal::$realPath ?>/css/style.css?v=16" rel="stylesheet">
    <link type="text/css" href="<?php echo ConfigGlobal::$realPath ?>/css/styles-iphone-7-375.css?v=16" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700&subset=latin,vietnamese" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&subset=latin,greek-ext,vietnamese" rel="stylesheet" type="text/css">

    <script src="<?php echo ConfigGlobal::$realPath ?>/js/header.js?v=5"></script>
    <style>
        .customSelect select{
            background-color: #fff;
            background-image: url(/images/dropdownCustom1.png);
            background-position: 245px center;
        }
        .main-menu{
            background-color: <?php echo $xanhColor ?>;
        }
        .top-header a.chat-now{
            background: <?php echo $xanhColor ?>;
        }
        .top-header .login{
            background: <?php echo $xanhColor ?>;
        }
        .btnCustom{
            background: <?php echo $xanhColor ?>;
        }
        .link-redirect a{
            background: <?php echo $xanhColor ?>;
        }
        .step-3 h3{
            color:<?php echo $xanhColor ?>;
        }
        .des-footer h3{
            color:<?php echo $xanhColor ?>;
        }
        .bottom-des{
            background-color: <?php echo $xanhColor ?>;
        }
    </style>
	<!-- Global site tag (gtag.js) - Google Analytics -->
</head>