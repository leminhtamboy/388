<?php
include_once ("Collection.php");
class BangDo extends  Collection{

    public function __construct($tableName, $primaryKey)
    {
        parent::__construct($tableName, $primaryKey);
    }
    public function getSoDiem($money,$loai_do){
        $dataDo = $this->load($loai_do);
        $so_tien_do = $dataDo->getgia_do();
        $so_diem  = $money / $so_tien_do;
        return number_format($so_diem,0);
    }
}