<?php
@session_start();
include_once("Article.php");
include_once("Menu.php");
include_once("Attribute.php");
include_once("AttributeValue.php");
include_once("processingImage.php");
include_once("Config.php");
include_once("BangDo.php");
include_once ("Giao_Dich_Tai_Khoan.php");
include_once ("Tai_Khoan_Ca_Cuoc.php");
$config= new ConfigGlobal("config","config_id");
$menu  = new Menu("menu","id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$article = new Article("article","id_article");
$attributeValue= new AttributeValue("entity_attribute_value_varchar","id_eav_varchar");
$processImage = new  processingImage();
//Gọi model bảng đô
$modelBangDo = new BangDo("bang_do","id_do");
$modelGiaoDichTaiKhaon = new Giao_Dich_Tai_Khoan("giao_dich_tai_khoan","id_giao_dich_tai_khoan");
$modelTaiKhoanCaCuoc = new Tai_Khoan_Ca_Cuoc("tai_khoan_ca_cuoc","id_tai_khoan");
$dataTaiKhoanCaCuoc = $modelTaiKhoanCaCuoc->loadByAttribute("id_member","eq",$_SESSION["member"],0);
$hasAccount = false;
if(count($dataTaiKhoanCaCuoc) > 0) {
    $hasAccount = true;
    $arrayDataBong88 = $modelTaiKhoanCaCuoc->loadByAttributeArray(["id_member","loai_tai_khoan"],["eq","eq"],[$_SESSION["member"],"bong88"],0);
    $arrayDataBanh88 = $modelTaiKhoanCaCuoc->loadByAttributeArray(["id_member","loai_tai_khoan"],["eq","eq"],[$_SESSION["member"],"banh88"],0);
    $arrayDataS128 = $modelTaiKhoanCaCuoc->loadByAttributeArray(["id_member","loai_tai_khoan"],["eq","eq"],[$_SESSION["member"],"s128"],0);
    $arrayDataS388 = $modelTaiKhoanCaCuoc->loadByAttributeArray(["id_member","loai_tai_khoan"],["eq","eq"],[$_SESSION["member"],"s388"],0);
}
?>
<!doctype html>
<!--[if lt IE 9]> <html class="no-js ie-old" lang="vi"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="vi"> <![endif]-->
<!--[if gt IE 8]> <html lang="vi"> <!--<![endif]-->
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<?php include_once("head.php"); ?>
<body>
<style>
    .chooseBankCustom .customRadio input[type="radio"], .chooseBank .customRadio label{
        margin-top: 0px !important;
    }
    .chooseBank .customRadio{
        min-height: 0;
    }
    .lineOneForm{
        margin-bottom: 55px;
    }
    .btnCustom{
        border-bottom: 0 !important;
    }
</style>
<div class="home-page-edit">
    <?php include_once("header.php"); ?>
    <div class="main-content">
        <div class="step-2 container">
            <div class='step'><span> Bước <number>1</number></span></div>
            <h2>
                <i class="icon-list"></i>Tạo Yêu Cầu
                <div class="NAPRUT">
                    <a class='onNap' href="#">Nạp</a>
                    <ul class='chooseAction'>
                        <li><a class='onRut' href="#">Rút</a></li>
                    </ul>
                </div>
                <select style='display:none' class='cash Oncash' onchange="changeOption(this)">
                    <option value="nạp">Nạp</option>
                    <option value="Rút">Rút</option>
                </select>tiền</h2>
            <span class="des-step-2 phieu-nap-text">NẠP TIỀN TỪ 88CUOC VÀO TÀI KHOẢN CÁ CƯỢC</span>
            <span class="des-step-2 phieu-rut-text" style="display:none;">LẬP PHIẾU RÚT TIỀN, ĐỢI CHÚNG TÔI RÚT ĐIỂM VÀ CHUYỂN KHOẢN</span>
            <div class="form-controlCustom">
                <a href="#" onclick="
                <?php if(!isset($_SESSION["member"])){ ?>
                    alert('Bạn chưa đăng nhập');return false;
                <?php }else{
                    ?>
                        $('.frm-invoice-panel').show();
                        var a = $('.onNap').text();
                        if(a == 'Nạp'){
                            $('#formChooseBankPush').show();
                            $('#formChooseBankPull').hide();
                        }else{
                            $('#formChooseBankPull').show();
                            $('#formChooseBankPush').hide();
                        }
                        return false;
                    <?php
                } ?>
                    " class='btnCustom btn-point bankinfo_open'>điền thông tin</a>
            </div>
            <?php if(isset($_SESSION["member"])){  ?>
                <?php
                if (isset($_POST['naptien'])) {
                    if($so_tien < intval(str_replace(".", "", $_POST["amount"]))){
                        ?>
                        <script>
                            alert("Số tiền không đủ để nạp");
                        </script>
                    <?php
                    }else {
                        $loai_giao_dich = $_POST["loai_giao_dich"];
                        $so_tien = $_POST["amount"];
                        $so_tien = str_replace(".", "", $so_tien);
                        $checkBoxTaiKhoan = $_POST["da_co_tai_khoan"];
                        $id_member = -1;
                        if ($checkBoxTaiKhoan == "chua") {
                            //Xử lý theo hướng có
                            $ly_do = $_POST["account_bet"];
                            $noi_dung = "";
                            if ($so_tien > 0) {
                                switch ($ly_do) {
                                    case "bong88":
                                        $noi_dung = "Tạo cho số điện thoại " . $_SESSION["sdt"] . " Tài khoản bong88 mới ";
                                        break;
                                    case "banh88":
                                        $noi_dung = "Tạo cho số điện thoại " . $_SESSION["sdt"] . " Tài khoản banh88 mới ";
                                        break;
                                    case "s128":
                                        $noi_dung = "Tạo cho số điện thoại " . $_SESSION["sdt"] . " Tài khoản sv128 mới ";
                                        break;
                                    case "s388":
                                        $noi_dung = "Tạo cho số điện thoại " . $_SESSION["sdt"] . " Tài khoản sv388 mới ";
                                        break;

                                }
                                //Xử lý giá đô
                                $id_do = $_POST["gia_do"];
                                $so_diem = $modelBangDo->getSoDiem($so_tien, $id_do);
                                $modelGiaoDichTaiKhaon->setData("id_giao_dich_tai_khoan", "NULL");
                                $modelGiaoDichTaiKhaon->setData("id_member", $_SESSION["member"]);
                                $modelGiaoDichTaiKhaon->setData("id_tai_khoan", "-1");
                                $modelGiaoDichTaiKhaon->setData("loai_giao_dich", $loai_giao_dich);
                                $modelGiaoDichTaiKhaon->setData("tinh_trang", 0);
                                $modelGiaoDichTaiKhaon->setData("so_tien", $so_tien);
                                $modelGiaoDichTaiKhaon->setData("so_diem", $so_diem);
                                $modelGiaoDichTaiKhaon->setData("noi_dung", $noi_dung);
                                $modelGiaoDichTaiKhaon->setData("id_do", $id_do);
                                $modelGiaoDichTaiKhaon->setData("ghi_chu", "chua co");
                                $modelGiaoDichTaiKhaon->inserRow();
                                include_once("Member.php");
                                $modelMember = new Member("member", "id");
                                $dataMember = $modelMember->loadMemberData($_SESSION["member"]);
                                $currentMoney = $dataMember->getso_tien();
                                $inputMoney = $currentMoney - $so_tien;
                                $modelMember->setData("id", $_SESSION["member"]);
                                $modelMember->setData("so_tien", $inputMoney);
                                $modelMember->updateRow();
                            } else {
                                switch ($ly_do) {
                                    case "bong88":
                                        $noi_dung = "Tạo cho số điện thoại " . $_SESSION["sdt"] . " Tài khoản bong88 mới (Chơi thử) ";
                                        break;
                                    case "banh88":
                                        $noi_dung = "Tạo cho số điện thoại " . $_SESSION["sdt"] . " Tài khoản banh88 mới (Chơi thử) ";
                                        break;
                                    case "s128":
                                        $noi_dung = "Tạo cho số điện thoại " . $_SESSION["sdt"] . " Tài khoản sv128 mới (Chơi thử) ";
                                        break;
                                    case "s388":
                                        $noi_dung = "Tạo cho số điện thoại " . $_SESSION["sdt"] . " Tài khoản sv388 mới (Chơi thử) ";
                                        break;

                                }
                                //Xử lý giá đô
                                $id_do = $_POST["gia_do"];
                                $so_diem = $modelBangDo->getSoDiem($so_tien, $id_do);
                                $modelGiaoDichTaiKhaon->setData("id_giao_dich_tai_khoan", "NULL");
                                $modelGiaoDichTaiKhaon->setData("id_member", $_SESSION["member"]);
                                $modelGiaoDichTaiKhaon->setData("id_tai_khoan", "-1");
                                $modelGiaoDichTaiKhaon->setData("loai_giao_dich", $loai_giao_dich);
                                $modelGiaoDichTaiKhaon->setData("tinh_trang", 0);
                                $modelGiaoDichTaiKhaon->setData("so_tien", $so_tien);
                                $modelGiaoDichTaiKhaon->setData("so_diem", $so_diem);
                                $modelGiaoDichTaiKhaon->setData("noi_dung", $noi_dung);
                                $modelGiaoDichTaiKhaon->setData("id_do", $id_do);
                                $modelGiaoDichTaiKhaon->setData("ghi_chu", "chua co");
                                $modelGiaoDichTaiKhaon->inserRow();
                            }
                        } else {
                            $tk_bong88 = $_POST["account_bet_bong88"];
                            $tk_banh88 = $_POST["account_bet_banh88"];
                            $tk_s128 = $_POST["account_bet_s128"];
                            $tk_s388 = $_POST["account_bet_s388"];
                            $id_do = -1;
                            $id_chung_tai_khoan = 0;
                            if ($tk_bong88 != "") {
                                //Nạp tiền cho tài khoản bóng 88
                                $id_chung_tai_khoan = $tk_bong88;
                                $dataTaiKhoanCaCuoc = $modelTaiKhoanCaCuoc->load($tk_bong88);
                                $nameMember = $dataTaiKhoanCaCuoc->getma_tai_khoan();
                                $noi_dung = "Nạp tiền cho tài khoản [ " . $nameMember . " ] bên bóng 88";
                                $id_do = $dataTaiKhoanCaCuoc->getid_do();
                            }
                            if ($tk_banh88 != "") {
                                //Nạp tiền cho tài khoản banh 88
                                $id_chung_tai_khoan = $tk_banh88;
                                $dataTaiKhoanCaCuoc = $modelTaiKhoanCaCuoc->load($tk_banh88);
                                $nameMember = $dataTaiKhoanCaCuoc->getma_tai_khoan();
                                $noi_dung = "Nạp tiền cho tài khoản [ " . $nameMember . " ] bên banh88";
                                $id_do = $dataTaiKhoanCaCuoc->getid_do();
                            }
                            if ($tk_s128 != "") {
                                //Nạp tiền cho tài khoản bóng 88
                                $id_chung_tai_khoan = $tk_s128;
                                $dataTaiKhoanCaCuoc = $modelTaiKhoanCaCuoc->load($tk_s128);
                                $nameMember = $dataTaiKhoanCaCuoc->getma_tai_khoan();
                                $noi_dung = "Nạp tiền cho tài khoản [ " . $nameMember . " ] bên s128";
                                $id_do = $dataTaiKhoanCaCuoc->getid_do();
                            }
                            if ($tk_s388 != "") {
                                //Nạp tiền cho tài khoản bóng 88
                                $id_chung_tai_khoan = $tk_s388;
                                $dataTaiKhoanCaCuoc = $modelTaiKhoanCaCuoc->load($tk_s388);
                                $nameMember = $dataTaiKhoanCaCuoc->getma_tai_khoan();
                                $noi_dung = "Nạp tiền cho tài khoản [ " . $nameMember . " ] bên sv388";
                                $id_do = $dataTaiKhoanCaCuoc->getid_do();
                            }
                            $so_diem = $modelBangDo->getSoDiem($so_tien, $id_do);
                            $modelGiaoDichTaiKhaon->setData("id_giao_dich_tai_khoan", "NULL");
                            $modelGiaoDichTaiKhaon->setData("id_member", $_SESSION["member"]);
                            $modelGiaoDichTaiKhaon->setData("id_tai_khoan", $id_chung_tai_khoan);
                            $modelGiaoDichTaiKhaon->setData("loai_giao_dich", $loai_giao_dich);
                            $modelGiaoDichTaiKhaon->setData("tinh_trang", 0);
                            $modelGiaoDichTaiKhaon->setData("so_tien", $so_tien);
                            $modelGiaoDichTaiKhaon->setData("so_diem", $so_diem);
                            $modelGiaoDichTaiKhaon->setData("noi_dung", $noi_dung);
                            $modelGiaoDichTaiKhaon->setData("id_do", $id_do);
                            $modelGiaoDichTaiKhaon->setData("ghi_chu", "chua co");
                            $modelGiaoDichTaiKhaon->inserRow();
                            include_once("Member.php");
                            $modelMember = new Member("member", "id");
                            $dataMember = $modelMember->loadMemberData($_SESSION["member"]);
                            $currentMoney = $dataMember->getso_tien();
                            $inputMoney = $currentMoney - $so_tien;
                            $modelMember->setData("id", $_SESSION["member"]);
                            $modelMember->setData("so_tien", $inputMoney);
                            $modelMember->updateRow();
                        }
                        $currentUrl = $modelGiaoDichTaiKhaon->getCurrentUrl();
                        $modelGiaoDichTaiKhaon->redirectToProcessingPage($currentUrl, ConfigGlobal::$realPath);
                    }
                }else{

                }
                ?>
                <div class="frm-invoice-panel" style="display:none">
                    <div id="formChooseBankPush" class="chooseBank chooseBankCustom">
                        <form id="sendmon" name="sendmon" method="post" class="cmxform form-horizontal tasi-form">
                            <input type="hidden" value="nap" name="loai_giao_dich" id="loai_giao_dich" />
                            <h3 class="chooseBankTile" style="position:relative;">Phiếu nạp tiền
                                <span class="infoVadilate sizeMedium nap_bank_error" style="display:none;font-weight:normal;left:159px;">Vui lòng chọn ngân hàng để nạp tiền</span>
                            </h3>
                            <div class="customRadio vietcombankchoose" style="display: none">
                                <input type="radio" checked="checked" name="bank" value="VCB" id="vietcombank"><label for="vietcombank"></label>
                                <label for="vietcombank"><img for="vietcombank" src="https://img.388bet.com/images/img-388-vcb.png"></label>
                            </div>
                            <div class="customRadio viettinchoose" style="display: none">
                                <input type="radio" name="bank" value="VietinBank" id="viettin"><label for="viettin"></label>
                                <label for="viettin"><img src="https://img.388bet.com/images/img-388-vtb.png"></label>
                            </div>
                            <div class="customRadio dongachoose" style="display: none">
                                <input type="radio" name="bank" value="DongA" id="donga"><label for="donga"></label>
                                <span class="customHeighBank"><label for="donga"><img src="https://img.388bet.com/images/img-388-donga.png"></label></span>
                            </div>
                            <div class="customRadio acbchoose" style="display: none">
                                <input type="radio" name="bank" value="ACB" id="acb"><label for="acb"></label>
                                <label for="acb"><img src="https://img.388bet.com/images/img-388-acb.png"> </label>
                            </div>
                            <div class="customRadio sacombankchoose" style="display: none">
                                <input type="radio" name="bank" value="Sacombank" id="sacombank"><label for="sacombank"></label>
                                <label for="sacombank"><img src="https://img.388bet.com/images/img-388-scb.png"></label>
                            </div>
                            <div class="customRadio techcombankchoose " style="display: none">
                                <input type="radio" name="bank" value="Techcombank" id="techcombank"><label for="techcombank"></label>
                                <label for="techcombank"><img src="https://img.388bet.com/images/img-388-tech.png"></label>
                            </div>
                            <div class="customRadio bidvchoose" style="display: none">
                                <input type="radio" name="bank" value="BIDV" id="bidv"><label for="bidv"></label>
                                <span class="customHeighBank">
                                  <label for="bidv"><img src="https://img.388bet.com/images/img-388-bidv.png"> </label>
                              </span>
                            </div>
                            <div class="formContentField">
                                <div class="lineOneForm">
                                    <div class="fieldForm oneInput">
                                        <input name="amount" type="text" autocomplete="off" class="customField vadilateCustomField" placeholder="Số tiền nạp" id="amount" style="width: 100%" onkeyup="moneyconvert(this);">
                                        <span class="infoVadilate sizeMedium amount_error" id="amount_error" style="display:none;">Số tiền nạp tối thiểu 300.000 vnd.</span>
                                        <span class="infoVadilate sizeMedium amount_nhap_error" id="amount_nhap_error" style="display:none;">Số tiền chưa nhập.</span>
                                        <span class="infoVadilate sizeMedium amount_nhap_error" id="amount_error_not_ok" style="display:none;"></span>

                                    </div>
                                    <!--<div class="fieldForm twoInput">
                                        <input name="sender" type="text" class="customField vadilateCustomField" placeholder="Tên người gửi">
                                        <span class="infoVadilate sizeMedium sender_error" style="display:none;">Tên người gửi không hợp lệ. </span>
                                    </div>-->
                                </div>
                                <div class="lineOneForm">
                                    <div class="fieldForm twoInput">
                                        <div class="customRadio">
                                            <input type="radio" name="da_co_tai_khoan" class="da_co_tai_khoan" checked="checked" value="chua" id="chua">
                                            <label for="chua"></label>
                                            <label for="chua">Tạo tài khoản mới</label>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="customRadio">
                                            <input type="radio" name="da_co_tai_khoan" class="da_co_tai_khoan" value="co" id="co">
                                            <label for="co"></label>
                                            <label for="co">Đã có tài khoản</label>
                                        </div>
                                    </div>
                                    <script>
                                        var gia_do_chon = 0;
                                        function refreshError(){
                                            $("#amount_nhap_error").hide();
                                            $("#amount_error").hide();
                                            $("#amount_error_not_ok").hide();
                                            $('#account_bet_error').hide();
                                            $("#gia_do_error").hide();
                                            $("#error_bong88").hide();
                                            $("#error_banh88").hide();
                                            $("#error_s128").hide();
                                            $("#error_s388").hide();
                                        }
                                        function checkError(){
                                            $moneyBeforeConvert = $("#amount").val();
                                            $moneyCheck = $moneyBeforeConvert.replace(".","");
                                            refreshError();
                                            $hasError = false;
                                            $checkCoTaiKhoan = $("#chua").is(":checked");
                                            if($moneyCheck < 300000){
                                                $("#amount_error").show();
                                                $("#amount_nhap_error").hide();
                                                $("#amount_error_not_ok").hide();
                                                $hasError = true;
                                            }
                                            if($checkCoTaiKhoan) {
                                                $gia_do = $("#gia_do :selected").attr("data-value");
                                                if ($moneyCheck % $gia_do != 0) {
                                                    $("#amount_nhap_error").hide();
                                                    $("#amount_error").hide();
                                                    $("#amount_error_not_ok").show();
                                                    $("#amount_error_not_ok").text("Số tiền phải là số chia hết cho " + $gia_do + " và lớn hơn 300.000.");
                                                    $hasError = true;
                                                }
                                            }else{
                                                console.log("asddsa"+gia_do_chon)
                                                if ($moneyCheck % gia_do_chon != 0) {
                                                    $("#amount_nhap_error").hide();
                                                    $("#amount_error").hide();
                                                    $("#amount_error_not_ok").show();
                                                    $("#amount_error_not_ok").text("Số tiền phải là số chia hết cho " + gia_do_chon + " và lớn hơn 300.000.");
                                                    $hasError = true;
                                                }
                                            }
                                            if($moneyCheck == ""){
                                                $("#amount_nhap_error").show();
                                                $("#amount_error").hide();
                                                $("#amount_error_not_ok").hide();
                                                $hasError = true;
                                            }
                                            if($checkCoTaiKhoan == false){
                                                if($("#account_bet_bong88").val() == "" && $("#account_bet_banh88").val() == "" && $("#account_bet_s128").val() == "" && $("#account_bet_s388").val() == ""){
                                                    $("#error_bong88").show();
                                                    $("#error_banh88").show();
                                                    $("#error_s128").show();
                                                    $("#error_s388").show();
                                                    $hasError = true;
                                                }

                                            }else{
                                                if($("#account_bet").val() == ""){
                                                    $('#account_bet_error').show();
                                                    $hasError = true;
                                                }
                                                if($("#gia_do").val() == ""){
                                                    $("#gia_do_error").show();
                                                    $hasError = true;
                                                }
                                            }
                                            return $hasError;
                                        }
                                        $(document).ready(function(){
                                            $("#noi_dung_tai_khoan_co").hide();
                                            $("#noi_dung_tai_khoan_chua").show();
                                            $("input[name='da_co_tai_khoan']").change(function(){
                                                $valueChange = $(this).val();
                                                refreshError();
                                                if($valueChange == "chua"){
                                                    $("#noi_dung_tai_khoan_co").hide();
                                                    $("#noi_dung_tai_khoan_chua").show();
                                                }else{
                                                    $("#noi_dung_tai_khoan_co").show();
                                                    $("#noi_dung_tai_khoan_chua").hide();
                                                }
                                                $.ajax({url: '<?php echo ConfigGlobal::$realPath ?>/ajax_get_noi_dung.php',
                                                    method: "POST",
                                                    data: {value:$valueChange},
                                                    success: function (data) {
                                                        $("#select_gia_do").html(data);
                                                    }});
                                            });
                                            $("#sendmon").submit(function () {
                                                if(checkError()){
                                                    return false;
                                                }
                                                var dong_y_rut = confirm("bạn có đồng ý tạo lệnh nạp tiền ?");
                                                if(dong_y_rut){
                                                    return true;
                                                }else{
                                                    return false;
                                                }
                                            });
                                            $("#account_bet_bong88").change(function(){
                                                $element = $(this);
                                                if($element.val()==""){
                                                    refreshSelectNap();
                                                }
                                                else{
                                                    gia_do_chon = $("#account_bet_bong88 :selected").attr("data-value");
                                                    disabledSelectBoxNap("account_bet_bong88")
                                                }
                                            });
                                            $("#account_bet_banh88").change(function(){
                                                $element = $(this);
                                                if($element.val()==""){
                                                    refreshSelectNap();
                                                }
                                                else{
                                                    gia_do_chon = $("#account_bet_banh88 :selected").attr("data-value");
                                                    disabledSelectBoxNap("account_bet_banh88")
                                                }
                                            });
                                            $("#account_bet_s128").change(function(){
                                                $element = $(this);
                                                if($element.val()==""){
                                                    refreshSelectNap();
                                                }
                                                else{
                                                    gia_do_chon = $("#account_bet_s128 :selected").attr("data-value");
                                                    disabledSelectBoxNap("account_bet_s128")
                                                }
                                            });
                                            $("#account_bet_s388").change(function(){
                                                $element = $(this);
                                                if($element.val()==""){
                                                    refreshSelectNap();
                                                }
                                                else{
                                                    gia_do_chon = $("#account_bet_s388 :selected").attr("data-value");
                                                    disabledSelectBoxNap("account_bet_s388")
                                                }
                                            });
                                            function disabledSelectBoxNap(idVao){
                                                var arrayId = ["account_bet_bong88","account_bet_banh88","account_bet_s128","account_bet_s388"];
                                                var $i=0;
                                                for($i=0;$i<arrayId.length;$i++){
                                                    if(idVao != arrayId[$i]){
                                                        $("#"+arrayId[$i]).attr("disabled","disabled");
                                                    }
                                                }
                                            }
                                            function refreshSelectNap(){
                                                var arrayId = ["account_bet_bong88","account_bet_banh88","account_bet_s128","account_bet_s388"];
                                                var $i=0;
                                                for($i=0;$i<arrayId.length;$i++){
                                                    $("#"+arrayId[$i]).removeAttr("disabled");
                                                }
                                            }
                                        });

                                    </script>
                                    <div class="fieldForm twoInput">
                                        <!--<input type='text' class='customField vadilateCustomField' placeholder='Số tài khoản'>-->
                                        <div class="customSelect" id="bno_opt">
                                            <select name="bno" style="display: none;">
                                                <option>Chọn tài khoản nhận</option>
                                                <option selected="selected" value="0103246596">TEST</option>
                                            </select>
                                        </div>
                                        <span class="infoVadilate bno_error" style="display:none;">Chọn tài khoản nhận</span>
                                        <script type="text/javascript">
                                            $("input[name='bank']").change(function(){
                                                $(".nap_bank_error").hide();
                                                if(this.value=="Nganluong"){
                                                    $("#phuong_thuc_giao_dich").val("QuaInternetBanking").attr("disabled","disabled");
                                                    $("#frmnap").append($("<input type=hidden id='txtphuong_thuc_giao_dich' value='QuaInternetBanking' name='phuong_thuc_giao_dich'/>"));
                                                }else{
                                                    $("#phuong_thuc_giao_dich").removeAttr("disabled");
                                                    $("#frmnap #txtphuong_thuc_giao_dich").remove();
                                                }
                                                gchange(document.getElementById('phuong_thuc_giao_dich').value);
                                                loadbno('d', this.value);
                                            });
                                            function gchange(val) {

                                                var _id= document.getElementById('transaction_bankcode');
                                                var _idbank=  $("input[name='bank']:checked").val();
                                                if( (val=='QuaInternetBanking') ) {
                                                    if(_idbank=='VCB')          { $(_id).attr("placeholder", 'Số lệnh giao dịch'); }
                                                    else if(_idbank=='DongA')        { $(_id).attr("placeholder",'Số tài khoản ngân hàng của bạn'); }
                                                    else if(_idbank=='ACB')          { $(_id).attr("placeholder", 'Họ và tên chủ tài khoản'); }
                                                    else if(_idbank=='VietinBank')   { $(_id).attr("placeholder",'Số tài khoản ngân hàng của bạn'); }
                                                    else if(_idbank=='BIDV')         { $(_id).attr("placeholder",'Số tham chiếu'); }
                                                    else if(_idbank=='Sacombank')    { $(_id).attr("placeholder",'Số giao dịch'); }
                                                    else if(_idbank=='Techcombank')  { $(_id).attr("placeholder",'Số bút toán'); }
                                                    else if(_idbank=='Nganluong')    { $(_id).attr("placeholder",'Mã giao dịch'); }
                                                    else  _id.innerHTML = 'Mã Giao Dịch';

                                                } else if(val=='QuaATM') {
                                                    $(_id).attr("placeholder",'Số tài khoản ngân hàng của bạn');
                                                    if(_idbank=='VietinBank')   $(_id).attr("placeholder",'Thời gian bạn chuyển tiền');
                                                } else if(val=='NopTienMat') {
                                                    $(_id).attr("placeholder",'Tên người nộp tiền');
                                                } else if((val=='')) {
                                                    $(_id).attr("placeholder",'Mã giao dịch')
                                                }
                                            }
                                        </script>
                                    </div>
                                </div>
                                <div class="lineOneForm" id="noi_dung_tai_khoan_chua">
                                    <div class="fieldForm twoInput">
                                        <div class="customSelect">
                                            <select id="account_bet" name="account_bet">
                                                <option value="">Chọn trang tạo tài khoản</option>
                                                <option value="bong88">Tạo cho tôi tài khoản Bong88 mới</option>
                                                <!--<option value="banh88">Tạo cho tôi tài khoản Banh88 mới</option>-->
                                                <option value="s128">Tạo cho tôi tài khoản Đá gà s128 mới</option>
                                                <option value="s388">Tạo cho tôi tài khoản Đá gà sv388 mới</option>
                                            </select>
                                        </div>
                                        <span class="infoVadilate sizeLarge account_bet_error" id="account_bet_error" style="display:none;">Vui lòng chọn trang để tạo tài khoản cá cược của bạn</span>
                                    </div>
                                    <div class="fieldForm twoInput">
                                        <div class="customSelect" id="select_gia_do">
                                        </div>
                                        <span class="infoVadilate sizeLarge gia_do_error" id="gia_do_error" style="display:none;">Vui lòng chọn giá đô cho tài khoản của bạn</span>
                                    </div>
                                    <script>
                                        $("#account_bet").change(function(){
                                            $valueChange = $(this).val();
                                            $.ajax({url: '<?php echo ConfigGlobal::$realPath ?>/ajax_get_do.php',
                                                method: "POST",
                                                data: {value:$valueChange},
                                                success: function (data) {
                                                    $("#select_gia_do").html(data);
                                               }});
                                        });
                                    </script>
                                </div>
                                <div class="lineOneForm" id="noi_dung_tai_khoan_co">
                                    <div class="fieldForm">
                                        <p><img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_bong88.png" style="margin-bottom: 40px"/></p>
                                        <div class="customSelect">
                                            <select id="account_bet_bong88" name="account_bet_bong88">
                                                <option value="">Chọn tài khoản cá cược</option>
                                                <?php
                                                foreach($arrayDataBong88 as $_item){
                                                    $valueId = $_item->getid_tai_khoan();
                                                    $ma_tai_khoan = $_item->getma_tai_khoan();
                                                    $ten_tai_khoan = $_item->getten_tai_khoan();
                                                    $so_diem = $_item->getso_diem();
                                                    $id_do = $_item->getid_do();
                                                    $dataDo = $modelBangDo->load($id_do);
                                                    $textShow = $ma_tai_khoan." - ".$ten_tai_khoan." - ".$dataDo->gethien_thi_do();
                                                    ?>
                                                    <option value="<?php echo $valueId; ?>" data-value="<?php echo $dataDo->getgia_do(); ?>"><?php echo $textShow; ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <span class="infoVadilate sizeLarge account_bet_error" id="error_bong88" style="display:none;bottom: auto;margin-top: -45px;">Vui lòng chọn tài khoản cá cược Bóng88 của bạn</span>
                                    </div>
                                    <div class="fieldForm">
                                        <p><img src="<?php echo ConfigGlobal::$realPath ?>/image/rsz_1rsz_1banh88.png" /></p>
                                        <div class="customSelect">
                                            <select id="account_bet_banh88" name="account_bet_banh88">
                                                <option value="">Chọn tài khoản cá cược</option>
                                                <?php
                                                foreach($arrayDataBanh88 as $_item){
                                                    $valueId = $_item->getid_tai_khoan();
                                                    $ma_tai_khoan = $_item->getma_tai_khoan();
                                                    $ten_tai_khoan = $_item->getten_tai_khoan();
                                                    $so_diem = $_item->getso_diem();
                                                    $id_do = $_item->getid_do();
                                                    $dataDo = $modelBangDo->load($id_do);
                                                    $textShow = $ma_tai_khoan." - ".$ten_tai_khoan." - ".$so_diem." điểm - ".$dataDo->gethien_thi_do()." - ".$dataDo->gethoa_hong();
                                                    ?>
                                                    <option value="<?php echo $valueId; ?>" data-value="<?php echo $dataDo->getgia_do(); ?>"><?php echo $textShow; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <span class="infoVadilate sizeLarge account_bet_error" id="error_banh88" style="display:none;bottom: auto;margin-top: -45px;">Vui lòng chọn tài khoản cá cược Banh88 của bạn</span>
                                    </div>
                                    <div class="fieldForm">
                                        <p><img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_288.png" style="width: 190px;height:55px;margin-bottom: 40px;margin-top: 40px"/></p>
                                        <div class="customSelect">
                                            <select id="account_bet_s128" name="account_bet_s128">
                                                <option value="">Chọn tài khoản cá cược</option>
                                                <?php
                                                foreach($arrayDataS128 as $_item){
                                                    $valueId = $_item->getid_tai_khoan();
                                                    $ma_tai_khoan = $_item->getma_tai_khoan();
                                                    $ten_tai_khoan = $_item->getten_tai_khoan();
                                                    $so_diem = $_item->getso_diem();
                                                    $id_do = $_item->getid_do();
                                                    $dataDo = $modelBangDo->load($id_do);
                                                    $textShow = $ma_tai_khoan." - ".$ten_tai_khoan." - ".$so_diem." điểm - ".$dataDo->gethien_thi_do()." - ".$dataDo->gethoa_hong();
                                                    ?>
                                                    <option value="<?php echo $valueId; ?>" data-value="<?php echo $dataDo->getgia_do(); ?>"><?php echo $textShow; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <span class="infoVadilate sizeLarge account_bet_error" id="error_s128" style="display:none;bottom: auto;margin-top: -45px;">Vui lòng chọn tài khoản cá cược S128 của bạn</span>
                                    </div>
                                    <div class="fieldForm">
                                        <p><img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_388.png" style="width: 190px;height:55px;margin-bottom: 40px;margin-top: 40px"/></p>
                                        <div class="customSelect">
                                            <select id="account_bet_s388" name="account_bet_s388">
                                                <option value="">Chọn tài khoản cá cược</option>
                                                <?php
                                                foreach($arrayDataS128 as $_item){
                                                    $valueId = $_item->getid_tai_khoan();
                                                    $ma_tai_khoan = $_item->getma_tai_khoan();
                                                    $ten_tai_khoan = $_item->getten_tai_khoan();
                                                    $so_diem = $_item->getso_diem();
                                                    $id_do = $_item->getid_do();
                                                    $dataDo = $modelBangDo->load($id_do);
                                                    $textShow = $ma_tai_khoan." - ".$ten_tai_khoan." - ".$so_diem." điểm - ".$dataDo->gethien_thi_do()." - ".$dataDo->gethoa_hong();
                                                    ?>
                                                    <option value="<?php echo $valueId; ?>" data-value="<?php echo $dataDo->getgia_do(); ?>"><?php echo $textShow; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <span class="infoVadilate sizeLarge account_bet_error" id="error_s388" style="display:none;bottom: auto;margin-top: -45px;">Vui lòng chọn tài khoản cá cược SV388 của bạn</span>
                                    </div>
                                </div>
                                <div class="actionChooseBank">
                                    <input type="submit" name="naptien" style="height:50px" value="Nạp Tiền" class="btnCustom"/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="formChooseBankPull" class="chooseBank" style="display:none;">
                        <form class="cmxform form-horizontal tasi-form" role="form" id="frmrut" name="frmrut" method="post">
                            <input type="hidden" value="rut" name="loai_giao_dich_rut" id="loai_giao_dich_rut" />
                            <h3 class="chooseBankTile" style="position: relative;">Chọn ngân hàng để rút tiền
                                <span class="infoVadilate sizeMedium rut_bank_error" style="display:none;font-weight:normal;left:159px;">Vui lòng chọn ngân hàng để rút tiền</span>
                            </h3>

                            <div class="customRadio vietcombankchoose">
                                <input type="radio" name="bank" value="VCB" id="vietcombankpull"><label for="vietcombankpull"></label>
                                <label for="vietcombankpull"><img src="https://img.388bet.com/images/vietcombankchoose.png"></label>
                            </div>

                            <div class="customRadio viettinchoose">
                                <input type="radio" name="bank" value="VietinBank" id="viettinpull"><label for="viettinpull"></label>
                                <label for="viettinpull"><img src="https://img.388bet.com/images/viettinchoose.png"></label>
                            </div>

                            <div class="customRadio dongachoose">
                                <input type="radio" name="bank" value="DongA" id="dongapull"><label for="dongapull"></label>
                                <span class="customHeighBank"><label for="dongapull"><img src="https://img.388bet.com/images/dongachoose.png"></label></span>
                            </div>

                            <div class="customRadio acbchoose">
                                <input type="radio" name="bank" value="ACB" id="acbpull"><label for="acbpull"></label>
                                <label for="acbpull"><img src="https://img.388bet.com/images/acbchoose.png"> </label>
                            </div>

                            <div class="customRadio sacombankchoose">
                                <input type="radio" name="bank" value="Sacombank" id="sacombankpull"><label for="sacombankpull"></label>
                                <label for="sacombankpull"><img src="https://img.388bet.com/images/sacombankchoose.png"></label>
                            </div>
                            <div class="customRadio techcombankchoose" style="width:145px;">
                                <input type="radio" name="bank" value="Techcombank" id="techcombankpull"><label style="top:-7px;" for="techcombankpull"></label>
                                <label for="techcombankpull"><img style="width:110px;height: 35px;top:-3px;" src="https://img.388bet.com/images/techcombankchoose.png"></label>
                            </div>
                            <div class="customRadio bidvchoose">
                                <input type="radio" name="bank" value="BIDV" id="bidvpull"><label for="bidvpull"></label>
                                <span class="customHeighBank"><label for="bidvpull"><img src="https://img.388bet.com/images/bidvchoose.png"> </label></span>
                            </div>
                            <div class="customRadio agribankchoose" style="margin-bottom: 55px">
                                <input type="radio" name="bank" value="agribank" id="agribankvpull"><label for="agribankvpull"></label>
                                <span class="customHeighBank">
                                    <label for="bidvpull">
                                        <img src="<?php echo ConfigGlobal::$realPath; ?>/image/rsz_agribank-logo-big.png">
                                    </label>
                                </span>
                            </div>
                            <script>
                                $("#frmrut input[name='bank']").change(function(){
                                    $(".rut_bank_error").hide();
                                });
                                $(document).ready(function(){
                                    $("#noi_dung_rut_tu_tai_khoan").hide();
                                    $("#frmrut").submit(function () {
                                        if(checkErrorRut()){
                                            return false;
                                        }else {
                                            var dong_y_rut = confirm("bạn có đồng ý tạo lệnh rút tiền ?");
                                            if (dong_y_rut) {
                                                return true;
                                            } else{
                                                return false;
                                            }
                                        }
                                    });
                                    $("input[name='rut_tien_tu_88_cuoc']").change(function(){
                                        $element = $(this);
                                        if($element.val()=="rut_88_cuoc"){
                                            $("#noi_dung_rut_tu_tai_khoan").hide();
                                        }
                                        else{
                                            $("#noi_dung_rut_tu_tai_khoan").show();
                                        }
                                    });
                                    $("#rut_account_bet_bong88").change(function(){
                                        $element = $(this);
                                        if($element.val()==""){
                                            refreshSelect();
                                        }
                                        else{
                                            disabledSelectBox("rut_account_bet_bong88")
                                        }
                                    });
                                    $("#rut_account_bet_banh88").change(function(){
                                        $element = $(this);
                                        if($element.val()==""){
                                            refreshSelect();
                                        }
                                        else{
                                            disabledSelectBox("rut_account_bet_banh88")
                                        }
                                    });
                                    $("#rut_account_bet_s128").change(function(){
                                        $element = $(this);
                                        if($element.val()==""){
                                            refreshSelect();
                                        }
                                        else{
                                            disabledSelectBox("rut_account_bet_s128")
                                        }
                                    });
                                    $("#rut_account_bet_s388").change(function(){
                                        $element = $(this);
                                        if($element.val()==""){
                                            refreshSelect();
                                        }
                                        else{
                                            disabledSelectBox("rut_account_bet_s388")
                                        }
                                    });
                                });
                                function disabledSelectBox(idVao){
                                    var arrayId = ["rut_account_bet_bong88","rut_account_bet_banh88","rut_account_bet_s128","rut_account_bet_s388"];
                                    var $i=0;
                                    for($i=0;$i<arrayId.length;$i++){
                                        if(idVao != arrayId[$i]){
                                            $("#"+arrayId[$i]).attr("disabled","disabled");
                                        }
                                    }
                                }
                                function refreshSelect(){
                                    var arrayId = ["rut_account_bet_bong88","rut_account_bet_banh88","rut_account_bet_s128","rut_account_bet_s388"];
                                    var $i=0;
                                    for($i=0;$i<arrayId.length;$i++){
                                            $("#"+arrayId[$i]).removeAttr("disabled");
                                    }
                                }
                                function refreshErrorRut(){
                                    $("#rut_dien_thoai_loi").hide();
                                    $("#rut_tien_loi").hide();
                                    $('#rut_nguoi_nhan_loi').hide();
                                    $("#rut_noi_dung_loi").hide();
                                    $("#rut_tai_khoan_loi").hide();

                                }
                                function checkErrorRut(){
                                    $moneyBeforeConvert = $("#rut_amount").val();
                                    $moneyCheck = $moneyBeforeConvert.replace(".","");
                                    refreshErrorRut();
                                    $hasError = false;
                                    if($("#frmrut").find("input[name='bank']:checked").length==0){
                                        $(".rut_bank_error").show();
                                        $hasError = true;
                                    }else{
                                        $(".rut_bank_error").hide();
                                        $hasError = false;
                                    }
                                    if($moneyCheck < 100000){
                                        $("#rut_tien_loi").show();
                                        $hasError = true;
                                    }
                                    if($moneyCheck == ""){
                                        $("#rut_tien_loi").show();
                                        $hasError = true;
                                    }
                                    $ten_nguoi_nhan = $("#rut_nguoi_nhan").val();
                                    if($ten_nguoi_nhan == ""){
                                        $("#rut_nguoi_nhan_loi").show();
                                        $hasError = true;
                                    }
                                    $so_tai_khoan = $("#rut_tai_khoan").val();
                                    if($so_tai_khoan == ""){
                                        $("#rut_tai_khoan_loi").show();
                                        $hasError = true;
                                    }
                                    $so_dien_thoai = $("#rut_dien_thoai").val();
                                    if($so_dien_thoai == ""){
                                        $("#rut_dien_thoai_loi").show();
                                        $hasError = true;
                                    }
                                    if($so_dien_thoai.length < 10 ||  $so_dien_thoai.length > 11){
                                        $("#rut_dien_thoai_loi").show();
                                        $hasError = true;
                                    }
                                    $checkCoTaiKhoan = $("#rut_tien_tu_tai_khoan").is(":checked");
                                    if($checkCoTaiKhoan == true){
                                        if($("#rut_account_bet_bong88").val() == ""
                                            && $("#rut_account_bet_banh88").val() == ""
                                            && $("#rut_account_bet_s128").val() == ""
                                            && $("#rut_account_bet_s388").val() == ""){
                                            $("#rut_error_bong88").show();
                                            $("#rut_error_banh88").show();
                                            $("#rut_error_s128").show();
                                            $("#rut_error_s388").show();
                                            $hasError = true;
                                        }
                                    }
                                    return $hasError;
                                }
                            </script>
                            <?php
                            if(isset($_POST["btnrut"])){
                                $bank = $_POST["bank"];
                                $so_tien = $_POST["rut_amount"];
                                $so_tien = str_replace(".","",$so_tien);
                                $ten_nguoi_dung = $_POST["rut_nguoi_nhan"];
                                $so_tai_khoan = $_POST["rut_tai_khoan"];
                                $so_dien_thoai = $_POST["rut_dien_thoai"];
                                $noi_dung_chinh = $_POST["rut_noi_dung"];
                                $id_tai_khoan_rut = "-1";
                                $noi_dung = "";
                                $so_diem = 0;
                                $id_do  = -1;
                                if($_POST["rut_tien_tu_88_cuoc"] == "rut_88_cuoc"){
                                    $noi_dung = " Rút tiền từ web 88 bóng sdt member [ ".$_SESSION["sdt"]." ] với số tiền là ".$so_tien." có số tài khoản là ".$so_tai_khoan." ".$noi_dung_chinh;
                                }else{
                                    $tk_bong88 = $_POST["rut_account_bet_bong88"];
                                    $tk_banh88 = $_POST["rut_account_bet_banh88"];
                                    $tk_s128 = $_POST["rut_account_bet_s128"];
                                    $tk_s388 = $_POST["rut_account_bet_s388"];
                                    $id_chung_tai_khoan = 0;
                                    if($tk_bong88 != ""){
                                        //Nạp tiền cho tài khoản bóng 88
                                        $id_chung_tai_khoan = $tk_bong88;
                                        $dataTaiKhoanCaCuoc = $modelTaiKhoanCaCuoc->load($tk_bong88);
                                        $nameMember = $dataTaiKhoanCaCuoc->getma_tai_khoan();
                                        $noi_dung = "Rút tiền cho tài khoản [ ".$nameMember." ] bên bóng 88 với số tiền là ".$so_tien." có số tài khoản là ".$so_tai_khoan." ".$noi_dung_chinh;
                                        $id_do = $dataTaiKhoanCaCuoc->getid_do();

                                    }
                                    if($tk_banh88 != ""){
                                        //Nạp tiền cho tài khoản banh 88
                                        $id_chung_tai_khoan = $tk_banh88;
                                        $dataTaiKhoanCaCuoc = $modelTaiKhoanCaCuoc->load($tk_banh88);
                                        $nameMember = $dataTaiKhoanCaCuoc->getma_tai_khoan();
                                        $noi_dung = "Rút tiền cho tài khoản [ ".$nameMember." ] bên banh88 với số tiền là ".$so_tien." có số tài khoản là ".$so_tai_khoan." ".$noi_dung_chinh;
                                        $id_do = $dataTaiKhoanCaCuoc->getid_do();
                                    }
                                    if($tk_s128 != ""){
                                        //Nạp tiền cho tài khoản bóng 88
                                        $id_chung_tai_khoan = $tk_s128;
                                        $dataTaiKhoanCaCuoc = $modelTaiKhoanCaCuoc->load($tk_s128);
                                        $nameMember = $dataTaiKhoanCaCuoc->getma_tai_khoan();
                                        $noi_dung = "Rút tiền cho tài khoản [ ".$nameMember." ] bên s128 với số tiền là ".$so_tien." có số tài khoản là ".$so_tai_khoan." ".$noi_dung_chinh;
                                        $id_do = $dataTaiKhoanCaCuoc->getid_do();
                                    }
                                    if($tk_s388 != ""){
                                        //Nạp tiền cho tài khoản bóng 88
                                        $id_chung_tai_khoan = $tk_s388;
                                        $dataTaiKhoanCaCuoc = $modelTaiKhoanCaCuoc->load($tk_s388);
                                        $nameMember = $dataTaiKhoanCaCuoc->getma_tai_khoan();
                                        $noi_dung = "Rút tiền cho tài khoản [ ".$nameMember." ] bên sv388 với số tiền là ".$so_tien." có số tài khoản là ".$so_tai_khoan." ".$noi_dung_chinh;
                                        $id_do = $dataTaiKhoanCaCuoc->getid_do();
                                    }
                                    $so_diem = $modelBangDo->getSoDiem($so_tien,$id_do);
                                    $id_tai_khoan_rut = $id_chung_tai_khoan;
                                }
                                $modelGiaoDichTaiKhaon->setData("id_giao_dich_tai_khoan","NULL");
                                $modelGiaoDichTaiKhaon->setData("id_member",$_SESSION["member"]);
                                $modelGiaoDichTaiKhaon->setData("id_tai_khoan",$id_tai_khoan_rut);
                                $modelGiaoDichTaiKhaon->setData("loai_giao_dich","rut");
                                $modelGiaoDichTaiKhaon->setData("tinh_trang",0);
                                $modelGiaoDichTaiKhaon->setData("so_tien",$so_tien);
                                $modelGiaoDichTaiKhaon->setData("so_diem",$so_diem);
                                $modelGiaoDichTaiKhaon->setData("noi_dung",$noi_dung);
                                $modelGiaoDichTaiKhaon->setData("id_do",$id_do);
                                $modelGiaoDichTaiKhaon->setData("ghi_chu","chua co");
                                $modelGiaoDichTaiKhaon->inserRow();
                                $currentUrl = $modelGiaoDichTaiKhaon->getCurrentUrl();
                                $modelGiaoDichTaiKhaon->redirectToProcessingPage($currentUrl,ConfigGlobal::$realPath);
                            }
                            ?>
                            <p></p>
                            <div class="formContentField" style="margin-top: 50px">
                                <div class="lineOneForm">
                                    <div class="fieldForm twoInput">
                                        <input name="rut_amount" id="rut_amount" type="text" class="customField vadilateCustomField" placeholder="Số tiền rút" onkeyup="moneyconvert(this);">
                                        <span class="infoVadilate sizeLarge amount_error" id="rut_tien_loi" style="display:none;">Số tiền rút tối thiểu 100.000 VND</span>
                                    </div>
                                    <div class="fieldForm twoInput">
                                        <input name="rut_nguoi_nhan"  type="text" id="rut_nguoi_nhan" class="customField vadilateCustomField" placeholder="Tên người nhận">
                                        <span class="infoVadilate sizeLarge reciever_error" id="rut_nguoi_nhan_loi" style="display:none;">Tên người nhận không hợp lệ.</span>
                                    </div>
                                </div>
                                <div class="lineOneForm">
                                    <div class="fieldForm twoInput">
                                        <input name="rut_tai_khoan" type="text" id="rut_tai_khoan" class="customField vadilateCustomField" placeholder="Số tài khoản">
                                        <span class="infoVadilate bank_no_error" id="rut_tai_khoan_loi" style="display:none;">Số tài khoản không hợp lệ.</span>
                                    </div>
                                    <div class="fieldForm twoInput">
                                        <input name="rut_dien_thoai" type="text" id="rut_dien_thoai" class="customField vadilateCustomField" value="<?php echo $_SESSION["sdt"] ?>" placeholder="Số điện thoại">
                                        <span class="infoVadilate phone_no_error" id="rut_dien_thoai_loi" style="display:none;">Điền SĐT của bạn</span>
                                    </div>
                                </div>
                                <div class="lineOneForm">
                                    <div class="fieldForm twoInput">
                                        <div class="customRadio">
                                            <input type="radio" name="rut_tien_tu_88_cuoc" class="rut_tien_88_cuoc" checked="checked" value="rut_88_cuoc" id="rut_tien_trang">
                                            <label for="rut_tien_trang"></label>
                                            <label for="rut_tien_trang">Rút tiền từ 88 cược</label>
                                        </div>
                                    </div>
                                    <div class="fieldForm twoInput">
                                        <div class="customRadio">
                                            <input type="radio" name="rut_tien_tu_88_cuoc" class="rut_tien_88_cuoc" value="rut_tu_tai_khoan" id="rut_tien_tu_tai_khoan">
                                            <label for="rut_tien_tu_tai_khoan"></label>
                                            <label for="rut_tien_tu_tai_khoan">Rút từ tài khoản cá cược</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="lineOneForm" id="noi_dung_rut_tu_tai_khoan">
                                    <div class="fieldForm">
                                        <p><img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_bong88.png" style="margin-bottom: 40px"/></p>
                                        <div class="customSelect">
                                            <select id="rut_account_bet_bong88" name="rut_account_bet_bong88">
                                                <option value="">Chọn tài khoản cá cược</option>
                                                <?php
                                                foreach($arrayDataBong88 as $_item){
                                                    $valueId = $_item->getid_tai_khoan();
                                                    $ma_tai_khoan = $_item->getma_tai_khoan();
                                                    $ten_tai_khoan = $_item->getten_tai_khoan();
                                                    $so_diem = $_item->getso_diem();
                                                    $id_do = $_item->getid_do();
                                                    $dataDo = $modelBangDo->load($id_do);
                                                    $textShow = $ma_tai_khoan." - ".$ten_tai_khoan." - ".$dataDo->gethien_thi_do();
                                                    ?>
                                                    <option value="<?php echo $valueId; ?>"><?php echo $textShow; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <span class="infoVadilate sizeLarge account_bet_error" id="rut_error_bong88" style="display:none;bottom: auto;margin-top: -45px;">Vui lòng chọn tài khoản cá cược Bóng88 của bạn</span>
                                    </div>
                                    <div class="fieldForm">
                                        <p><img src="<?php echo ConfigGlobal::$realPath ?>/image/rsz_1rsz_1banh88.png" /></p>
                                        <div class="customSelect">
                                            <select id="rut_account_bet_banh88" name="rut_account_bet_banh88">
                                                <option value="">Chọn tài khoản cá cược</option>
                                                <?php
                                                foreach($arrayDataBanh88 as $_item){
                                                    $valueId = $_item->getid_tai_khoan();
                                                    $ma_tai_khoan = $_item->getma_tai_khoan();
                                                    $ten_tai_khoan = $_item->getten_tai_khoan();
                                                    $so_diem = $_item->getso_diem();
                                                    $id_do = $_item->getid_do();
                                                    $dataDo = $modelBangDo->load($id_do);
                                                    $textShow = $ma_tai_khoan." - ".$ten_tai_khoan." - ".$so_diem." điểm - ".$dataDo->gethien_thi_do()." - ".$dataDo->gethoa_hong();
                                                    ?>
                                                    <option value="<?php echo $valueId; ?>"><?php echo $textShow; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <span class="infoVadilate sizeLarge account_bet_error" id="rut_error_banh88" style="display:none;bottom: auto;margin-top: -45px;">Vui lòng chọn tài khoản cá cược Banh88 của bạn</span>
                                    </div>
                                    <div class="fieldForm">
                                        <p><img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_288.png" style="width: 190px;height:55px;margin-bottom: 40px;margin-top: 40px"/></p>
                                        <div class="customSelect">
                                            <select id="rut_account_bet_s128" name="rut_account_bet_s128">
                                                <option value="">Chọn tài khoản cá cược</option>
                                                <?php
                                                foreach($arrayDataS128 as $_item){
                                                    $valueId = $_item->getid_tai_khoan();
                                                    $ma_tai_khoan = $_item->getma_tai_khoan();
                                                    $ten_tai_khoan = $_item->getten_tai_khoan();
                                                    $so_diem = $_item->getso_diem();
                                                    $id_do = $_item->getid_do();
                                                    $dataDo = $modelBangDo->load($id_do);
                                                    $textShow = $ma_tai_khoan." - ".$ten_tai_khoan." - ".$so_diem." điểm - ".$dataDo->gethien_thi_do()." - ".$dataDo->gethoa_hong();
                                                    ?>
                                                    <option value="<?php echo $valueId; ?>"><?php echo $textShow; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <span class="infoVadilate sizeLarge account_bet_error" id="rut_error_s128" style="display:none;bottom: auto;margin-top: -45px;">Vui lòng chọn tài khoản cá cược S128 của bạn</span>
                                    </div>
                                    <div class="fieldForm">
                                        <p><img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_388.png" style="width: 190px;height:55px;margin-bottom: 40px;margin-top: 40px"/></p>
                                        <div class="customSelect">
                                            <select id="rut_account_bet_s388" name="rut_account_bet_s388">
                                                <option value="">Chọn tài khoản cá cược</option>
                                                <?php
                                                foreach($arrayDataS128 as $_item){
                                                    $valueId = $_item->getid_tai_khoan();
                                                    $ma_tai_khoan = $_item->getma_tai_khoan();
                                                    $ten_tai_khoan = $_item->getten_tai_khoan();
                                                    $so_diem = $_item->getso_diem();
                                                    $id_do = $_item->getid_do();
                                                    $dataDo = $modelBangDo->load($id_do);
                                                    $textShow = $ma_tai_khoan." - ".$ten_tai_khoan." - ".$so_diem." điểm - ".$dataDo->gethien_thi_do()." - ".$dataDo->gethoa_hong();
                                                    ?>
                                                    <option value="<?php echo $valueId; ?>"><?php echo $textShow; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <span class="infoVadilate sizeLarge account_bet_error" id="rut_error_s388" style="display:none;bottom: auto;margin-top: -45px;">Vui lòng chọn tài khoản cá cược SV388 của bạn</span>
                                    </div>
                                </div>
                                <div class="lineOneForm">
                                    <div class="fieldForm oneInput">
                                        <div class="textArea">
                                            <label for="ghi_chu" style="margin-bottom: 10px;font-weight: bold"><b>Nội Dung</b></label>
                                            <textarea class="ghi_chu" width="100%" name="rut_noi_dung" id="rut_noi_dung" height="150px" style="width: 100%;height:150px"></textarea>
                                        </div>
                                        <span class="infoVadilate sizeLarge account_bet_error" id="rut_noi_dung_loi" style="display:none;">Vui lòng điền nội dung rút tiền</span>
                                    </div>
                                </div>
                                <div class="actionChooseBank">
                                    <input type="submit" name="btnrut" id="btnrut" style="height:50px" value="Rút Tiền" class="btnCustom"/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <style>
                        .infoVadilate.sizeMedium,.infoVadilate.sizeLarge{
                            bottom:41px;
                            top:auto;

                        }
                    </style>
                </div>
            <?php } ?>
            <style>
                .customSelect select{
                    text-indent:7px;
                    line-height:31px;
                }
                .step-3 strong{
                    width: auto !important;
                }
            </style>
        </div>
        <div class="step-3 container">
            <div class="step"><span> Bước <number>2</number></span></div>
            <h2 class="oneMin"><i class="icon-Time"></i>1 phút - Nhanh chóng</h2>
            <span class="des-step-2">Sau khi nạp hoặc rút tiền, đợi nhân viên xử lý và cập nhật yêu cầu quý khách</span>
            <span class="des-step-2">Quý khách có thể xem phiếu yêu cầu quý khách</span>
            <strong>
                <img src="https://static.388bet.com/images/tai.png">
                <div>
                    <a style="text-decoration: none;color:black" href="<?php echo ConfigGlobal::$realPath ?>/lich-su-giao-dich.html"> Xem Giao Dịch </a>
                </div>
            </strong>

            <span class="lineIn"></span>
            <div class="step-3-left">
                <img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_bong88.png" style="margin: 25px 100px;"/>
                <div class="link-redirect">
                    <a class="new-link" target="_blank" href="<?php echo ConfigGlobal::$realPath; ?>/tai-khoan-dung-thu-bong-88.html">Link demo</a>
                    <a class="home-page" target="_blank" href="http://www.bong88.com/">trang chủ</a>
                </div>
            </div>
            <div class="step-3-right">
                <img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_banh888_new.png" style="margin: 12px 150px;width: 190px"/>
                <div class="link-redirect">

                    <a class="new-link" href="#" onclick="alert('Banh888.com chúng tôi sẽ sớm phát hành trong thời gian sớm nhất. Cám ơn sự quan tâm của quý khách')">Link demo</a>
                    <a class="home-page" href="#" onclick="alert('Banh888.com chúng tôi sẽ sớm phát hành trong thời gian sớm nhất. Cám ơn sự quan tâm của quý khách')">trang chủ</a>
                </div>
            </div>
            <div class="step-3-left">
                <img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_288.png" style="margin: 25px 100px;width: 190px;height:55px"/>
                <div class="link-redirect">
                    <a class="new-link" target="_blank" href="<?php echo ConfigGlobal::$realPath ?>/tai-khoan-dung-thu-sv128.html">Link demo</a>
                    <a class="home-page" target="_blank" href="http://www1.s128.net/Landing.aspx?">trang chủ</a>

                </div>
            </div>
            <div class="step-3-right">
                <img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_388.png" style="margin: 25px 100px;"/>
                <div class="link-redirect">

                    <a class="new-link" target="_blank" href="<?php echo ConfigGlobal::$realPath ?>/tai-khoan-dung-thu-sv388.html">Link demo</a>
                    <a class="home-page" target="_blank" href="https://www.sv388.com/index.jsp?722556214">trang chủ</a>
                </div>
            </div>
            <div style="clear:both;"></div>
            <script>
                var slide_is_stop = false;
                var rand = Math.floor((Math.random() * $(".news-partition ul li").length) + 0) * 20 * -1;
                $(".news-partition ul").animate({top: rand + 'px'}, 500);
                setInterval(function () {
                    if (slide_is_stop || $(".news-partition ul li").length < 2)
                        return;
                    if ($(".news-partition ul").position().top + $(".news-partition ul").height() <= 20) {
                        $(".news-partition ul").css("top", "0px");
                    }
                    $(".news-partition ul").animate({top: '-=20px'}, 500);
                }, 60000);
                $(".news-partition ul li a").hover(function () {
                    slide_is_stop = true;
                }, function () {
                    slide_is_stop = false;
                });
            </script>
        </div>
    </div>
    <?php include_once("footer.php") ?>
</div>
<script type="text/javascript">


    var imgArr=['<?php echo ConfigGlobal::$realPath ?>/image/home_jalan_vi-VN.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/ad_fastmarket.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/banner3_en.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/cock_fight.jpg'];
    var numberSilider =Math.floor((Math.random() * imgArr.length));
    //alert(imgArr);
    var url =imgArr[numberSilider];
    $('#imagesRandom').attr('src',url);

    $(document).ready(function(){
        $('.onNap').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            if(a == 'Nạp'){
                $('.onRut').text('Rút');
                $("#lb-naprut-caption").text("Rút tiền");

            }
            else{
                $('.onRut').text('Nạp');
                $("#lb-naprut-caption").text("Nạp tiền");
            }
            $('.chooseAction').toggle();
        });
        $('.onRut').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            //show_invoice_form();
            if(a == 'Nạp'){
                $(".form-controlCustom").show();
                $('#formChooseBankPull').hide();
                $('#formChooseBankPush').hide();
                $(".phieu-nap-text").hide();
                $(".phieu-rut-text").show();
                $('.onNap').text('Rút');
                $('.chooseAction').hide()
            }else{
                $(".form-controlCustom").show();
                $('#formChooseBankPush').hide();
                $('#formChooseBankPull').hide();
                $(".phieu-nap-text").show();
                $(".phieu-rut-text").hide();
                $('.onNap').text('Nạp');
                $('.chooseAction').hide()
            }
        });
        $('#basic').popup();
        $('#fade').popup();

        $('#phone_fix_dialog').popup();
        $('#bankinfo').popup({
            blur:false,
            beforeopen:function(){
                // $("#invoice-content").html("<p>content here!!!!</p>");

            }
        });



    });
    function show_invoice_form(){
        //$(".form-controlCustom").hide();
//    $(".frm-invoice-panel").show();
        var a = $('.onNap').text();
        if(a == 'Nạp'){
            $('#formChooseBankPull').hide();
            $('#formChooseBankPush').show();
        }else{
            $('#formChooseBankPush').hide ();
            $('#formChooseBankPull').show();
        }
    }
</script>
</body>
</html>
