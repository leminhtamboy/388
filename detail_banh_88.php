<?php
@session_start();
include_once("Article.php");
include_once("Menu.php");
include_once("Attribute.php");
include_once("AttributeValue.php");
include_once("processingImage.php");
include_once("Config.php");
include_once("Giaodich.php");
$config= new ConfigGlobal("config","config_id");
$menu  = new Menu("menu","id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$article = new Article("article","id_article");
$attributeValue= new AttributeValue("entity_attribute_value_varchar","id_eav_varchar");
?>
<!doctype html>
<!--[if lt IE 9]> <html class="no-js ie-old" lang="vi"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="vi"> <![endif]-->
<!--[if gt IE 8]> <html lang="vi"> <!--<![endif]-->
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<?php include_once("head.php"); ?>
<body>
<div class="home-page-edit">
    <?php include_once("header.php"); ?>
    <div class="main-content">
        <div class="container">
            <div id="pSport">
                <div class="left_ct">
                    <img class="img-default" src="https://img.388bet.com/images/soccers1/Scorer_55.jpg" alt="">
                    <img class="img-latest" src="https://img.388bet.com/images/soccers1/Scorer_11.jpg" style="">
                    <img class="img-latest" src="https://img.388bet.com/images/soccers1/Scorer_22.jpg" style="">
                    <img class="img-latest" src="https://img.388bet.com/images/soccers1/Scorer_33.jpg" style="">
                    <img class="img-latest" src="https://img.388bet.com/images/soccers1/Scorer_44.jpg" style="">
                </div>
                <style>
                    .left_ct img{width:290px;margin-top:50px;}
                    strong {  font-weight:bold !important;}
                </style>
                <div class="right_ct">
                    <div class="breadcrumb-wrap">
                        <ul class="breadcrumb breadcrumb-cus">
                            <li><a href="<?php echo ConfigGlobal::$realPath; ?>">Trang chủ</a></li>
                            <li><a href="<?php echo  ConfigGlobal::$realPath ?>/tai-khoan-dung-thu-banh-88.html">Hướng dẫn chơi Banh888</a></li>
                        </ul>
                    </div>
                    <h1 class="content_header title-details">
                        <span class="triangle-left" style="left: -12px;"></span>
                        <span class="txt-title">Tài khoản dùng thử và hướng dẫn chơi</span>
                        <span class="triangle-right"></span>
                    </h1>
                    <div class="post-details-ext">
                        <div>Đảm bảo việc cá cược của người chơi là hợp pháp, bạn không cá cược trực tiếp trên 88Cuoc. Chúng tôi đóng vai trò quản lý các giao dịch nạp - rút tiền và cung cấp tài khoản để người chơi tham gia trên các thị trường cá cược hàng đầu Châu Á hiện nay.</div>
                        <div>&nbsp;</div>

                        <div><strong>Banh888&nbsp;</strong></div>

                        <div>&nbsp;</div>

                        <div>
                            Banh888 là thị trường thể thao mới đầy tiềm năng cho giới đam mê cá độ bóng đá, thể thao, casino , lô đề, keno, lô đề, slots. Giao diện trang đẹp mắt, dễ truy cập, không cần tài khoản vẫn có thể vào trang để xem trận và kèo, phù hợp thị hiếu của thị trường cá cược Việt Nam. Banh888 cung cấp đầy đủ các môn thể thao, các giải đấu lớn nhỏ mỗi ngày để người chơi có thể tham gia dự đoán và đặt cược.
                        </div>

                        <div>&nbsp;</div>

                        <div style="text-align: center;"><img alt="" src="<?php echo ConfigGlobal::$realPath ?>/image/odd_banh888_bg.png" style="width: 700px; height: 363px;"></div>

                        <div style="text-align: center;"><cite>Giao diện trang cược banh888</cite></div>

                        <div>&nbsp;</div>
                        <div>&nbsp;</div>
                        <div>
                            Số đề: sáng cược chiều sổ, bao lô, bao xô. Banh888 đánh lô online với tỷ lệ ăn cược rất cao và được đánh giá là có tỷ lệ ăn cao nhất trong tất cả các nhà cái đánh đề online tại Việt Nam hiện nay.
                        </div>
                        <div style="text-align: center;"><img alt="" src="<?php echo ConfigGlobal::$realPath ?>/image/huong_dan_banh_88_1.png" style="width: 700px; height: 363px;"></div>

                        <div style="text-align: center;"><cite>Giao diện trang số đề</cite></div>
                        <div>&nbsp;</div>
                        <div>&nbsp;</div>
                        <div>
                            Đánh bài casino online: với đầy đủ các thể loại các bài được yêu thích nhất hiện nay như xóc đĩa, tài xỉu xí ngầu, xì tố Poker, Xì dách Blackjack, Baccarat, Cò quay Roulette,…
                        </div>
                        <div style="text-align: center;"><img alt="" src="<?php echo ConfigGlobal::$realPath ?>/image/huong_dan_banh_88_2.png" style="width: 700px; height: 363px;"></div>
                        <div style="text-align: center;"><cite>Giao diện trang casino có bộ môn hot Xóc Đĩa</cite></div>
                        <div>&nbsp;</div>
                        <div>&nbsp;</div>
                        <div>
                            KENO là một trong những trò chơi trực tuyến đang thu hút đông đảo người chơi hiện nay. Nó có nguồn gốc từ Trung Quốc và cách chơi giống như trò chơi xổ số hay loto, bingo tại Việt Nam.

                            Keno có cách chơi rất đơn giản, người chơi chỉ cần lựa chọn ngẫu nhiên từ 1-10 con số trong các số từ 1 đến 80. Sau đó, nhà cái sẽ rút ngẫu nhiên 20 số trong các con số từ 1 đến 80, con số được rút ngẫu nhiên từ hệ thống sẽ được so với con số mà người chơi lựa chọn và đó cũng chính là kết quả của trò chơi.
                        </div>
                        <div style="text-align: center;"><img alt="" src="<?php echo ConfigGlobal::$realPath ?>/image/huong_dan_banh_88_3.png" style="width: 700px; height: 363px;"></div>
                        <div style="text-align: center;"><cite>Giao diện keno</cite></div>
                        <div>&nbsp;</div>
                        <style>
                            .tk {
                                width: 700px;
                                overflow: hidden;
                                padding-left: 10px;
                            }
                            .tk ul.tkul {
                                margin-left: 0px !important;
                            }
                            .tk ul {
                                width: 153px;
                                float: left;
                                margin: 0px;
                                padding: 0;
                                margin-left: 25px;
                            }
                            .entry ul, .comment-content ul {
                                list-style-type: disc;
                            }
                            ol, ul {
                                list-style: none;
                            }
                            .tk ul li.den {
                                background: white;
                                height: 25px;
                                line-height: 25px;
                                border:1px solid black;
                                box-sizing: border-box;
                            }
                            .tk ul li.trang {
                                background: white;
                                height: 25px;
                                line-height: 25px;
                                border:1px solid black;
                                box-sizing: border-box;
                            }
                            .tk ul li {
                                list-style: none;
                                padding-left: 0px;
                                color: #878787;


                            }
                            .tk ul li {
                                list-style: none;
                                padding-left: 0px;
                                color: #878787;
                            }
                            /*.tk ul li:nth-child(odd){
                                border-right:none;
                            }
                            .tk ul li:nth-last-child(2),
                            .tk ul li:nth-last-child(2) ~ .tk ul li{
                                border-bottom:1px solid black
                            }*/
                        </style>
                        <h2><strong>Tài khoản dùng thử banh888</strong></h2>
                        <div>&nbsp;</div>
                        <div style="text-align: center;">

                            <center>
                                <div class="tk">
                                    <ul class="tkul">
                                        <li class="den">
                                            <ul class="tkul">
                                                <li class="den">889win01</li>
                                            </ul>
                                        </li>
                                        <li class="den">889win02</li>
                                        <li class="trang">889win03</li>
                                        <li class="den">889win04</li>
                                    </ul>
                                    <ul>
                                        <li class="den">889win05</li>
                                        <li class="trang">889win06</li>
                                        <li class="den">889win07</li>
                                        <li class="trang">889win08</li>
                                    </ul>
                                    <ul class="mbf">
                                        <li class="den">889win11</li>
                                        <li class="trang">889win22</li>
                                        <li class="den">889win33</li>
                                        <li class="trang">889win55</li>
                                    </ul>
                                    <ul>
                                        <li class="den">889win66</li>
                                        <li class="trang">889win77</li>
                                        <li class="den">889win88</li>
                                        <li class="trang">889win99</li>
                                    </ul>
                                </div>
                                <p></p>
                                Link đăng nhập mạng banh888 của Casino889
                                Chú ý: tất cả các tài khoản có mật khẩu là Casino889  chữ  ” C  ” viết hoa
                            </center>
                        </div>
                        <div class="">&nbsp;</div>
                        <div>Quý khách vào banh888 theo link: <a href="https://www.banh888.com" target="_blank">www.banh888.com</a>&nbsp;---&gt; Điền tên đăng nhập + mật khẩu để có thể vào xem giao diện trang.</div>

                        <div>&nbsp;</div>

                        <div style="text-align: center;"><em><i>Điền tên đăng nhập và mật khẩu vào ô tương ứng để truy cập trang cược banh888</i></em></div>

                        <div>&nbsp;</div>

                        <div>&nbsp;</div>
                        <div><strong>2. Hoa hồng và Mức Quy Đổi Điểm&nbsp;</strong></div>
                        <div>&nbsp;</div>
                        <div>&nbsp;</div>
                        <div>
                            <ul>
                                <li><span style=" ">Thành Viên <span style="color: #ffcc00;">mạng banh888</span> chọn mức độ&nbsp;<span style="color: #ffa500;">100.000</span>&nbsp;VND hoa hồng được 1.25% &nbsp;được cộng trực tiếp vào từng mã cược 0,25% và 1% phần còn lại vào tài khoản ca cuoc bong da</span><span style=" ">, được tính 1 tuần 1 lần trước 18h ngày thứ 7 hàng tuần, ngoài ra còn được hoàn trả 3% tiền thua hàng tuần.</span></li>
                                <p></p>
                                <li><span style=" ">Thành viên mạng banh888 chọn mức độ&nbsp;<span style="color: #ffa500;">50.000</span>&nbsp;VND hoa hồng được 1,25% &nbsp;được cộng trực tiếp vào từng mã cược 0,25% và 1% phần còn lại vào tài khoản cá cược, được tính 1 tuần 1 lần trước 18h &nbsp;ngày&nbsp;thứ 7 hàng tuần.</span></li>
                                <p></p>
                                <li><span style=" ">Thành viên mạng banh888 chọn mức độ&nbsp;<span style="color: #ffa500;">30.000</span>&nbsp;VND hoa hồng được 1% &nbsp;được cộng trực tiếp vào từng mã cược 0,25% và cộng 0,75% phần còn lại vào tài khoản cá cược, được tính 1 tuần 1 lần trước 18h&nbsp;&nbsp;ngày&nbsp;thứ 7 hàng tuần.</span></li>
                                <p></p>
                                <li><span style=" ">Theo thống kê của chúng tôi ,tất cả phần thưởng của những chương trình khuyến mãi các công ty khác đưa ra cộng lại cũng chưa bằng hoa hồng của 88Cuoc tặng cho khách hàng.</span></li>
                                <p></p>
                                <li class="cuoi"><span style="color: #ff0000;"><em><span style=" font-size: 11pt; color: #ffa500;">Thời gian từ 11 đến 14h các mạng thường bảo trì nên không kích hoạt được tài khoản banh888 trong thời gian này.</span></em></span></li>
                            </ul>
                        </div>
                        <div>&nbsp;</div>
                        <div><strong>3. Hướng Dẫn Chơi</strong></div>
                        <div>&nbsp;</div>
                        <div class="page-content">
                            <p style="text-align: justify;">Có cập nhật trong trang cược banh888 hoặc liên hệ tổng đài viên để biết thêm chi tiết.</p>
                            <div>&nbsp;</div> </div>
                        <div>&nbsp;</div>
                        <div>
                            <div>&nbsp;</div>
                        </div>
                        <div style="text-align: center;">Mọi thắc mắc quý khách hàng có thể liên hệ bộ phận support để được giải đáp.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function(){
            $(function() {
                var act=0;
                if(window.location.hash) {
                    act =parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs({active: act});
            });
            $(window).on('hashchange', function() {
                var act=0;
                if(window.location.hash) {
                    act = parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs( "option", "active", act );
            });

        });
    </script>
    <?php include_once("footer.php") ?>
</div>
<script type="text/javascript">


    var imgArr=['<?php echo ConfigGlobal::$realPath ?>/image/home_jalan_vi-VN.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/ad_fastmarket.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/banner3_en.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/cock_fight.jpg'];
    var numberSilider =Math.floor((Math.random() * imgArr.length));
    //alert(imgArr);
    var url =imgArr[numberSilider];
    $('#imagesRandom').attr('src',url);

    $(document).ready(function(){
        $('.onNap').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            if(a == 'Nạp'){
                $('.onRut').text('Rút');
                $("#lb-naprut-caption").text("Rút tiền");

            }
            else{
                $('.onRut').text('Nạp');
                $("#lb-naprut-caption").text("Nạp tiền");
            }
            $('.chooseAction').toggle()

        })
        $('.onRut').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            //show_invoice_form();
            if(a == 'Nạp'){
                $(".form-controlCustom").show();
                $('#formChooseBankPull').hide();
                $('#formChooseBankPush').hide();
                $(".phieu-nap-text").hide();
                $(".phieu-rut-text").show();
                $('.onNap').text('Rút');
                $('.chooseAction').hide()
            }else{
                $(".form-controlCustom").show();
                $('#formChooseBankPush').hide();
                $('#formChooseBankPull').hide();
                $(".phieu-nap-text").show();
                $(".phieu-rut-text").hide();
                $('.onNap').text('Nạp');
                $('.chooseAction').hide()
            }

        });
        $('#basic').popup();
        $('#fade').popup();

        $('#phone_fix_dialog').popup();
        $('#bankinfo').popup({
            blur:false,
            beforeopen:function(){
                // $("#invoice-content").html("<p>content here!!!!</p>");

            }
        });



    });
    function show_invoice_form(){
        //$(".form-controlCustom").hide();
//    $(".frm-invoice-panel").show();
        var a = $('.onNap').text();
        if(a == 'Nạp'){
            $('#formChooseBankPull').hide();
            $('#formChooseBankPush').show();
        }else{
            $('#formChooseBankPush').hide ();
            $('#formChooseBankPull').show();
        }
    }
</script>
</body>
</html>
