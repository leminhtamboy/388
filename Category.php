<?php
include_once("Collection.php");
class Category extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
    function getCategoryIsActive(){
        $sql="select * from category where is_active=1";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
}