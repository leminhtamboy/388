<div class="footer footer-custom">
    <div class="container">
        <div class="des-footer help">
            <h3>Trợ giúp</h3>
            <ul>
                <li><a href="<?php echo ConfigGlobal::$realPath ?>/huong-dan.html">Hướng dẫn sử dụng</a></li>
                <li><a href="<?php echo ConfigGlobal::$realPath ?>/about-us.html">Về chúng tôi</a></li>
            </ul>
        </div>

        <div class="des-footer cash">
            <h3>Phương Thức Thanh Toán</h3>
            <ul>
                <li><a href="#">DongA Bank - Sacombank</a></li>
                <li><a href="#">Vietcombank - Techcombank</a></li>
                <li><a href="#">ACB - Vietinbank</a></li>
                <li><a href="#">BIDV - Agribank</a></li>
            </ul>
        </div>
        <div class="des-footer help-2">
            <h3>Liên hệ</h3>
            <ul>
                <li><a href="#">Email: banh888vn@gmail.com</a></li>
                <li><a href="#">Sdt: 0933.00.1088</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="bottom-des">
</div>
<style>
    .footer-custom .des-footer{
        text-align:center;
        width:33.33333%;
        padding-left:0;
    }
</style>

<script type="text/javascript">
    (function() {
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement("style");
            msViewportStyle.appendChild(
                document.createTextNode("@-ms-viewport{width:1024px!important}")
            );
            document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
        }
    })();
</script>
<!-- Start of LiveChat (www.livechatinc.com) code -->
<!--Add the following script at the bottom of the web page (before </body></html>)-->
<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 9569415;
    (function() {
        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
</script>
<!-- End of LiveChat code -->
<!-- End of LiveChat code -->
<noscript>
    <img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=SvDln1a4KM104B" style="display:none" height="1" width="1" alt="" />
</noscript>
<div class="modal fade" id="popupModalInfoBank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <a type="button" class="closeCustom" data-dismiss="modal"></a>
            <div class="dialogContent">
                <h2 >Thông tin ngân hàng</span></h2>
            </div>
            <div class="contentAllBank">
                <span class="headerTitleBank"> Chúng tôi có các tài khoản Ngân hàng dưới đây:</span>
                <img src="https://static.388bet.com/images/infoBankItem.jpg">
                <span class="footerBank">
            Lưu ý:<br>
            - Tài khoản được thay đổi hàng quý <br>
            - Chuyển khoản củng hệ thống sẽ nhận được tiền nhanh <br>
            </span>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="popupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <a type="button" class="closeCustom" data-dismiss="modal"></a>
            <div class="dialogContent">
                <h2><span>Bạn có thể gửi tiền qua</span></h2>
                <div class='custom-bank-send'>
                    <div class='custom-bank'>
                        <div class="allLogoBank">
                            <img src="https://static.388bet.com/images/infoSendMooney.jpg">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>