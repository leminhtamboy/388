<?php
@session_start();
include_once("Article.php");
include_once("Menu.php");
include_once("Attribute.php");
include_once("AttributeValue.php");
include_once("processingImage.php");
include_once("Config.php");
$config= new ConfigGlobal("config","config_id");
$menu  = new Menu("menu","id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$article = new Article("article","id_article");
$attributeValue= new AttributeValue("entity_attribute_value_varchar","id_eav_varchar");
$processImage = new  processingImage();
$_SESSION["allow_me"] = "0";
?>
<!doctype html>
<!--[if lt IE 9]> <html class="no-js ie-old" lang="vi"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="vi"> <![endif]-->
<!--[if gt IE 8]> <html lang="vi"> <!--<![endif]-->
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<?php include_once("head.php"); ?>
<body>
<div class="home-page-edit">
    <?php include_once("header.php"); ?>
    <script src="<?php  echo ConfigGlobal::$realPath ?>/js/jssor.slider.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {
            var jssor_1_options = {
                $AutoPlay: 1,
                $SlideDuration: 800,
                $SlideEasing: $Jease$.$OutQuint,
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /*jssor slider loading skin spin css*/
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }

        /*jssor slider bullet skin 032 css*/
        .jssorb032 {position:absolute;}
        .jssorb032 .i {position:absolute;cursor:pointer;}
        .jssorb032 .i .b {fill:#fff;fill-opacity:0.7;stroke:#000;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.25;}
        .jssorb032 .i:hover .b {fill:#000;fill-opacity:.6;stroke:#fff;stroke-opacity:.35;}
        .jssorb032 .iav .b {fill:#000;fill-opacity:1;stroke:#fff;stroke-opacity:.35;}
        .jssorb032 .i.idn {opacity:.3;}

        /*jssor slider arrow skin 051 css*/
        .jssora051 {display:block;position:absolute;cursor:pointer;}
        .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
        .jssora051:hover {opacity:.8;}
        .jssora051.jssora051dn {opacity:.5;}
        .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
    </style>
    <div class="main-content">
        <div class="slider" style="display: block;margin: 0 auto 10px;max-width: 100%;">
            <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1349px;height:395px;overflow:hidden;visibility:hidden;">
                <!-- Loading Screen -->
                <div data-u="loading" class="jssorl-009-spin" id="something" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                    <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo ConfigGlobal::$realPath ?>/image/spin.svg" />
                </div>
                <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1349px;height:395px;overflow:hidden;">
                    <div>
                        <img data-u="image" src="<?php echo ConfigGlobal::$realPath ?>/image/Banner3.jpg" />
                    </div>
                    <div>
                        <img data-u="image" src="<?php echo ConfigGlobal::$realPath ?>/image/Banner1.jpg" />
                    </div>
                    <div>
                        <img data-u="image" src="<?php echo ConfigGlobal::$realPath ?>/image/Banner2.jpg" />
                    </div>
                    <div>
                        <img data-u="image" src="<?php echo ConfigGlobal::$realPath ?>/image/Banner4.jpg" />
                    </div>
                </div>
                <!-- Bullet Navigator -->
                <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                    <div data-u="prototype" class="i" style="width:16px;height:16px;">
                        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                        </svg>
                    </div>
                </div>
                <!-- Arrow Navigator -->
                <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                    <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                    </svg>
                </div>
                <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                    <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                    </svg>
                </div>
            </div>
            <script type="text/javascript">jssor_1_slider_init();</script>
        </div>
        <div class="step-1 container">
            <?php if(isset($_SESSION["member"])){ ?>
                <div class="step-success">
                    <!-- <span class="users-icon"><i class="icon-users"></i></span> -->
                    <span class="users-icon">Bước<number>1</number></span>
                    <span class="step-tk">tài khoản</span>
                    <span class="user-email"><i class="iconEmail"></i><strong>Tên đăng nhập:</strong> <?php echo $_SESSION["sdt"] ?></span>
						<span class="user-price">
                            <i class="iconMooney"></i><strong>Giá:</strong> Linh động <i class="iconQuestion">
							    <p id="hint-question">Bạn nhờ hỗ trợ viên thay đổi giá nếu cần</p>
						    </i>
                        </span>
                    <span class="next-resgister">
                        <a href="<?php echo ConfigGlobal::$realPath ?>/logout.php">Đăng xuất<i class="icon-next-sm"></i></a></span>
                </div>
            <?php }else{ ?>
                <div class="step-register">
                    <form action="reg_account.php" id="frm-quick-regis" method="post" autocomplete="off">
                        <!-- <span class="users-icon"><i class="icon-users"></i></span> -->
                        <span class="users-icon">Bước<number>1</number></span>
                        <span class='step-dk'>Đăng ký</span>
                            <span class='user-input none-login'>
                                <span id="phone_error" class="infoVadilate" style="display:none;top:-58px;">Tên người dùng không hợp lệ</span>
                                <input maxlength="100" minlength="5" type="text" name="phone" autocomplete="off"  id="txt-phone"  placeholder='Tên đăng nhập - bắt buộc'>
                            </span>
                            <span class='user-input none-login'>
                                <span id="password_error" class="infoVadilate" style="display:none;top:-58px;">Mật khẩu quá ngắn</span>
                                <input type="password" name="pass"  id="txt-password"  placeholder='Mật khẩu - bắt buộc'>
                            </span>
                            <span class='user-input none-login'>
                                <span id="email_error" class="infoVadilate" style="display:none;top:-58px;">Email đăng ký không hợp lệ</span>
                                <input type="text" name="user" autocomplete="off" id="txt-email" placeholder='Nhập Email - Không bắt buộc'>
                            </span>
                        <span class="next-resgister none-login"><a onclick=" $('#frm-quick-regis').submit();return false;" href="#"><i class='icon-next-resgister'></i></a></span>
                        <input type="hidden" value="1" name="btnSubmit"/>

                        <span class="resgistersucces have-login" style="display:none;">Đăng ký thành công</span>
                        <span class="autoRedirect have-login" style="display:none;">tự động đăng nhập trong vòng 3s...</span>

                        <input type="submit" value="OK" name="btnSubmit" style="display:none;"/>
                        <script>
                            $("#txt-password,#txt-email,#txt-phone").on("keyup", function () {
                                check_error(false);
                            });
                            function validateEmail(email) {
                                var re = /^[a-zA-Z0-9\._-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/igm;
                                return re.test(email);
                            }
                            function check_error(show_error) {
                                var txtpass = $("#txt-password").val(), txtuser = $("#txt-email").val(), txtphone = $("#txt-phone").val();
                                /*if (txtuser.length < 3 || !validateEmail(txtuser)) {
                                    if (show_error) {
                                        $("#email_error").show();
                                        return false;
                                    }
                                } else {
                                    $("#email_error").hide();
                                }*/
                                if (txtpass.length < 6) {
                                    if (show_error) {
                                        $("#password_error").show();
                                        return false;
                                    }
                                } else {
                                    $("#password_error").hide();
                                }
                                var phoneRe = /^\+?[0-9]+$/;
                                if (txtphone.length < 5 || txtphone.length > 100 /*|| !phoneRe.test(txtphone)*/) {
                                    if (show_error) {
                                        $("#phone_error").show();
                                        return false;
                                    }
                                } else {
                                    $("#phone_error").hide();
                                }
                                return true;
                            }
                            $("#frm-quick-regis").submit(function () {
                                if (!check_error(true))
                                    return false;
                                var txtpass = $("#txt-password").val(), txtuser = $("#txt-email").val(), txtphone = $("#txt-phone").val();
                                $.ajax({url: '<?php echo ConfigGlobal::$realPath ?>/ajax_register.php',
                                    method: "POST",
                                    data: $("#frm-quick-regis").serialize(),
                                    success: function (data) {
                                        console.log(data);
                                        if(data == 2)
                                        {
                                            alert("Đã tồn tại tên đăng nhập này. Xin vui lòng dùng tên đăng nhập khác");

                                        }else{
                                            $("#user").val(txtphone);
                                            $("#password").val(txtpass);
                                        }
                                        $(".none-login").hide();
                                        $(".have-login").show();
                                        setTimeout(function () {
                                            $("#btnSubmitLogin").click();
                                        }, 3000);

                                    }});
                                return false;
                            });
                        </script>
                    </form>
                    <!--<p>* Nên có email để tiện cho việc khôi phục mật khẩu</p>-->
                </div>
            <?php } ?>
            <div class="step-1-left">
                <h2><a target="_blank" onclick="popbox('https://secure.livechatinc.com/licence/6962071/open_chat.cgi')" href="#" class="icon-voice"></a>chat với tổng đài viên</h2>
                <span class="des-step-1">Chúng tôi làm việc 24/7. Trò chuyện qua <a target="_blank" onclick="popbox('https://secure.livechatinc.com/licence/6962071/open_chat.cgi')" style="color:#000;border-bottom:1px solid #d8242b" href="#">LIVE CHAT</a> hoặc 0933.00.1088</span>
            </div>
        </div>


        <div class="step-2 container">
            <div class='step'><span> Bước <number>2</number></span></div>
            <h2><i class="icon-list"></i>lập phiếu
                <div class="NAPRUT">
                    <a class='onNap' href="#">Nạp</a>
                </div>
                <select style='display:none' class='cash Oncash' onchange="changeOption(this)">
                    <option value="nạp">Nạp</option>
                    <option value="Rút">Rút</option>
                </select>tiền</h2>
            <span class="des-step-2 phieu-nap-text">CHUYỂN TIỀN TỪ TÀI KHOẢN NGÂN HÀNG CỦA BẠN VÀO 88CUOC</span>
            <span class="des-step-2 phieu-rut-text" style="display:none;">LẬP PHIẾU RÚT TIỀN, ĐỢI CHÚNG TÔI RÚT ĐIỂM VÀ CHUYỂN KHOẢN</span>
            <div class="form-controlCustom">
                <a href="#" onclick="
                <?php if(!isset($_SESSION["member"])){ ?>
                    alert('Bạn chưa đăng nhập');return false;
                <?php }else{
                ?>
                    $('.frm-invoice-panel').show();
                    $('#formChooseBankPush').show();
                    return false;
                <?php
                } ?>
                    " class='btnCustom btn-point bankinfo_open'>điền thông tin</a>
            </div>
            <?php if(isset($_SESSION["member"])){  ?>
                <?php
                    include_once 'config_nganluong.php';
                    include_once 'lib/nganluong.class.php';
                ?>
                <?php
                $showError = false;
                if (isset($_POST['naptien'])) {
                    ?>
                    <script>
                        alert("Bạn vui lòng chờ trong giây lát. Trình duyệt tự động chuyển sang trang ngân hàng. Vui lòng không F5 hay làm mới trình duyệt. Xin cảm ơn.");
                    </script>
                    <?php
                    // Lấy các tham số để chuyển sang Ngânlượng thanh toán:
                    //$ten= $_POST["txt_test"];
                    $receiver=RECEIVER;
                    //Mã đơn hàng
                    $order_code='NL_'.time();
                    //Khai báo url trả về
                    $return_url= $_SERVER['HTTP_REFERER']. "success.php";
                    // Link nut hủy đơn hàng
                    $cancel_url= $_SERVER['HTTP_REFERER']. "success.php";
                    //Giá của cả giỏ hàng
                    $txh_name =$_POST['sender'];
                    $txt_email =$dataMoneyMember->getemail();
                    if($txt_email == ""){
                        $txt_email = $txh_name."@gmail.com";
                    }
                    $txt_phone ="";
                    $price = str_replace(".","",$_POST['amount']);
                    //Thông tin giao dịch
                    $transaction_info="Thong tin giao dich";
                    $currency= "vnd";
                    $quantity=1;
                    $tax=0;
                    $discount=0;
                    $fee_cal=0;
                    $fee_shipping=0;
                    $order_description="Thong tin don hang: ".$order_code;
                    $buyer_info=$txh_name."*|*".$txt_email."*|*".$txt_phone."*|* không có địa chỉ nhận hàng";
                    $affiliate_code="";
                    if($price < 50000){
                        $showError = true;
                    }else {
                        //Khai báo đối tượng của lớp NL_Checkout
                        $nl = new NL_Checkout();
                        $nl->nganluong_url = NGANLUONG_URL;
                        $nl->merchant_site_code = MERCHANT_ID;
                        $nl->secure_pass = MERCHANT_PASS;
                        //Tạo link thanh toán đến nganluong.vn
                        $url = $nl->buildCheckoutUrlExpand($return_url, $receiver, $transaction_info, $order_code, $price, $currency, $quantity, $tax, $discount, $fee_cal, $fee_shipping, $order_description, $buyer_info, $affiliate_code);
                        if ($order_code != "") {
                            //một số tham số lưu ý
                            //&cancel_url=http://yourdomain.com --> Link bấm nút hủy giao dịch
                            //&option_payment=bank_online --> Mặc định forcus vào phương thức Ngân Hàng
                            $url .= '&cancel_url=' . $cancel_url;
                            $url .= '&option_payment=bank_online';
                        }
                        echo '<meta http-equiv="refresh" content="0; url=' . $url . '" >';
                    }
                }else{

                }
                ?>
            <div class="frm-invoice-panel">
                <div id="formChooseBankPush" class="chooseBank chooseBankCustom">
                    <form id="sendmon" name="sendmon" method="post" class="cmxform form-horizontal tasi-form">
                        <h3 class="chooseBankTile" style="position:relative;">Chọn ngân hàng để nạp tiền
                            <span class="infoVadilate sizeMedium nap_bank_error" style="display:none;font-weight:normal;left:159px;">Vui lòng chọn ngân hàng để nạp tiền</span>
                        </h3>
                        <div class="formContentField">
                            <div class="lineOneForm">
                                <div class="fieldForm twoInput">
                                    <input name="amount" type="text" autocomplete="off" class="customField vadilateCustomField" placeholder="Số tiền nạp" onkeyup="moneyconvert(this);">
                                    <?php if($showError){ ?>
                                        <span class="infoVadilate sizeMedium amount_error" >Số tiền nạp tối thiểu 50.000 vnd.</span>
                                    <?php } ?>
                                </div>
                                <div class="fieldForm twoInput">
                                    <input name="sender" type="text" value="<?php echo $_SESSION["sdt"] ?>" class="customField vadilateCustomField" placeholder="Tên người gửi">
                                    <span class="infoVadilate sizeMedium sender_error" style="display:none;">Tên người gửi không hợp lệ. </span>
                                </div>
                            </div>
                            <div class="lineOneForm">
                                <div class="fieldForm twoInput">
                                    <div class="customSelect">
                                        <select id="phuong_thuc_giao_dich" onchange="gchange(this.value);" name="phuong_thuc_giao_dich" style="display: none">
                                            <option value=""> Phương thức thanh toán </option>
                                            <option value="QuaInternetBanking" selected="selected"> Gửi internet banking</option>
                                            <option value="QuaATM"> Gửi ATM</option>
                                            <option value="NopTienMat"> Gửi tiền mặt tại quầy</option>
                                        </select>
                                    </div>
                                <span class="infoVadilate sizeLarge phuong_thuc_giao_dich_error" style="display:none;">Chọn phương thức thanh toán</span>
                                </div>
                                <div class="fieldForm twoInput">
                                    <!--<input type='text' class='customField vadilateCustomField' placeholder='Số tài khoản'>-->
                                    <div class="customSelect" id="bno_opt">
                                        <select name="bno" style="display: none;">
                                            <option>Chọn tài khoản nhận</option>
                                            <option selected="selected" value="0103246596">TEST</option>
                                        </select>
                                    </div>
                                    <span class="infoVadilate bno_error" style="display:none;">Chọn tài khoản nhận</span>
                                </div>
                            </div>
                            <div class="lineTwoForm" style="display: none;">
                                <div class="fieldForm twoInput" style="display: none">
                                    <input id="transaction_bankcode" autocomplete="off" name="transcode" type="text" class="customField vadilateCustomField" placeholder="Mã giao dịch">
                                    <span class="infoVadilate transcode_error" style="display:none;"></span>
                                </div>
                                <div class="fieldForm twoInput">
                                    <div class="customSelect">
                                        <select id="account_bet" name="account_bet">
                                            <option value="">Chọn tài khoản cá cược</option>
                                            <option value="Tạo tài khoản Bong88 mới"> Tạo cho tôi tài khoản dùng thử Bong88</option>
                                            <option value="Tạo tài khoản 8bong mới"> Tạo cho tôi tài khoản dùng thử Banh88</option>
                                            <option value="Tạo tài khoản 8bong mới"> Tạo cho tôi tài khoản dùng thử S128</option>
                                            <option value="Tạo tài khoản 8bong mới"> Tạo cho tôi tài khoản dùng thử SV388</option>
                                        </select>
                                    </div>
                                <span class="infoVadilate sizeLarge account_bet_error" style="display:none;">Vui lòng chọn tài khoản cá cược của bạn</span>
                                </div>
                            </div>
                            <div class="actionChooseBank">
                                <input type="submit" name="naptien" style="height:50px" value="Nạp Tiền" class="btnCustom"/>
                            </div>
                        </div>
                    </form>
                </div>
                <style>
                    .infoVadilate.sizeMedium,.infoVadilate.sizeLarge{
                        bottom:41px;
                        top:auto;

                    }
                </style>
            </div>
            <?php } ?>
            <style>
                .customSelect select{
                    text-indent:7px;
                    line-height:31px;
                }
            </style>
        </div>
        <div class="step-3 container">
            <div class="step"><span> Bước <number>3</number></span></div>
            <h2 class="oneMin"><i class="icon-Time"></i>1 phút - Nhanh chóng</h2>
            <span class="des-step-2">Bạn không cá cược trực tiếp trên 88Cuoc, bạn không vi phạm pháp luật</span>
            <span class="des-step-2">Sau khi nạp tiền, đợi support cung cấp tài khoản cá cược</span>
            <strong><img src="https://static.388bet.com/images/tai.png"></strong>
            <span class="lineIn"></span>
            <div class="step-3-left">
                <img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_bong88.png" style="margin: 25px 100px;"/>
                <div class="link-redirect">
                    <a class="new-link" target="_blank" href="<?php echo ConfigGlobal::$realPath; ?>/tai-khoan-dung-thu-bong-88.html">Link demo</a>
                    <a class="home-page" target="_blank" href="http://www.bong88.com/">trang chủ</a>
                </div>
            </div>
            <div class="step-3-right">
                <img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_banh888_new.png" style="margin: 12px 150px;width: 190px"/>
                <div class="link-redirect">

                    <!--<a class="new-link" target="_blank" href="<?php echo ConfigGlobal::$realPath ?>/tai-khoan-dung-thu-banh-88.html">Link demo</a>
                    <a class="home-page" target="_blank" href="#">trang chủ</a>-->
                    <a class="new-link" href="#" onclick="alert('Banh888.com chúng tôi sẽ sớm phát hành trong thời gian sớm nhất. Cám ơn sự quan tâm của quý khách')">Link demo</a>
                    <a class="home-page" href="#" onclick="alert('Banh888.com chúng tôi sẽ sớm phát hành trong thời gian sớm nhất. Cám ơn sự quan tâm của quý khách')">trang chủ</a>
                </div>
            </div>
            <div class="step-3-left">
                <img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_288.png" style="margin: 25px 100px;width: 190px;height:55px"/>
                <div class="link-redirect">
                    <a class="new-link" target="_blank" href="<?php echo ConfigGlobal::$realPath ?>/tai-khoan-dung-thu-sv128.html">Link demo</a>
                    <a class="home-page" target="_blank" href="http://www1.s128.net/Landing.aspx?">trang chủ</a>

                </div>
            </div>
            <div class="step-3-right">
                <img src="<?php echo ConfigGlobal::$realPath ?>/image/logo_388.png" style="margin: 25px 100px;"/>
                <div class="link-redirect">

                    <a class="new-link" target="_blank" href="<?php echo ConfigGlobal::$realPath ?>/tai-khoan-dung-thu-sv388.html">Link demo</a>
                    <a class="home-page" target="_blank" href="https://www.sv388.com/index.jsp?722556214">trang chủ</a>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
    <?php include_once("footer.php") ?>
</div>
</body>
</html>
