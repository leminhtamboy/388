<?php
include_once("Article.php");
include_once("Menu.php");
include_once("Attribute.php");
include_once("AttributeValue.php");
include_once("Config.php");
include_once("Category.php");
$menu  = new Menu("menu","id");
$config= new ConfigGlobal("config","config_id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$article = new Article("article","id_article");
$attributeValue= new AttributeValue("entity_attribute_value_varchar","id_eav_varchar");
$category = new Category("category","id");
$dataCategory=$category->getCategoryIsActive();
$dataCollectionArticle=null;
$countPage=0;
if(isset($_REQUEST["id_category"])){
    $idCurrentCategory=$_REQUEST["id_category"];
    $dataCollectionArticle=$article->loadByAttribute("has_category","eq",$idCurrentCategory,0);
}else{
    $allArticle=$article->getCollection();
    $allPage=count($allArticle);
    $limit=12;
    $start=0;
    $end=12;
    $countPage=intval($allPage/$limit);
    if(isset($_REQUEST["page"])){
        $page=$_REQUEST["page"];
        if($requestPage==1){
            $start=0;
            $end=$limit;
        }else{
            $end=$page * ($limit + 1);
            $start=$last-($limit+1);
        }
    }
    $dataCollectionArticle=$article->getCollectionArticleOnPgae($start,$end);
}
//phân trang


?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<?php include_once ("head.php")?>
<body>
<div class="container">
    <?php include_once("header.php"); ?>
    <div class="page-path row">
        <a href="<?php echo ConfigGlobal::$realPath ?>">HomePage</a>
         >>
        <a href="category.html">Category Article</a>
    </div>
    <div id="content" class="row">
        <div class="page-title">
            <h2>Category Article</h2>
        </div>
        <div class="page-cover">
            <div class="flexslider">
                <ul class="slides">
                    <li class="background-image flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2; background-image: url(&quot;<?php echo $config->loadbyPath("background_image_category"); ?>&quot;);">
                        <div class="slide-content">
                        </div>
                    </li>
                </ul>
                <ol class="flex-control-nav flex-control-paging"></ol></div>
        </div>
        <div class="row">
            <div class="tab">
                <ul>
                    <li class="active"><a href="/category.html">All</a></li>
                    <?php foreach($dataCategory as $cate){ ?>
                    <li><a href="/category.html?id_category=<?php echo $cate->getid() ?>"><?php echo $cate->gettitle(); ?></a></li>
                    <?php } ?>
                    </ul>
            </div>
            <div class="page-metas">
                <div class="page-desc col-sm-4 col-xs-6">
                    <b>Show:</b> from 1 to 12 on all of <?php echo count($dataCollectionArticle); ?>
                </div>

                <div class="pagination col-sm-4 col-xs-6">
                    <b>Page </b>
                    <?php for($p=0;$p<$countPage;$p++){ ?>
                        <?php if(isset($_REQUEST["id_category"])){ ?>
                            <a href="/category.html?id_category=<?php echo $_REQUEST["id_category"] ?>&page=<?php echo $p ?>" <?php if($p==$_REQUEST["page"]) echo "class='current'" ?>><?php echo $p ?></a>
                        <?php }else{ ?>
                            <a href="/category.html?page=<?php echo $p ?>" <?php if($p==$_REQUEST["page"]) echo "class='current'" ?>><?php echo $p ?></a>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <ul class="event-list">
                <?php foreach ($dataCollectionArticle as $dataArt) { ?>
                <?php $idEntity=$dataArt->getid_article(); ?>
                <?php $shortDescription=$attributeValue->getValueAttribuetOfEntityByAttributeCode($idEntity,"short_description")[0]->getvalue();  ?>
                <?php $linkSeo=$attributeValue->getValueAttribuetOfEntityByAttributeCode($idEntity,"link_seo")[0]->getvalue();  ?>
                    <?php $nameCategory=$category->load($dataArt->gethas_category())->gettitle(); ?>
                <li class="item">
                    <div class="content">
                        <div class="course-type"><?php echo $nameCategory; ?></div>
                        <a href="/article/<?php echo $linkSeo ?>-<?php echo $idEntity ?>.html" class="name" title="<?php echo $dataArt->gettitle(); ?>">
                            <span><?php echo $dataArt->gettitle(); ?></span>
                        </a>
                        <div class="class-content">
                            <a href="/article/<?php echo $linkSeo ?>-<?php echo $idEntity ?>.html" class="cover">
                                <img width="300" height="300" src="<?php echo $dataArt->getthumbail(); ?>" class="full-width attachment-thumbnail">
                            </a>
                            <div class="description"><?php echo $shortDescription ?></div>
                            <a href="/article/<?php echo $linkSeo ?>-<?php echo $idEntity ?>.html" class="read-more">Read More</a>
                        </div>
                    </div>
                    <!-- <div class="corner-status future">Sắp diễn ra</div>-->
                </li>
                <?php } ?>
            </ul>
            <div class="page-metas">
                <div class="page-desc col-sm-4 col-xs-6">
                    <b>Show:</b> from 1 to 12 on all of <?php echo count($dataCollectionArticle); ?>
                </div>

                <div class="pagination col-sm-4 col-xs-6">
                    <b>Page </b>
                    <?php for($p=0;$p<$countPage;$p++){ ?>
                        <?php if(isset($_REQUEST["id_category"])){ ?>
                            <a href="/category.html?id_category=<?php echo $_REQUEST["id_category"] ?>&page=<?php echo $p ?>" <?php if($p==$_REQUEST["page"]) echo "class='current'" ?>><?php echo $p ?></a>
                        <?php }else{ ?>
                            <a href="/category.html?page=<?php echo $p ?>" <?php if($p==$_REQUEST["page"]) echo "class='current'" ?>><?php echo $p ?></a>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>
    <?php include_once("footer.php"); ?>
</div>
</body>
</html>
