<?php
@session_start();
include_once("Article.php");
include_once("Menu.php");
include_once("Attribute.php");
include_once("AttributeValue.php");
include_once("processingImage.php");
include_once("Config.php");
include_once("Giaodich.php");
$config= new ConfigGlobal("config","config_id");
$menu  = new Menu("menu","id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$article = new Article("article","id_article");
$attributeValue= new AttributeValue("entity_attribute_value_varchar","id_eav_varchar");
?>
<!doctype html>
<!--[if lt IE 9]> <html class="no-js ie-old" lang="vi"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="vi"> <![endif]-->
<!--[if gt IE 8]> <html lang="vi"> <!--<![endif]-->
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<?php include_once("head.php"); ?>
<body>
<div class="home-page-edit">
    <?php include_once("header.php"); ?>
    <style>
        body {
            min-width: 0 !important;
        }

        div.header-breadcrumb {
            text-align: center;
            height: 42px;
            border-bottom: 1px solid #c3c3c3;
            line-height: 42px;
            font-weight: 300;
            font-size: 13px;
        }

        a.header-breadcrumb-link {
            text-decoration: none;
            color: #000000;
        }

        a.header-breadcrumb-link:hover {
            color: #d8242b;
            transition: all 0.75s ease;
        }

        a.header-breadcrumb-link.active {
            border-bottom: 4px solid #d8242b;
            padding-bottom: 9px;
            color: #d8242b;
        }

        div.page-badge-wrapper {
            text-align: center;
            padding-top: 88px;
            padding-bottom: 50px;
        }

        span.page-badge {
            background-color: #d8242b;
            color: #ffffff;
            font-weight: 700;
            font-size: 15px;
            height: 29px;
            width: 186px;
            line-height: 29px;
            display: inline-block;
        }

        div.page-wrapper {
            text-align: center;
        }

        div.page-content-wrapper {
            max-width: 1024px;
            width: 100%;
            display: inline-block;
        }

        div.content-card {
            border: 1px solid #e6e6e6;
            padding-bottom: 50px;
            margin-bottom: 50px;
            margin-left: 10px;
            margin-right: 10px;
        }

        hr.guide-card-dash {
            width: 20px;
            border: 1px solid #d8242b;
            margin-top: 30px;
            margin-bottom: 20px;
        }

        span.content-card-text {
            font-weight: 700;
            font-size: 18px;
            color: #000000;
        }

        a.card-wrapper-link {
            text-decoration: none;
        }

        a.card-wrapper-link:hover span.content-card-text,
        a.card-wrapper-link:hover span.news-title {
            color: #d8242b !important;
            transition: all 0.75s ease;
        }

        div.guide-wrapper {
            padding-top: 35px;
        }

        span.day-title,
        p.article-title {
            font-weight: 700;
            font-size: 55px;
            color: #333333;
        }

        p.article-title {
            margin-left: 10%;
            margin-right: 10%;
            margin-top: -25px;
            line-height: 55px;
            margin-bottom: 30px;
        }

        hr.news-day-title-dash {
            width: 20px;
            border: 1px solid #d8242b;
            margin-top: 5px;
            margin-bottom: 50px;
        }

        img.news-thumb {
            width: 100%;
            height: 190px;
            object-fit: cover;
        }

        div.news-card-thumb-wrapper {
            overflow: hidden;
        }

        div.news-card:hover img {
            /*-moz-transform: scale(1.5);
            -webkit-transform: scale(1.5);
            transform: scale(1.5);
            transition: all 1s ease;*/
        }

        img.guide-icon {
            margin-top: 60px;
        }

        span.news-card-category {
            font-weight: 700;
            font-size: 12px;
            color: #d8242b;
            display: block;
            margin-top: 30px;
            margin-bottom: 15px;
        }

        span.news-title {
            margin-left: 10%;
            margin-right: 10%;
            display: block;
            color: #232323 !important;
        }

        span.news-date {
            font-weight: 400;
            font-size: 14px;
            color: #999999;
        }

        div.news-card {
            padding-bottom: 15px;
        }

        button.news-load-more {
            background: none;
            border: 1px solid #e6e6e6;
            width: 100%;
            height: 50px;
            font-weight: 700;
            font-size: 18px;
            color: #232323 !important;
            margin-left: 10px;
            margin-right: 10px;
        }

        button.news-load-more:hover {
            transition: all 0.75s ease;
            color: #d8242b !important;
        }

        div.load-more-wrapper {
            margin-top: 30px;
            margin-bottom: 135px;
        }

        div.article-content-wrapper {
            text-align: left;
            margin-top: 30px;
            margin-left: 10px;
            margin-right: 10px;
        }

        img.article-cover-image {
            width: 100%;
            height: 500px;
            object-fit: cover;
        }

        p.article-highlight {
            font-weight: 500;
            font-size: 20px;
            line-height: 34px;
            margin: 5%;
            color: #333333;
        }

        div.article-ads {
            margin: 5%;
        }

        div.article-ads img {
            width: 100%;
        }

        p.article-content {
            font-weight: 400;
            font-size: 16px;
            line-height: 28px;
            margin: 3% 5%;
            color: #333333;
        }

        img.article-content-image {
            width: 90%;
            margin-left: 5%;
            height: 500px;
            object-fit: cover;
        }

        div.social-separator {
            margin: 70px 10px;
        }

        hr.social-separator-dash {
            border-bottom: 1px solid #d1d1d1;
            margin-top: 25px;
        }

        a.social-icon-image {
            margin: 0;
            display: inline-block;
        }

        a.social-icon-image div {
            display: inline-block;
            width: 135px;
            height: 50px;
        }

        div.social-icon-facebook {
            background-image: url('../images/icon-facebook.png');
            background-repeat: no-repeat;
            background-position: center;
        }

        div.social-icon-twitter {
            background-image: url('../images/icon-twitter.png');
            background-repeat: no-repeat;
            background-position: center;
        }

        div.social-icon-google {
            background-image: url('../images/icon-google.png');
            background-repeat: no-repeat;
            background-position: center;
        }

        a.social-icon-image:hover div.social-icon-facebook {
            background-image: url('../images/icon-facebook-hover.png');
            transition: all 1s ease;
        }

        a.social-icon-image:hover div.social-icon-twitter {
            background-image: url('../images/icon-twitter-hover.png');
            transition: all 1s ease;
        }

        a.social-icon-image:hover div.social-icon-google {
            background-image: url('../images/icon-google-hover.png');
            transition: all 1s ease;
        }

        div.social-separator-dash-wrapper {
            width: auto;
            overflow: hidden;
            padding-right: 25px;
        }

        div.social-buttons-wrapper {
            text-align: right;
            float: right;
            max-width: 405px;
        }

        div.social-buttons-wrapper a {
            width: 33.33333333%;
        }

        div.social-buttons-wrapper img {
            width: 100%;
        }

        div.related-articles-title {
            font-weight: 700;
            font-size: 30px;
            margin-bottom: 50px;
        }

        @media (max-width: 450px) {
            div.social-separator-dash-wrapper {
                display: none;
            }
        }

        .top-header, .top-header * {
            box-sizing: content-box !important;
            -webkit-box-sizing: content-box !important;
            -moz-box-sizing: content-box !important;
        }

        img.cc-icon-login {
            vertical-align: baseline;
        }

        .container {
            max-width: 1024px;
        }

        div.row-mobile {
            max-width: 750px;
            width: 100%;
        }
        div.guide-wrapper .col-sm-4, .related-articles-wrapper .col-sm-4, .news-day .col-sm-4{
            box-sizing: border-box;
        }
    </style>
    <div class="main-content">
        <div class="header-breadcrumb">
            <a class="header-breadcrumb-link" href="<?php echo ConfigGlobal::$realPath; ?>">TRANG CHỦ</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="header-breadcrumb-link active" href="#">HƯỚNG DẪN</a>
        </div>
        <div class="page-badge-wrapper">
            <span class="page-badge"><h1 style=" display: inline; font-size: inherit; ">HƯỚNG DẪN</h1></span>
        </div>
        <div class="page-wrapper">
            <div class="page-content-wrapper guide-wrapper">
                <div class="row">
                    <div class="col-sm-3">
                        <a class="card-wrapper-link" href="<?php echo ConfigGlobal::$realPath ?>/huong-dan-dang-ky.html">
                            <div class="content-card">
                                <img class="guide-icon" src="https://static.388bet.com/css/assets/images/guide-register.png">
                                <hr class="guide-card-dash">
                                <span class="content-card-text">Hướng dẫn Đăng Ký</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="card-wrapper-link" href="<?php echo ConfigGlobal::$realPath ?>/huong-dan-nap-rut-tien.html">
                            <div class="content-card">
                                <img class="guide-icon" src="https://static.388bet.com/css/assets/images/guide-cash.png">
                                <hr class="guide-card-dash">
                                <span class="content-card-text">Hướng dẫn Nạp/Rút tiền</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function(){
            $(function() {
                var act=0;
                if(window.location.hash) {
                    act =parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs({active: act});
            });
            $(window).on('hashchange', function() {
                var act=0;
                if(window.location.hash) {
                    act = parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs( "option", "active", act );
            });

        });
    </script>
    <?php include_once("footer.php") ?>
</div>
<script type="text/javascript">


    var imgArr=['<?php echo ConfigGlobal::$realPath ?>/image/home_jalan_vi-VN.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/ad_fastmarket.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/banner3_en.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/cock_fight.jpg'];
    var numberSilider =Math.floor((Math.random() * imgArr.length));
    //alert(imgArr);
    var url =imgArr[numberSilider];
    $('#imagesRandom').attr('src',url);

    $(document).ready(function(){
        $('.onNap').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            if(a == 'Nạp'){
                $('.onRut').text('Rút');
                $("#lb-naprut-caption").text("Rút tiền");

            }
            else{
                $('.onRut').text('Nạp');
                $("#lb-naprut-caption").text("Nạp tiền");
            }
            $('.chooseAction').toggle()

        })
        $('.onRut').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            //show_invoice_form();
            if(a == 'Nạp'){
                $(".form-controlCustom").show();
                $('#formChooseBankPull').hide();
                $('#formChooseBankPush').hide();
                $(".phieu-nap-text").hide();
                $(".phieu-rut-text").show();
                $('.onNap').text('Rút');
                $('.chooseAction').hide()
            }else{
                $(".form-controlCustom").show();
                $('#formChooseBankPush').hide();
                $('#formChooseBankPull').hide();
                $(".phieu-nap-text").show();
                $(".phieu-rut-text").hide();
                $('.onNap').text('Nạp');
                $('.chooseAction').hide()
            }

        });
        $('#basic').popup();
        $('#fade').popup();

        $('#phone_fix_dialog').popup();
        $('#bankinfo').popup({
            blur:false,
            beforeopen:function(){
                // $("#invoice-content").html("<p>content here!!!!</p>");

            }
        });



    });
    function show_invoice_form(){
        //$(".form-controlCustom").hide();
//    $(".frm-invoice-panel").show();
        var a = $('.onNap').text();
        if(a == 'Nạp'){
            $('#formChooseBankPull').hide();
            $('#formChooseBankPush').show();
        }else{
            $('#formChooseBankPush').hide ();
            $('#formChooseBankPull').show();
        }
    }
</script>
</body>
</html>
