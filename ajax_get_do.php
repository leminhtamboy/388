<?php
include_once ("BangDo.php");
$modelBangDo = new BangDo("bang_do","id_do");
$value = $_POST["value"];
$dataBangDo = $modelBangDo->loadByAttribute("ghi_chu","eq",$value,0);
?>
<select id="gia_do" name="gia_do">
    <option value="">Giá Đô</option>
    <?php foreach($dataBangDo as $_do){ ?>
        <?php
            $id_do = $_do->getid_do();
            $gia_do = $_do->getgia_do();
            $hien_thi_do = $_do->gethien_thi_do();
            $hoa_hong = $_do->gethoa_hong();
            //$text = $hien_thi_do." - ".$hoa_hong;
            $text = $hien_thi_do;
        ?>
        <option value="<?php echo $id_do ?>" data-value="<?php echo $gia_do; ?>"><?php echo $text; ?></option>
    <?php } ?>
</select>
