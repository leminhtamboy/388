﻿<?php
@session_start();
include_once("Article.php");
include_once("Menu.php");
include_once("Attribute.php");
include_once("AttributeValue.php");
include_once("processingImage.php");
include_once("Config.php");
include_once("Giaodich.php");
$config= new ConfigGlobal("config","config_id");
$menu  = new Menu("menu","id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$article = new Article("article","id_article");
$attributeValue= new AttributeValue("entity_attribute_value_varchar","id_eav_varchar");
include 'config_nganluong.php';
include 'lib/nganluong.class.php';
?>
<!doctype html>
<!--[if lt IE 9]> <html class="no-js ie-old" lang="vi"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="vi"> <![endif]-->
<!--[if gt IE 8]> <html lang="vi"> <!--<![endif]-->
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<?php include_once("head.php"); ?>
<body>
<div class="home-page-edit">
	<?php include_once("header.php"); ?>
	<style>
		strong{
			font-weight: bold;
		}
		h3{
			font-weight: bold;
		}
	</style>
	<div class="main-content">
		<div class="container" style="margin-top: 5%;margin-bottom: 5%">
			<?php
			if (isset($_GET['payment_id'])) {
				// Lấy các tham số để chuyển sang Ngânlượng thanh toán:
				$transaction_info =$_GET['transaction_info'];
				$order_code =$_GET['order_code'];
				$price =$_GET['price'];
				$payment_id =$_GET['payment_id'];
				$payment_type =$_GET['payment_type'];
				$error_text =$_GET['error_text'];
				$secure_code =$_GET['secure_code'];
				//Khai báo đối tượng của lớp NL_Checkout
				$nl= new NL_Checkout();
				$nl->merchant_site_code = MERCHANT_ID;
				$nl->secure_pass = MERCHANT_PASS;
				//Tạo link thanh toán đến nganluong.vn
				$checkpay= $nl->verifyPaymentUrl($transaction_info, $order_code, $price, $payment_id, $payment_type, $error_text, $secure_code);

				if ($checkpay && $_SESSION["allow_me"] == "0") {
				$_SESSION["allow_me"] = "1";
				echo "<center><h3 style='color:#228b22;'><strong>Chúc mừng bạn đã thanh toán thành công cho đơn hàng $order_code với số tiền là $price. Trình duyệt tự động chuyển về trang chủ sau 5s và cập nhật số dư: </strong></h3></center>";
				// bạn viết code vào đây để cung cấp sản phẩm cho người mua
				include_once ("Giaodich.php");
				$price = str_replace(".","",$price);
				$id_member = $_SESSION["member"];
				$username = $_SESSION["sdt"];
				$ma_giao_dich_ngan_luong = $payment_id."|".$transaction_info."|".$order_code;
				$giaodich = new Giaodich("giao_dich","id");
				$ngay_ht = date('m/d/Y h:i:s A');
				$giaodich->setData("id","NULL");
				$giaodich->setData("id_member",$id_member);
				$giaodich->setData("ngay_gio",$ngay_ht);
				$giaodich->setData("loai_phieu","nap");
				$giaodich->setData("ngan_hang","Ngân Lượng");
				$giaodich->setData("so_tien",$price);
				$giaodich->setData("trang_thai",0);
				$giaodich->setData("ghi_chu",$ma_giao_dich_ngan_luong);
				$giaodich->inserRow();
				include_once("Member.php");
				$modelMember = new Member("member","id");
				$dataMember = $modelMember->loadMemberData($id_member);
				$currentMoney = $dataMember->getso_tien();
				$inputMoney = $currentMoney+$price;
				$modelMember->setData("id",$id_member);
				$modelMember->setData("so_tien",$inputMoney);
				$modelMember->updateRow();
				//Inset
				?>
				<script type="text/javascript">
					setTimeout(function(){
						top.location="<?php echo ConfigGlobal::$realPath; ?>"
					},3000)
				</script>
			<?php
				}else{
					echo "<center><h3><strong style='color:red'>Thanh toán thất bại</strong></h3></center>";
				}
			}else{
				echo "<center><h3><strong style='color:red'>Hủy đơn hàng</strong></h3></center>";
			}
		?>
		</div>
	</div>
	<?php include_once("footer.php") ?>
</div>
</body>
</html>