<?php
@session_start();
include_once("Article.php");
include_once("Menu.php");
include_once("Attribute.php");
include_once("AttributeValue.php");
include_once("processingImage.php");
include_once("Config.php");
include_once("Giaodich.php");
$config= new ConfigGlobal("config","config_id");
$menu  = new Menu("menu","id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$article = new Article("article","id_article");
$attributeValue= new AttributeValue("entity_attribute_value_varchar","id_eav_varchar");
?>
<!doctype html>
<!--[if lt IE 9]> <html class="no-js ie-old" lang="vi"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="vi"> <![endif]-->
<!--[if gt IE 8]> <html lang="vi"> <!--<![endif]-->
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<?php include_once("head.php"); ?>
<body>
<div class="home-page-edit">
    <?php include_once("header.php"); ?>
    <div class="main-content">
        <div class="container">
            <div id="pSport">
                <div class="left_ct">
                    <img class="img-default" src="https://img.388bet.com/images/soccers1/Scorer_55.jpg" alt="">
                    <img class="img-latest" src="https://img.388bet.com/images/soccers1/Scorer_11.jpg" style="">
                    <img class="img-latest" src="https://img.388bet.com/images/soccers1/Scorer_22.jpg" style="">
                    <img class="img-latest" src="https://img.388bet.com/images/soccers1/Scorer_33.jpg" style="display:none;">
                    <img class="img-latest" src="https://img.388bet.com/images/soccers1/Scorer_44.jpg" style="display:none;">
                </div>
                <style>
                    .left_ct img{width:290px;margin-top:50px;}
                    strong {  font-weight:bold !important;}
                </style>
                <div class="right_ct">
                    <div class="breadcrumb-wrap">
                        <ul class="breadcrumb breadcrumb-cus">
                            <li><a href="<?php echo ConfigGlobal::$realPath; ?>">Trang chủ</a></li>
                            <li><a href="<?php echo  ConfigGlobal::$realPath ?>/huong-dan-dang-ky.html">Hướng Dẫn Đăng Kí</a></li>
                        </ul>
                    </div>
                    <h1 class="content_header title-details">
                        <span class="triangle-left" style="left: -12px;"></span>
                        <span class="txt-title">Hướng dẫn đăng ký</span>
                        <span class="triangle-right"></span>
                    </h1>
                    <div class="post-details-ext">
                        <p><strong><strong style="line-height: 1.6em;">Đăng ký tài khoản tại 88Cuoc.COM&nbsp;giúp bạn dễ dàng quản lý các tài khoản cược cũng như lịch sử giao dịch Nạp - Rút Tiền. Lợi ích lớn nhất khi đăng ký tài khoản tại 88Cuoc là&nbsp; đảm bảo quá trình tham gia cá cược của bạn hoàn toàn tuân thủ luật pháp Việt Nam.</strong></strong></p>
                        <p><strong>HƯỚNG DẪN ĐĂNG KÝ:</strong></p>

                        <p>Ở giao diện trang chủ bạn sẽ thấy mục đăng ký tài khoản ngay bên dưới Slide.</p>

                        <p>
                            <img alt="" src="<?php echo ConfigGlobal::$realPath; ?>/image/dang-ki-1.png" style="width: 680px;">
                        </p>

                        <p>&nbsp;Bạn điền đầy đủ các thông tin như&nbsp; hình trên để đăng ký tài khoản <strong>88CUOC</strong>&nbsp;nhé:</p>

                        <p>- <strong>Tên đăng nhập:</strong> bạn cần điền tên đăng nhập vào.(Bắt buộc)</p>
                        <p>- <strong>Mật khẩu:</strong> tối thiểu 6 ký tự.(Bắt buộc)</p>
                        <p>- <strong>Email:</strong> Không bắt buộc nhưng chúng tôi khuyên bạn nên có để tiện cho việc khôi phục mật khẩu.(Không bắt buộc)</p>



                        <p>Sau khi điền đầy đủ thông tin bạn click vào button <img alt="" src="<?php echo ConfigGlobal::$realPath ?>/image/bt-next.png" style="height: 20px; width: 20px;">&nbsp;để hoàn tất việc đăng ký tài khoản <strong>88CUOC</strong>.</p>
                        <p><strong>------------------------------------</strong></p>

                        <p>Hệ thống thông báo thành công và tự động đăng nhập vào tài khoản bạn mới đăng ký.</p>

                        <p><img alt="" src="<?php echo ConfigGlobal::$realPath; ?>/image/dang-ki-2.png" style="width: 680px;"></p>

                        <p>Để kiểm tra chi tiết&nbsp;các&nbsp;thông tin tài khoản, bạn&nbsp;chỉ cần click vào Tên Đăng Nhập tại góc trái màn hình. <strong>88CUOC</strong> tối ưu hóa trang quản lý tài khoản một cách chi tiết, giúp quý&nbsp;khách hàng có thể kiểm tra và quản lý mọi thông tin tài khoản dễ dàng, tiện lợi nhất.</p>

                        <p><img alt="" src="<?php echo ConfigGlobal::$realPath; ?>/image/dang-ky-3.png" style="width: 680px;"></p>

                        <p>&nbsp;</p>

                        <p><strong>Xem thêm: <a href="<?php echo ConfigGlobal::$realPath ?>/huong-dan-nap-rut-tien.html">Hướng dẫn nạp rút tiền tại website 88CUOC</a></strong></p>                                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function(){
            $(function() {
                var act=0;
                if(window.location.hash) {
                    act =parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs({active: act});
            });
            $(window).on('hashchange', function() {
                var act=0;
                if(window.location.hash) {
                    act = parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs( "option", "active", act );
            });

        });
    </script>
    <?php include_once("footer.php") ?>
</div>
<script type="text/javascript">


    var imgArr=['<?php echo ConfigGlobal::$realPath ?>/image/home_jalan_vi-VN.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/ad_fastmarket.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/banner3_en.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/cock_fight.jpg'];
    var numberSilider =Math.floor((Math.random() * imgArr.length));
    //alert(imgArr);
    var url =imgArr[numberSilider];
    $('#imagesRandom').attr('src',url);

    $(document).ready(function(){
        $('.onNap').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            if(a == 'Nạp'){
                $('.onRut').text('Rút');
                $("#lb-naprut-caption").text("Rút tiền");

            }
            else{
                $('.onRut').text('Nạp');
                $("#lb-naprut-caption").text("Nạp tiền");
            }
            $('.chooseAction').toggle()

        })
        $('.onRut').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            //show_invoice_form();
            if(a == 'Nạp'){
                $(".form-controlCustom").show();
                $('#formChooseBankPull').hide();
                $('#formChooseBankPush').hide();
                $(".phieu-nap-text").hide();
                $(".phieu-rut-text").show();
                $('.onNap').text('Rút');
                $('.chooseAction').hide()
            }else{
                $(".form-controlCustom").show();
                $('#formChooseBankPush').hide();
                $('#formChooseBankPull').hide();
                $(".phieu-nap-text").show();
                $(".phieu-rut-text").hide();
                $('.onNap').text('Nạp');
                $('.chooseAction').hide()
            }

        });
        $('#basic').popup();
        $('#fade').popup();

        $('#phone_fix_dialog').popup();
        $('#bankinfo').popup({
            blur:false,
            beforeopen:function(){
                // $("#invoice-content").html("<p>content here!!!!</p>");

            }
        });



    });
    function show_invoice_form(){
        //$(".form-controlCustom").hide();
//    $(".frm-invoice-panel").show();
        var a = $('.onNap').text();
        if(a == 'Nạp'){
            $('#formChooseBankPull').hide();
            $('#formChooseBankPush').show();
        }else{
            $('#formChooseBankPush').hide ();
            $('#formChooseBankPull').show();
        }
    }
</script>
</body>
</html>
