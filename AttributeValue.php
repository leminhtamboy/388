<?php
include_once ("Collection.php");
class AttributeValue extends Collection{
    function __construct($tableName, $primaryKey)
    {
        parent::__construct($tableName, $primaryKey);
    }
    function getValueAttribuetOfEntityBySql($idEntity,$idAttribute){
        $sql="SELECT * from  entity_attribute_value_varchar where attribute_id=$idAttribute AND entity_id=$idEntity";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getValueAttribuetOfEntityByAttributeCode($idEntity,$codeAttribute){
        $sql="SELECT * from  entity_attribute_value_varchar inner join entity_attribute on entity_attribute_value_varchar.attribute_id=entity_attribute.attribute_id  where entity_attribute.attribute_code='$codeAttribute' AND entity_id=$idEntity";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
}