<?php
include_once("Article.php");
include_once("Menu.php");
include_once("Attribute.php");
include_once("AttributeValue.php");
include_once("Config.php");
include_once("Category.php");
$menu  = new Menu("menu","id");
$config= new ConfigGlobal("config","config_id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$article = new Article("article","id_article");
$attributeValue= new AttributeValue("entity_attribute_value_varchar","id_eav_varchar");
$category = new Category("category","id");
$dataCollectionArticle=null;
$countPage=0;
$keySearch=$_REQUEST["q"];
    $allArticle=$article->loadByAttribute("title","like",$keySearch,0);
    $allPage=count($allArticle);
    $limit=12;
    $start=0;
    $end=12;
    $countPage=intval($allPage/$limit);
    if(isset($_REQUEST["page"])){
        $page=$_REQUEST["page"];
        if($requestPage==1){
            $start=0;
            $end=$limit;
        }else{
            $end=$page * ($limit + 1);
            $start=$last-($limit+1);
        }
    }
    $dataCollectionArticle=$article->getCollectionArticleOnPageByCustomColumn($start,$end,"title","like",$keySearch);
?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<?php include_once ("head.php")?>
<body>
<div class="container">
    <?php include_once("header.php"); ?>
    <div class="page-path row">
        <a href="<?php echo ConfigGlobal::$realPath ?>">HomePage</a>
        >>
        <a href="search.html?q=">Search Article</a>
    </div>
    <div id="content" class="row">
        <div class="page-title">
            <h2>Search Article</h2>
        </div>
        <div class="row">
            <div class="page-metas">
                <div class="page-desc col-sm-4 col-xs-6">
                    <b>Show:</b> from 1 to 12 on all of <?php echo count($dataCollectionArticle); ?>
                </div>

                <div class="pagination col-sm-4 col-xs-6">
                    <b>Page </b>
                    <?php for($p=0;$p<$countPage;$p++){ ?>
                        <a href="/search.html?q=<?php echo $keySearch ?>&page=<?php echo $p; ?>" <?php if($p==$_REQUEST["page"]) echo "class='current'" ?>><?php echo $p ?></a>
                    <?php } ?>
                </div>
            </div>
            <ul class="event-list">
                <?php foreach ($dataCollectionArticle as $dataArt) { ?>
                    <?php $idEntity=$dataArt->getid_article(); ?>
                    <?php $shortDescription=$attributeValue->getValueAttribuetOfEntityByAttributeCode($idEntity,"short_description")[0]->getvalue();  ?>
                    <?php $linkSeo=$attributeValue->getValueAttribuetOfEntityByAttributeCode($idEntity,"link_seo")[0]->getvalue();  ?>
                    <?php $nameCategory=$category->load($dataArt->gethas_category())->gettitle(); ?>
                    <li class="item">
                        <div class="content">
                            <div class="course-type"><?php echo $nameCategory; ?></div>
                            <a href="/article/<?php echo $linkSeo ?>-<?php echo $idEntity ?>.html" class="name" title="<?php echo $dataArt->gettitle(); ?>">
                                <span><?php echo $dataArt->gettitle(); ?></span>
                            </a>
                            <div class="class-content">
                                <a href="/article/<?php echo $linkSeo ?>-<?php echo $idEntity ?>.html" class="cover">
                                    <img width="300" height="300" src="<?php echo $dataArt->getthumbail(); ?>" class="full-width attachment-thumbnail">
                                </a>
                                <div class="description"><?php echo $shortDescription ?></div>
                                <a href="/article/<?php echo $linkSeo ?>-<?php echo $idEntity ?>.html" class="read-more">Read More</a>
                            </div>
                        </div>
                        <!-- <div class="corner-status future">Sắp diễn ra</div>-->
                    </li>
                <?php } ?>
            </ul>
            <div class="page-metas">
                <div class="page-desc col-sm-4 col-xs-6">
                    <b>Show:</b> from 1 to 12 on all of <?php echo count($dataCollectionArticle); ?>
                </div>

                <div class="pagination col-sm-4 col-xs-6">
                    <b>Page </b>
                    <?php for($p=0;$p<$countPage;$p++){ ?>
                        <a href="/search.html?q=<?php echo $keySearch ?>&page=<?php echo $p; ?>" <?php if($p==$_REQUEST["page"]) echo "class='current'" ?>><?php echo $p ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>
    <?php include_once("footer.php"); ?>
</div>
</body>
</html>
