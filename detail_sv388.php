<?php
@session_start();
include_once("Article.php");
include_once("Menu.php");
include_once("Attribute.php");
include_once("AttributeValue.php");
include_once("processingImage.php");
include_once("Config.php");
include_once("Giaodich.php");
$config= new ConfigGlobal("config","config_id");
$menu  = new Menu("menu","id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$article = new Article("article","id_article");
$attributeValue= new AttributeValue("entity_attribute_value_varchar","id_eav_varchar");
?>
<!doctype html>
<!--[if lt IE 9]> <html class="no-js ie-old" lang="vi"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="vi"> <![endif]-->
<!--[if gt IE 8]> <html lang="vi"> <!--<![endif]-->
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<?php include_once("head.php"); ?>
<body>
<div class="home-page-edit">
    <?php include_once("header.php"); ?>
    <div class="main-content">
        <div class="container">
            <div id="pSport">
                <div class="left_ct">
                    <img class="img-default" src="<?php echo ConfigGlobal::$realPath ?>/image/banner.jpg" alt="">
                    <img class="img-latest" src="<?php echo ConfigGlobal::$realPath ?>/image/banner2.jpg" style="">
                    <img class="img-latest" src="<?php echo ConfigGlobal::$realPath ?>/image/banner3_vn.jpg" style="">
                    <img class="img-latest" src="<?php echo ConfigGlobal::$realPath ?>/image/click_cockfight.png" style="">
                </div>
                <style>
                    .left_ct img{width:290px;margin-top:50px;}
                    strong {  font-weight:bold !important;}
                    .static h1		{font-size: 28px; line-height: 40px; margin-bottom: 15px;font-weight: bold;}
                    .static h2		{font-size: 20px; line-height: 30px; margin-bottom: 10px;font-weight: bold;}
                    .static h3		{font-size: 16px; line-height: 24px; margin-bottom: 5px;font-weight: bold;}
                    .static li		{padding-left: 8px; padding-bottom: 8px;}
                    /**********/
                    .introduction		{font-size: 16px;}

                    .aboutus .intro		{padding-bottom: 20px; height: 140px;}

                    .gamerules li		{padding-bottom: 5px;}



                    /*************************
                        Global Overwrite
                    **************************/
                    .clear-both			{clear: both; width: 0; height: 0; line-height: 0; font-size: 0;}
                    .width-auto			{width: auto;}
                    .inactive			{filter: alpha(opacity=50); opacity: 0.50;}
                </style>
                <div class="right_ct">
                    <div class="breadcrumb-wrap">
                        <ul class="breadcrumb breadcrumb-cus">
                            <li><a href="<?php echo ConfigGlobal::$realPath; ?>">Trang chủ</a></li>
                            <li><a href="<?php echo  ConfigGlobal::$realPath ?>/tai-khoan-dung-thu-sv388.html">Hướng dẫn Đá gà SV388</a></li>
                        </ul>
                    </div>
                    <h1 class="content_header title-details">
                        <span class="triangle-left" style="left: -12px;"></span>
                        <span class="txt-title">Tài khoản dùng thử và hướng dẫn chơi</span>
                        <span class="triangle-right"></span>
                    </h1>
                    <div class="static post-details-ext">

                        <div>Đảm bảo việc cá cược của người chơi là hợp pháp, bạn không cá cược trực tiếp trên <strong>88Cuoc</strong>. Chúng tôi đóng vai trò quản lý các giao dịch nạp - rút tiền và cung cấp tài khoản để người chơi tham gia trên các thị trường cá cược hàng đầu Châu Á hiện nay.</div>

                        <div>&nbsp;</div>

                        <div><strong>1. SV388&nbsp;</strong></div>

                        <div>&nbsp;</div>

                        <div>
                            Chọi gà là một môn thể thao giữa 2 con gà trong một trường đấu. Việc về sát thương trên cơ thể gà với dao gắn làm cựa gà như một vũ khí, từ xưa tới nay nó vẫn là một môn thể thao máu lửa. Cựa đá gà (Gaffs) là một loại dao được trang bị thêm cho cựa gà chọi. Hồi tưởng lại quá khứ nơi các chiến binh trong đấu trường La Mã, chọi gà tạo ra một bầu không khí thể thao quen thuộc được bao bọc bởi sự nồng nhiệt và tinh thần chiến đấu không mệt mỏi.
                            <br/>
                            <br/>
                            Chọi gà ở hầu hết các quốc gia trên thế giới bị cấm. Tại Campuchia chọi gà được biết đến là “Sabong” bởi người địa phương và nó là một trò giải trí phổ biến trước khi người Tây Ban Nha đến. Chọi gà ở Campuchia là hợp pháp theo luật, trong nghị định số 449 được ban hành bởi tổng thống Ferdinand E. Marcos vào ngày 9 tháng 5 năm 1974 để xác nhận và bảo tồn như di sản truyền thống, giải trí theo phong tục và giải trí. Một nghị định khác số 1802 được đưa ra sau đó. Nghị định này là để ngăn chắn các hoạt động bất hợp pháp chọi gà không giấy phép trong khi đang tiêu chuẩn hóa trò chơi. Tuy nhiên chọi gà bất hợp pháp vẫn được tổ chức trong thời gian này một cách kín đáo để tránh truy quét của chính quyền.
                            <br/>
                            <br/>
                            Chọi gà đã trở thành một di sản độc đáo của người Campuchia, một truyền thống văn hóa đó là một hình ảnh thu nhỏ của tinh thần thể thao, một quy tắc danh dự trong việc chơi đẹp, tôn trọng người khác và sự kiêu hãnh khi thua mà người Campuchia tuân thủ nghiêm ngặt không ngoại lệ. Trong việc theo đuổi môn thể thao này, người hâm mộ giữ được sự kính trọng và tránh xa các dèm pha. Các môn thể thao luôn phù hợp với nguyên tắc dân chủ, nơi mà những thái cực của xã hội từ nông dân chân đất, các anh hùng và tất cả mọi người đều được đối xử bình đẳng.
                            <br/>
                            <br/>
                            Campuchia cũng là một trung tâm nơi mà các sự kiện, hội chợ quốc tế trò chơi về gà diễn ra và World Slasher Cup được tổ chức 2 năm 1 lần. Đây là tập hợp các nhà lại tạo hàng đầu trong thế giới gà chọi và chọi gà và là nơi đam mê được tập hợp lại tốt nhât và tôn vinh lên tầm vô địch thế giới. Campuchia có một bề dầy lịch sử về chọi gà nơi mà đã được thử thách qua thời gian. Chọi gà cũng đã đạt đến như là một môn thể thao số một tại Campuchia cũng là một kho báu.
                        </div>

                        <div>&nbsp;</div>

                        <div style="text-align: center;"><img alt="" src="<?php echo ConfigGlobal::$realPath ?>/image/bg_cuoc_sv388.png" style="width: 700px; height: 363px;"></div>

                        <div style="text-align: center;"><cite>Giao diện trang cược sv388</cite></div>
                        <div>&nbsp;</div>
                        <style>
                            .tk {
                                width: 700px;
                                overflow: hidden;
                                padding-left: 10px;
                            }
                            .tk ul.tkul {
                                margin-left: 0px !important;
                            }
                            .tk ul {
                                width: 153px;
                                float: left;
                                margin: 0px;
                                padding: 0;
                                margin-left: 25px;
                            }
                            .entry ul, .comment-content ul {
                                list-style-type: disc;
                            }
                            ol, ul {
                                list-style: none;
                            }
                            .tk ul li.den {
                                background: white;
                                height: 25px;
                                line-height: 25px;
                                border:1px solid black;
                                box-sizing: border-box;
                            }
                            .tk ul li.trang {
                                background: white;
                                height: 25px;
                                line-height: 25px;
                                border:1px solid black;
                                box-sizing: border-box;
                            }
                            .tk ul li {
                                list-style: none;
                                padding-left: 0px;
                                color: #878787;


                            }
                            .tk ul li {
                                list-style: none;
                                padding-left: 0px;
                                color: #878787;
                            }
                            /*.tk ul li:nth-child(odd){
                                border-right:none;
                            }
                            .tk ul li:nth-last-child(2),
                            .tk ul li:nth-last-child(2) ~ .tk ul li{
                                border-bottom:1px solid black
                            }*/
                        </style>
                        <h2><strong>Tài khoản dùng thử sv388</strong></h2>
                        <div>&nbsp;</div>
                        <div style="text-align: center;">

                            <center>
                                <div class="tk">
                                    <ul class="tkul">
                                        <li class="den">
                                            <ul class="tkul">
                                                <li class="den">hphcssta00</li>
                                            </ul>
                                        </li>
                                        <li class="den">hphcssta01</li>
                                        <li class="trang">hphcssta02</li>
                                        <li class="den">hphcssta03</li>
                                    </ul>
                                    <ul>
                                        <li class="den">hphcssta04</li>
                                        <li class="trang">hphcssta05</li>
                                        <li class="den">hphcssta06</li>
                                        <li class="trang">hphcssta07</li>
                                    </ul>
                                    <ul class="mbf">
                                        <li class="den">hphcssta08</li>
                                        <li class="trang">hphcssta09</li>
                                        <li class="den">hphcssta10</li>
                                        <li class="trang">hphcssta11</li>
                                    </ul>
                                    <ul>
                                        <li class="den">hphcssta12</li>
                                        <li class="trang">hphcssta13</li>
                                        <li class="den">hphcssta14</li>
                                        <li class="trang">hphcssta15</li>
                                    </ul>
                                </div>
                                <p></p>
                                Chú ý: tất cả các tài khoản có mật khẩu là <strong>Cuoc88123</strong>  chữ  "C" viết hoa
                            </center>
                        </div>
                        <div>&nbsp;</div>
                        <div>Quý khách vào sv388 theo link: <a href="https://www.sv388.com/index.jsp?722556185" target="_blank">www.sv388.com</a>&nbsp;---&gt; Điền tên đăng nhập + mật khẩu để có thể vào xem giao diện trang.</div>

                        <div>&nbsp;</div>

                        <div style="text-align: center;"><img alt="" src="<?php echo ConfigGlobal::$realPath ?>/image/bg_dang_nhap_sv388.png" style="width: 700px; height: 363px;"></div>

                        <div style="text-align: center;"><em><i>Điền tên đăng nhập và mật khẩu vào ô tương ứng để truy cập trang cược sv388</i></em></div>

                        <div>&nbsp;</div>

                        <div><strong></strong> Đê có tài khoản dùng thử hoặc của riêng mình, Qúy khách vui lòng liên hệ <strong>88Cuoc</strong> bằng cách CHAT với nhân viên hoặc đăng kí nạp tiền 0 đồng để có tài khoản dùng thử nhé</div>
                        <div>&nbsp;</div>
                        <div><strong>2. Hoa hồng và Mức Quy Đổi Điểm&nbsp;</strong></div>
                        <div>&nbsp;</div>
                        <div>&nbsp;</div>
                        <div>
                            <ul>
                                <li><span style=" ">Thành Viên <span style="color: #ffcc00;">mạng sv388</span> chọn mức độ&nbsp;<span style="color: #ffa500;">1 điểm = 50.000</span>&nbsp;VND hoa hồng được 1% &nbsp;được cộng trực tiếp vào từng mã cược 0,25% và 0.75% phần còn lại vào tài khoản ca cuoc sv388</span><span style=" ">, được tính 1 tuần 1 lần trước 18h ngày thứ 7 hàng tuần.</span></li>
                                <p></p>
                                <li><span style=" ">Thành viên mạng sv388 chọn mức độ&nbsp;<span style="color: #ffa500;">1 điểm = 25.000</span>&nbsp;VND hoa hồng được 0.25% &nbsp;được cộng trực tiếp vào từng mã cược 0,25% vào tài khoản cá cược.</span></li>
                                <p></p>
                                <li><span style=" "><span style="color:red">Lưu ý:</span> Tài khoản chơi nạp tối thiểu 300.000 VND</span></li>
                                <p></p>
                                <li class="cuoi"><span style="color: #ff0000;"><em><span style=" font-size: 11pt; color: #ffa500;">Thời gian từ 24h đến 7h các mạng thường bảo trì nên không kích hoạt được tài khoản sv388 trong thời gian này.</span></em></span></li>
                            </ul>
                        </div>
                        <div>&nbsp;</div>
                        <div><strong>3. Hướng Dẫn Chơi</strong></div>
                        <div>&nbsp;</div>
                        <div>&nbsp;</div>
                        <div>&nbsp;</div>
                        <div>
                            <h3>QUY TẮC 1 (GIỚI HẠN TRỌNG LƯỢNG)</h3>
                            Không được thấp hơn 1,7 kg cũng không hơn 2,2 kg trong một trận derby stag; và không ít hơn 1,9 kg cũng không hơn 2,45 kg trong một trận derby cock. Đối với derby bull stag, trọng lượng không nhỏ hơn 1,8 kg cũng không hơn 2,3 kg.<br>
                            <br><br>
                            <h3>QUY TẮC 2 (NỘP HỒ SƠ TRỌNG LƯỢNG)</h3>
                            Các chỉ số trọng lượng của gà trống để được chiến đấu trong một trận derby sẽ được nộp vào ngày trước trận derby dự kiến, tại địa điểm và thời gian chỉ định và theo quy định của sv388. Sau đó, quá trình matching sẽ được triển khai<br>
                            <br><br>
                            <h3>QUY TẮC 3 (QUYỀN TỪ CHỐI)</h3>
                            Chúng tôi, sv388, có quyền không cho phép sự tham gia trận derby, đặc biệt trong quá trình diễn ra vòng loại, bán kết, chung kết mà ba con gà cuối bảng hoặc đầu bảng không đủ điều kiện thi đấu.<br>
                            <br><br>
                            <h3>QUY TẮC 4 (GHI NHẬN TRỌNG LƯỢNG)</h3>
                            Nếu được thông báo trước bởi sv388, trọng lượng của gà trống trong một trận derby được ghi lại bởi các chủ sở hữu, trong hai phần: phần thứ nhất, phần có chứa số lượng nhập và nhập tên gửi vào một hộp thả đánh dấu \ 93a \ 94, và thứ hai, một phần có chứa số lượng nhập và trọng lượng gửi vào hộp thả đánh dấu \ 93b \ 94.<br>
                            <br><br>
                            <h3>QUY TẮC 5 (MATCHING QUA MÁY TÍNH, KHÔNG SẮP ĐẶT TRẬN ĐẤU, ĐÁ ENTRY)</h3>
                            Việc matching các trận derby được thực hiện trên máy vi tính và cùng được thực hiện trong sự hiện diện của các ban derby và đại biểu. Các máy tính được lập trình để thực hiện kết hợp trong tiến trình bắt đầu với các con gà có trọng lượng tương đương.<br>
                            <br><br>
                            <b>QUY TẮC VỀ CHÊNH LỆCH TRỌNG LƯỢNG:</b>
                            <ol style="list-style-type:lower-alpha;">
                                <li>Đối với stag và bull stag nặng 1,9 kg trở xuống, trọng lượng chênh lệch khoảng 20 gram, và cho stag và bull nặng hơn 1,9 kg, trọng lượng chênh lệch khoảng 30 gram.
                                </li><li>Đối với gà trống nặng 2,2 kg trở xuống, trọng lượng chênh lệch là 30 gram và đối với gà trống nặng hơn 2,2 kg, trọng lượng chênh lệch là 50 gram.
                                </li><li>Trận đấu không được phép nếu không được 3 sự đồng thuận
                                </li></ol><br>
                            <h3>QUY TẮC 6 (KHÔNG THI ĐẤU TRÙNG LẶP)</h3>
                            Các máy tính được lập trình để tránh chiến đấu trùng lặp.<br>
                            <br><br>
                            <h3>QUY TẮC 7 (KHÔNG DÙNG MÁY TÍNH ĐỂ MATCHING)</h3>
                            Nếu trong trường hợp máy tính không có sẵn (máy tính hỏng, bị lỗi hoặc bất kỳ nguyên nhân tương tự khác), matching theo hệ thống “bola” sẽ được triển khai.<br>
                            <br>
                            <h3>QUY TẮC 8 (GIAI ĐOẠN CHIẾN ĐẤU)</h3>
                            Quá trình chiến đấu sẽ được thực hiện theo đúng các thủ tục của cuộc chiến đầu, kết thúc sớm, cuối cuộc chiến, kết thúc muộn. Năm trận đánh đầu tiên hoặc hơn, tùy thuộc vào số lượng các trận đấu, sẽ bắt đầu với các cặp trọng lượng thấp nhất, theo thứ tự tăng dần; và số lượng bằng nhau tiếp theo của chiến đấu sẽ bắt đầu với các cặp trọng lượng cao nhất, thứ tự giảm dần.<br>
                            <br><br>
                            <h3>QUY TẮC 9 (XỬ PHẠT)</h3>
                            Đối với những vi phạm các quy tắc nói trên, các hình phạt có thể áp dụng như sau:<br>
                            <ol style="list-style-type:lower-alpha;">
                                <li>Phạt theo tỷ lệ đặt cược thấp nhất của center bet nếu stag có cân nặng không đủ chuẩn hoặc hình thể xấu.
                                </li><li>Tịch thu lệ phí entry;
                                </li><li>Tiền phạt khác, hình phạt tiền có thể được chỉ định bởi các ủy ban derby;
                                </li><li>Cấm tiến hành việc dời lại cuộc chiến dự kiến
                                </li><li>Không đạt chuẩn để tham gia vào trận derby
                                </li><li>Vĩnh viễn không được tham gia thi đấu vào các trận đấu khác.
                                </li></ol><br>
                            <h3>QUY TẮC SỐ 10 (Hình dạng xấu: ảnh hưởng)</h3>
                            Các trường hợp ảnh hưởng xấu về dịnh dạng như sau:<br>
                            <ul>
                                <li>Khập khểnh</li>
                                <li>Nhát không đấu</li>
                            </ul>
                            Trong trường hợp chân khập khiễng hoặc nhát không đấu sẽ được tài đấu lần thứ hai hoặc họ sẽ thay cựa gà để đấu. Nếu thay gà thì pải trả đối thủ 2500php (tiền philipin) cho việc trì hoãn và quy tắc 1 sẽ được áp dụng.<br>
                            <br>
                            Nếu gà vẫn bị khập khiễng khi thay cựa gà, nó sẽ thay thế bằng gà có trọng lượng nhỏ hơn trọng lượng thực tế của đối thủ cho cựa gà và cựa gà thay thế và trọng lượng cũng không it hơn 35gam.<br>
                            <br>
                            Đấu thủ phải trả cho đối phương tiền bằng 1/2 của kèo và trận đấu sẽ diễn ra như quy tắc 1<br>
                            <br>
                            Nếu gà thay thế cũng khập khiễng, hoặc một hình thức tệ, thì cần phải xem tới mục “a” “d” của quy tắc 10 và theo quy tắc số 1 để xác định số điểm của đối thủ.<br>
                            <ol style="list-style-type:lower-alpha;">
                                <li>Nếu gà có dấu hiệu của việc nhát không đấu, như sởn da gáy, cơ thể ủ rũ, di chuyển xung quanh gà khác hoặc nản như run rẩy, trúng độc, hoặc thua trước khi bị đánh bới đối thủ, hoặc tương tự, gà thay thế sẽ được sử dụng và trọng lượng bằng hoặc nhỏ hơn 35gam trở lại so với gà đối thủ khi đưa ra, trong đó nếu bên nào cao hơn thì sẽ được xếp lại lịch đấ theo các quy tắc 1 để xác định điểm số mà không bị phạt.</li>
                                <li>Trong trường hợp mà cả hai gà được thả ra trong chuồng đấu, mà một gà không đấu trước khi bất kỳ sự tiếp xúc nào xảy ra, trận đấu sẽ được coi là “no match” không có trận thi đấu. Với điều kiện như vậy quy tắc số 11 sẽ xem gà nào có biểu hiện là không chiến đấu, đứng yên khi đấu/ gà ốm thì vẫn có cơ hội để thay thế gà theo quy tắc sô 1. Anh sẽ bị xử phạt năm nghìn pê số (tiền philipin), và chịu hình phạt tại quy tắc số 10. Và cũng được thêm một cơ hội thay thế gà khác. Nếu sau 2 lần thay thế đều bị từ chối đấu, thì đấu thủ phải chịu toàn bộ theo quy tắc số 10 và cùng với quy tắc số 1 để định số điểm của đối thủ. Để kiểm tra gà có nhát đấu không, hội đồng trận đấu sẽ trực tiếp xử lý kết thúc trong lúc tham khảo thêm, các trọng tài được chỉ định bởi hội đồng sẽ giữ cho con gà đó trước khi nó nhìn thấy con gà khác ở khoảng cách đủ gần để bị kích động từ con gà khác.</li>
                                <li>Nếu gá xuất hiện mà có sự thay đổi đáng kể (vd như lông cổ không có, hoặc lông nhuộm màu, không đúng với tự nhiên, hoặc thay đổi tương tự), các mục nhập không đúng tiêu chuẩn sẽ bị phạt tương tự trong các mục từ đoạn (b) ở trên. Hình dạng xấu sẽ được xác định bởi hội đồng trận đấu ở ngay trường đấu hoặc bất kỳ chỗ nào dựa trên sự cân nhắc thận trọng.</li>
                                <li>Nếu gà nằm trong bất kỳ điều kiện hoàn cảnh nào sẽ hoặc có xu hướng làm suy yếu khả năng chiến đấu cho chính nó hoặc đối thủ, sẽ phải chịu hình phạt trong quy tắc số 10; và thủ thục sẽ tuân thủ theo các giới hạn ở quy tắc số 1 để quyết định số điểm của đối thủ. Do đó sẽ không được chấp nhận trong các trận đấu tổ chức bới NFGB</li>
                            </ol><br>
                            <h3>QUY TẮC 11 (KHOẢNG CÁCH TRẬN ĐẤU)</h3>
                            Gà trống được phép chiến đấu không quá mười (10) phút hoặc mổ nhau cùng một lúc. Cuộc chiến chỉ được coi là kết thúc khi có dấu hiệu từ trọng tài.<br>
                            <br><br>
                            <h3>QUY TẮC 12 (QUYỀN HẠN CỦA TRỌNG TÀI)</h3>
                            Trọng tài có toàn quyền kiểm soát trong trận đấu. Trong các sự cố, trọng tài có thể kêu gọi sự trợ giúp của những người trong đấu trường.<br>
                            <br><br>
                            <h3>QUY TẮC 13 (THIẾT BỊ TÍNH GIỜ)</h3>
                            Các thiết bị đo thời gian chiến đấu sẽ được cung cấp bởi các chủ sở hữu / nhà điều hành của các địa điểm của các trận derby của các liên đoàn, họ cũng sẽ cung cấp một máy phát điện dự phòng.<br>
                            <br><br>
                            <h3>QUY TẮC 14 (CARREO ĐƯỢC TIẾN HÀNH)</h3>
                            Nếu tại bất kỳ thời điểm nào trong thời gian chiến đấu một hoặc cả hai con gà ngừng chiến đấu, trọng tài sẽ áp dụng quy tắc 3-up3-down.<br>
                            <br><br>
                            <h3>QUY TẮC 15 (TUYÊN BỐ NGƯỜI THẮNG CUỘC)</h3>
                            Để được tuyên bố là người thắng cuộc, con gà phải được 2 phát mổ hoặc 1 hit trực tiếp nhưng không nhất thiết phải chạm vào đối thủ, và sau đó cả hai con đều không mổ lại.<br>
                            <br><br>
                        </div>

                        <div>&nbsp;</div>

                        <div style="text-align: center;">Mọi thắc mắc quý khách hàng có thể liên hệ bộ phận support để được giải đáp.</div>

                        <div>
                            <div>&nbsp;</div>
                        </div>                                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function(){
            $(function() {
                var act=0;
                if(window.location.hash) {
                    act =parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs({active: act});
            });
            $(window).on('hashchange', function() {
                var act=0;
                if(window.location.hash) {
                    act = parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs( "option", "active", act );
            });

        });
    </script>
    <?php include_once("footer.php") ?>
</div>
<script type="text/javascript">


    var imgArr=['<?php echo ConfigGlobal::$realPath ?>/image/home_jalan_vi-VN.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/ad_fastmarket.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/banner3_en.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/cock_fight.jpg'];
    var numberSilider =Math.floor((Math.random() * imgArr.length));
    //alert(imgArr);
    var url =imgArr[numberSilider];
    $('#imagesRandom').attr('src',url);

    $(document).ready(function(){
        $('.onNap').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            if(a == 'Nạp'){
                $('.onRut').text('Rút');
                $("#lb-naprut-caption").text("Rút tiền");

            }
            else{
                $('.onRut').text('Nạp');
                $("#lb-naprut-caption").text("Nạp tiền");
            }
            $('.chooseAction').toggle()

        })
        $('.onRut').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            //show_invoice_form();
            if(a == 'Nạp'){
                $(".form-controlCustom").show();
                $('#formChooseBankPull').hide();
                $('#formChooseBankPush').hide();
                $(".phieu-nap-text").hide();
                $(".phieu-rut-text").show();
                $('.onNap').text('Rút');
                $('.chooseAction').hide()
            }else{
                $(".form-controlCustom").show();
                $('#formChooseBankPush').hide();
                $('#formChooseBankPull').hide();
                $(".phieu-nap-text").show();
                $(".phieu-rut-text").hide();
                $('.onNap').text('Nạp');
                $('.chooseAction').hide()
            }

        });
        $('#basic').popup();
        $('#fade').popup();

        $('#phone_fix_dialog').popup();
        $('#bankinfo').popup({
            blur:false,
            beforeopen:function(){
                // $("#invoice-content").html("<p>content here!!!!</p>");

            }
        });



    });
    function show_invoice_form(){
        //$(".form-controlCustom").hide();
//    $(".frm-invoice-panel").show();
        var a = $('.onNap').text();
        if(a == 'Nạp'){
            $('#formChooseBankPull').hide();
            $('#formChooseBankPush').show();
        }else{
            $('#formChooseBankPush').hide ();
            $('#formChooseBankPull').show();
        }
    }
</script>
</body>
</html>
