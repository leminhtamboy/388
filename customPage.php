<?php
include_once("Page.php");
include_once("Menu.php");
include_once("Config.php");
include_once("Category.php");
$menu  = new Menu("menu","id");
$config= new ConfigGlobal("config","config_id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$page = new Page("page","id_page");
$idPage=$_REQUEST["id_page"];
$dataCurrentPage=$page->load($idPage);
?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/toatau.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.2.2"}};
        !function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='vye_dynamic-css'  href='<?php echo ConfigGlobal::$realPath ?>/css/main.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mailchimp-for-wp-checkbox-css'  href='<?php echo ConfigGlobal::$realPath."/"?>css/checkbox.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='life-style-css'  href='<?php echo ConfigGlobal::$realPath."/"?>css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mailchimp-for-wp-form-css'  href='<?php echo ConfigGlobal::$realPath."/"?>css/form.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bs3_css-css'  href='<?php echo ConfigGlobal::$realPath."/"?>css/bootstrap.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='fa_css-css'  href='<?php echo ConfigGlobal::$realPath."/"?>css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='plugin_css-css'  href='<?php echo ConfigGlobal::$realPath."/"?>css/plugin.css' type='text/css' media='all' />
    <link rel='stylesheet' id='validate_css-css'  href='<?php echo ConfigGlobal::$realPath."/"?>css/formValidation.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='flexslider_css-css'  href='<?php echo ConfigGlobal::$realPath."/"?>css/flexslider.css' type='text/css' media='all' />
    <link rel='stylesheet' id='common_css-css'  href='http://toatau.com/wp-content/themes/life/assets/css/common.css?ver=2' type='text/css' media='all' />
    <link rel='stylesheet' id='style_css-css'  href='<?php echo ConfigGlobal::$realPath."/"?>css/style_assets.css' type='text/css' media='all' />
    <link rel='stylesheet' id='blog_css-css'  href='<?php echo ConfigGlobal::$realPath."/"?>css/blog.css' type='text/css' media='all' />
    <script type='text/javascript' src='http://toatau.com/wp-content/themes/life/assets/vendor/js/jquery-1.11.2.min.js?ver=2'></script>
    <script type='text/javascript' src='http://toatau.com/wp-content/themes/life/assets/vendor/js/jquery.flexslider-min.js?ver=2'></script>
    <script type='text/javascript' src='http://toatau.com/wp-content/themes/life/assets/vendor/js/bootstrap.min.js?ver=2'></script>
    <script type='text/javascript' src='http://toatau.com/wp-content/themes/life/assets/vendor/js/jquery-ui.min.js?ver=2'></script>
    <script type='text/javascript' src='http://toatau.com/wp-content/themes/life/assets/vendor/js/formValidation.min.js?ver=2'></script>
    <script type='text/javascript' src='http://toatau.com/wp-content/themes/life/assets/vendor/js/framework/bootstrap.min.js?ver=2'></script>
    <script type='text/javascript' src='http://toatau.com/wp-content/themes/life/assets/vendor/js/language/vi_VN.js?ver=2'></script>
    <script type='text/javascript' src='http://toatau.com/wp-content/themes/life/assets/js/main.js?ver=2'></script>
    <script type='text/javascript' src='http://toatau.com/wp-content/themes/life/assets/js/toatau.js?ver=2'></script>
    <meta name="generator" content="Super AI 1.0.0" />
    <link rel='canonical' href='<?php echo ConfigGlobal::$realPath ?>' />
    <link rel='shortlink' href='<?php echo ConfigGlobal::$realPath ?>' />
    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <meta property="fb:admins" content="thereds1106"/>
    <meta property="fb:app_id" content="1676796799215601"/>
    <meta property="og:url" content="<?php echo ConfigGlobal::$realPath ?>"/>
    <meta property="og:title" content="<?php echo $dataCurrentPage->gettitle(); ?>"/>
    <meta property="og:site_name" content="<?php echo $dataCurrentPage->gettitle(); ?>"/>
    <meta property="og:description" content="<?php echo $dataCurrentPage->getmeta_description(); ?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content=""/>
    <meta property="og:locale" content="vi_VN"/>
    <style type="text/css">.mc4wp-form input[name="_mc4wp_required_but_not_really"] { display: none !important; }</style>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-58050608-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- END GADWP Universal Tracking -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo ConfigGlobal::$realPath."/image/".$logo->getvalue(); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo ConfigGlobal::$realPath."/image/".$logo->getvalue(); ?>">
    <script type="text/javascript">
        var GlobalConfigs = {
            URL: "http://toatau.com"
        };
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '1491344647838450');
        fbq('track', "PageView");</script>
    <noscript>
        <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1491344647838450&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
<div class="container">
    <?php include_once("header.php"); ?>
    <div class="page-path row">
        <a href="<?php echo ConfigGlobal::$realPath ?>">HomePage</a>
        >>
        <a href="<?php echo $dataCurrentPage->getidentifier();  ?>"><?php echo $dataCurrentPage->gettitle(); ?></a>
    </div>
    <div id="content" class="row">
        <?php if($dataCurrentPage->getstatus()==0){ ?>
            <?php echo $config->loadbyPath("404_error_page"); ?>
        <?php }else{ ?>
        <div class="page-title">
            <h2><?php echo $dataCurrentPage->gettitle(); ?></h2>
        </div>
        <div class="row">
            <?php  echo $dataCurrentPage->getcontent(); ?>
        </div>
        <?php } ?>
    </div>
    <?php include_once("footer.php"); ?>
</div>
</body>
</html>
