<?php
@session_start();
include_once("Article.php");
include_once("Menu.php");
include_once("Attribute.php");
include_once("AttributeValue.php");
include_once("processingImage.php");
include_once("Config.php");
include_once("Giaodich.php");
$config= new ConfigGlobal("config","config_id");
$menu  = new Menu("menu","id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$article = new Article("article","id_article");
$attributeValue= new AttributeValue("entity_attribute_value_varchar","id_eav_varchar");
?>
<!doctype html>
<!--[if lt IE 9]> <html class="no-js ie-old" lang="vi"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="vi"> <![endif]-->
<!--[if gt IE 8]> <html lang="vi"> <!--<![endif]-->
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<?php include_once("head.php"); ?>
<body>
<div class="home-page-edit">
    <?php include_once("header.php"); ?>
    <div class="main-content">
        <div class="container">
            <div id="pSport">
                <style>
                    .left_ct img{width:290px;margin-top:50px;}
                    strong {  font-weight:bold !important;}
                </style>
                <div class="right_ct" style="width: 100%">
                    <div class="breadcrumb-wrap">
                        <ul class="breadcrumb breadcrumb-cus">
                            <li><a href="<?php echo ConfigGlobal::$realPath; ?>">Trang chủ</a></li>
                            <li><a href="<?php echo  ConfigGlobal::$realPath ?>/about-us.html">Về chúng tôi</a></li>
                        </ul>
                    </div>
                    <h1 class="content_header title-details">
                        <span class="triangle-left" style="left: -12px;"></span>
                        <span class="txt-title">Về Chúng Tôi</span>
                        <span class="triangle-right"></span>
                    </h1>
                    <div class="post-details-ext">
                        <p style="text-align: justify;"><strong>Hãy đến với 88CUOC.COM&nbsp;để trải nghiệm&nbsp;dịch vụ <a href="<?php echo ConfigGlobal::$realPath; ?>">cá cược trực tuyến</a> tốt nhất !</strong></p>

                        <p style="text-align: justify;"><strong>1. Nhà cái lớn và uy tín nhất khu vực</strong></p>

                        <p style="text-align: justify;"><strong>88CUOC.COM&nbsp;&nbsp;</strong>thành lập từ năm 2011, được Ủy quyền và cấp phép trực tiếp bởi <strong>Lucky89 Casino & Resort, Svey Rieng, Cambodia.</strong>&nbsp;Với bề dày kinh nghiệm trong lĩnh vực cá cược online và đội ngũ nhân viên hùng hậu, nhiều kinh nghiệm&nbsp;<strong>88CUOC.COM&nbsp;</strong>hứa hẹn đem đến sự hài lòng viên mãn cho khách hàng tại Việt Nam và khu vực.</p>

                        <p style="text-align: justify;"><img alt="" src="<?php echo ConfigGlobal::$realPath; ?>/image/about-us-lucky.jpeg" style="opacity: 0.9; line-height: 20.8px;"></p>

                        <p style="text-align: justify;"><strong>2. Thanh toán tiền cực nhanh</strong></p>

                        <p style="text-align: justify;">&nbsp;</p>

                        <p style="text-align: justify;"><img alt="" src="https://img.388bet.com/images/abouts/388bet_AboutUs_Banner_2.png" style="width: 660px; height: 383px;"></p>

                        <p style="text-align: justify;">&nbsp;</p>

                        <p style="text-align: justify;">Nạp - Rút Tiền là một thế mạnh của <strong>88CUOC</strong>. Các phương thức thanh toán nhanh gọn&nbsp;nhất, làm hài lòng&nbsp;khách hàng,&nbsp;<strong>88CUOC</strong> hỗ trợ hơn 20 ngân hàng thông dụng ở Việt Nam như Vietcombank, Đông Á, ACB, Techcombank, Sacombank… giúp khách hàng gửi/rút tiền vô cùng dễ dàng và &nbsp;nhanh chóng.</p>

                        <p style="text-align: justify;">– Gửi tiền và Rút tiền: Thời gian xử lý lệnh chỉ từ 1 đến 5 phút, Quý khách không tốn bất kỳ một chi phí dịch vụ nào.</p>

                        <p style="text-align: justify;">Việc thanh toán được thực hiện dễ dàng với nhiều lựa chọn. <strong>88CUOC</strong> nỗ lực mang đến cho khách hàng các phương thức thanh toán tốt nhất, phù hợp với từng cá nhân đồng thời đảm bảo an toàn, tính tiện dụng. Có ngân hàng riêng theo yêu cầu cho các khách hàng VIP.</p>

                        <p style="text-align: justify;"><strong>3. Sản phẩm đa dạng</strong></p>

                        <p style="text-align: justify;">&nbsp;</p>

                        <p style="text-align: justify;"><img alt="" src="https://img.388bet.com/images/abouts/388bet_AboutUs_Banner_3.png" style="width: 682px; height: 240px;"></p>

                        <p style="text-align: justify;">&nbsp;</p>

                        <p style="text-align: justify;">Ngoài việc <a href="<?php echo ConfigGlobal::$realPath ?>">cá cược bóng đá</a>, bạn còn có thể tham gia cá cược các môn thể thao khác như: bóng rổ, đua ngựa, cầu lông, tennis, bóng bầu dục, đua xe… hoặc&nbsp;tham gia casino trực tuyến với các trò Đá Gà, Xóc đĩa, Lô Đề, Keno, Poker, Baccarat, Roulette. Những ván bài cân não và các&nbsp;<span class="text" style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: 0px 0px;">Dealer</span> xinh đẹp đang chờ đón bạn.</p>

                        <p style="text-align: justify;"><strong>88CUOC</strong> không ngừng cải tiến sản phẩm đi cùng với những bổ sung mới, nhằm&nbsp;mang đến&nbsp;dịch vụ cá cược&nbsp;trực tuyến tốt nhất cho Quý Khách.</p>

                        <p style="text-align: justify;"><strong>4. Dịch vụ chăm sóc khách hàng 24/7</strong></p>

                        <p style="text-align: justify;">&nbsp;</p>

                        <p style="text-align: justify;"><img alt="" src="https://img.388bet.com/images/abouts/388bet_AboutUs_Banner_4.png" style="width: 682px; height: 240px;"></p>

                        <p style="text-align: justify;">&nbsp;</p>

                        <p style="text-align: justify;">Nhà cái <strong>88CUOC</strong> tập trung vào nhu cầu của Khách Hàng và với phương châm:</p>

                        <p style="text-align: justify;">– Khách Hàng là Thượng Đế.</p>

                        <p style="text-align: justify;">– Sự hài lòng của Quý khách đem đến thành công cho <strong>88CUOC</strong></p>

                        <p style="text-align: justify;">– Phương châm phục vụ mỗi ngày một tốt hơn.</p>

                        <p style="text-align: justify;">Nỗ lực mang đến sự trải nghiệm tốt nhất về chất lượng dịch vụ khách hàng và đón nhận mọi ý kiến phản hồi từ Quý Khách để không ngừng nâng cao dịch vụ của <strong>88CUOC</strong>. Nếu Quý Khách có bất kỳ thắc mắc hoặc góp ý, đội ngũ&nbsp;dịch vụ chăm sóc khách hàng thân thiện của <strong>88CUOC</strong> luôn sẵn sàng lắng nghe và hỗ trợ.</p>

                        <p style="text-align: justify;"><strong id="hoahong">5. Tiền thưởng thể thao và khuyến mãi</strong></p>

                        <p style="text-align: justify;"><img alt="" src="https://img.388bet.com/images/abouts/388bet_AboutUs_Banner_5.png"></p>

                        <p style="text-align: justify;">&nbsp;</p>

                        <p style="text-align: justify;">Hoàn tiền&nbsp;cao nhất tại Việt Nam, chúng tôi sẵn sàng hoàn tiền nếu có nhà cái khác thưởng&nbsp;cao hơn. Các chương trình tặng thưởng theo quý cùng&nbsp;nhiều khuyến mãi đặc biệt khác từ Thể thao, Casino trực tuyến cho đến Keno, chơi Poker online. Hãy truy cập <strong>88CUOC</strong> và cập nhật những khuyến mãi hấp dẫn mới nhất. Chắc chắn Quý khách sẽ cảm thấy thật hài lòng về dịch vụ của chúng tôi.</p>

                        <p style="text-align: justify;"><strong id="visao">6. Vì sao 88CUOC hợp pháp</strong></p>

                        <p style="text-align: justify;"><img alt="" src="https://img.388bet.com/images/abouts/388bet_AboutUs_Banner_6.png" style="width: 682px; height: 600px;"></p>

                        <p style="text-align: justify;">+ Chúng tôi không lưu thông tin khách hàng.&nbsp;<br>
                            + Cam kết mỗi giao dịch thanh toán đều hợp pháp bằng nội dung thương mại.<br>
                            + Tài khoản cá cược và tài khoản giao dịch thanh toán không liên quan đến nhau. B<span class="s1">ạn không cá cược trực tiếp trên 88CUOC, bạn không lo sợ vi phạm pháp luật.</span></p>                                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function(){
            $(function() {
                var act=0;
                if(window.location.hash) {
                    act =parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs({active: act});
            });
            $(window).on('hashchange', function() {
                var act=0;
                if(window.location.hash) {
                    act = parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs( "option", "active", act );
            });

        });
    </script>
    <?php include_once("footer.php") ?>
</div>
<script type="text/javascript">


    var imgArr=['<?php echo ConfigGlobal::$realPath ?>/image/home_jalan_vi-VN.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/ad_fastmarket.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/banner3_en.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/cock_fight.jpg'];
    var numberSilider =Math.floor((Math.random() * imgArr.length));
    //alert(imgArr);
    var url =imgArr[numberSilider];
    $('#imagesRandom').attr('src',url);

    $(document).ready(function(){
        $('.onNap').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            if(a == 'Nạp'){
                $('.onRut').text('Rút');
                $("#lb-naprut-caption").text("Rút tiền");

            }
            else{
                $('.onRut').text('Nạp');
                $("#lb-naprut-caption").text("Nạp tiền");
            }
            $('.chooseAction').toggle()

        })
        $('.onRut').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            //show_invoice_form();
            if(a == 'Nạp'){
                $(".form-controlCustom").show();
                $('#formChooseBankPull').hide();
                $('#formChooseBankPush').hide();
                $(".phieu-nap-text").hide();
                $(".phieu-rut-text").show();
                $('.onNap').text('Rút');
                $('.chooseAction').hide()
            }else{
                $(".form-controlCustom").show();
                $('#formChooseBankPush').hide();
                $('#formChooseBankPull').hide();
                $(".phieu-nap-text").show();
                $(".phieu-rut-text").hide();
                $('.onNap').text('Nạp');
                $('.chooseAction').hide()
            }

        });
        $('#basic').popup();
        $('#fade').popup();

        $('#phone_fix_dialog').popup();
        $('#bankinfo').popup({
            blur:false,
            beforeopen:function(){
                // $("#invoice-content").html("<p>content here!!!!</p>");

            }
        });



    });
    function show_invoice_form(){
        //$(".form-controlCustom").hide();
//    $(".frm-invoice-panel").show();
        var a = $('.onNap').text();
        if(a == 'Nạp'){
            $('#formChooseBankPull').hide();
            $('#formChooseBankPush').show();
        }else{
            $('#formChooseBankPush').hide ();
            $('#formChooseBankPull').show();
        }
    }
</script>
</body>
</html>
