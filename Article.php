<?php
include_once ("Collection.php");
class Article extends Collection{
    function __construct($tableName, $primaryKey)
    {
        parent::__construct($tableName, $primaryKey);
    }
    //On Grid
    //Giao Tiếp Model
    function getCollectionArticleBySql(){
        $sql="SELECT * from article inner join entity_attribute_value_varchar on article.id_article=entity_attribute_value_varchar.entity_id";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getCollectionArticleOnPgae($start,$limit){
        $sql="select  * from article limit $start,$limit";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    //overidie custom column on collection call and suppper class
    function getCollectionArticleOnPageByCustomColumn($start,$limit,$column,$condition,$value){
        $sql="";
        switch ($condition){
            case "like":
                $sql="select  * from article where `$column` like '%$value%' limit $start,$limit";
                break;
        }
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
}