<?php
date_default_timezone_set("Asia/Ho_Chi_Minh");
include_once("Collection.php");
define('SUPERAI_ROOT',$_SERVER['DOCUMENT_ROOT']);
class ConfigGlobal extends  Collection{

    public static $rootWeb=SUPERAI_ROOT;
    public static $realPath="http://www.dev388.com";
    public static $_DANG_XU_LY = "Đang Xử Lý";
    public static $_HOAN_THANH = "Hoàn Thành";
    public static $_TU_CHOI = "Từ Chối";
    public static $_GIAO_DICH_THAT_BAI = "Giao Dịch Thật Bại";
    public static function getHuongDan(){
        return self::$realPath."/huong-dan.html";
    }
    function __construct($tableName, $primaryKey)
    {
        parent::__construct($tableName, $primaryKey);
    }
    function loadbyPath($configName){
        $data=$this->loadByAttribute("config_name","eq",$configName,1);
        return $data->getvalue();
    }
    function filterMoney($money){
        //for PHP 7.0
        return number_format($money,0,",",".");
        //for PHP  < 5.0
    }
}