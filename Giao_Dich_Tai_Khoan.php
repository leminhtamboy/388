<?php
include_once ("Collection.php");
class Giao_Dich_Tai_Khoan extends  Collection{

    public function __construct($tableName, $primaryKey)
    {
        parent::__construct($tableName, $primaryKey);
    }
    public function redirectToProcessingPage($href,$mainURL){
        ?>
        <script type="text/javascript">
            top.location.href="<?php echo $mainURL ?>/processing.html?href=<?php echo $href ?>";
        </script>

    <?php
    }
    public function getCurrentUrl(){
        return ConfigGlobal::$realPath."/tao-yeu-cau.html";
    }
}