<?php
include_once ("Collection.php");
class Attribute extends Collection{
    function __construct($tableName, $primaryKey)
    {
        parent::__construct($tableName, $primaryKey);
    }
    function getCollectionAttributeBySql(){
        $sql="SELECT * from entity_attribute where show_on_grid=1";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getCustomCollectionAttributeBySql($columnName,$condition,$value){
        $sql="";
        switch ($condition){
            case "eq":
                $sql.="SELECT * from entity_attribute where `$columnName`='$value'";
                break;
            case "like":
                $sql.="SELECT * from entity_attribute where `$columnName` like '%$value%'";
                break;
            case "neq":
                $sql.="SELECT * from entity_attribute where `$columnName` != '%$value%'";
                break;
        }
        $data=$this->getCollectionBySql($sql);
        return $data;
    }

}