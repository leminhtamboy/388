
function showDetailInfor(a,b,c,d,e,f){
        $("name").value=a;
        $("sku").value=b;
        $("weight").value=c;
        $("chuong_trinh_product_id").value=f;
        if(e=="1"){
            $("status").options[1].selected=true;
            $("status").options[2].selected=false;
        }
        if(e=="0"){
            $("status").options[2].selected=true;    
            $("status").options[1].selected=false;
        }
        
        if(d=="1"){
            $("url_key").value="Còn Hàng";
        }else{
            $("url_key").value="Hết Hàng";
        }
        return ;
}
function showProductAccosiated(id,id_chuong_trinh,url,name){
    new Ajax.Request(url,{
        method:"post",
        parameters: {don_vi_id:id,chuong_trinh_id:id_chuong_trinh,name:name},
        onSuccess: function(response) {
            $("related_product_grid_san_pham").innerHTML=response.responseText;
            $("loading-mask").setAttribute("style","display:block");
        }
    });
}
function addProduct(){
    $("popup_open").style.display="block";
}
function them_san_pham_cho_chuong_trinh(url_them_san_pham_vao_chuong_trinh,id_chuong_trinh){
    var stringProduct="";
    var loi=true;
    $$(".check_box_popup_product").each(function(e){
        if(e.checked==true){
               stringProduct+=e.value+"|";
               if($("so_luong_"+e.value).value==""){
                  $("validate_"+e.value).setAttribute("style","display:block");
                  loi=false;
               }
        }
    });
    if(loi==false){
        alert(Translator.translate("Xin Vui Lòng Điền Số Lượng Cho Mỗi Sản Phẩm Của Chương Trình Trả Góp"));
        return false;
    }
    if(stringProduct==""){
        alert(Translator.translate("Xin Vui Lòng Chọn Sản Phẩm Cần Thêm"));
        return false;
    }
    stringProduct=stringProduct.substring(0,stringProduct.length-1);
    new Ajax.Request(url_them_san_pham_vao_chuong_trinh,{
                method:"post",
                parameters:{productIds:stringProduct,id_chuong_trinh:id_chuong_trinh},
                onSuccess: function(response) {
                    console.log(response.responseText);
                }
    });
}
function editProduct(chuong_trinh_id){
    var product_id=$("chuong_trinh_product_id").value;
    var so_luong=$("weight").value;
}
function deleteProduct(chuong_trinh_id,url){
    var product_id=$("chuong_trinh_product_id").value;
    new Ajax.Request(url,{
        method:"post",
        parameters: {chuong_trinh_id:chuong_trinh_id,product_id:product_id},
        onSuccess: function(response) {
           UpdateLuoiProductChuongTrinh();
           console.log(response.responseText);
        }
    });
}
function cutString(tempString){
    var retString="";
    retString=tempString.substring(0,tempString.length-1);
    return retString;
}
function them_san_pham_cho_don_vi_tra_gop(url,id_chuong_trinh){
    var stringProduct="";
    var stringSoLuong="";
    var stringMin="";
    var stringMax="";
    var loi=true;
    if(!$("id_don_vi_tra_gop_san_pham")){
        alert(Translator.translate("Xin Vui Lòng Chọn Đơn Vị Cần Thểm Sản Phẩm"));
        return false;
    }
    var id_don_vi=$("id_don_vi_tra_gop_san_pham").value;
    $$(".check_box_don_vi_product").each(function(e){
        if(e.checked==true){
               var so_luong=$("don_vi_san_pham_so_luong_"+e.value).value;
               var dv_min=$("don_vi_san_pham_min_"+e.value).value;
               var dv_max=$("don_vi_san_pham_max_"+e.value).value;
               stringProduct+=e.value+"|";
               stringSoLuong+=so_luong+"|";
               stringMin+=dv_min+"|";
               stringMax+=dv_max+"|";
               /*if($("so_luong_"+e.value).value==""){
                  $("validate_"+e.value).setAttribute("style","display:block");
                  loi=false;
               }*/
        }
    });
    /*if(loi==false){
        alert(Translator.translate("Xin Vui Lòng Điền Số Lượng Cho Mỗi Sản Phẩm Của Chương Trình Trả Góp"));
        return false;
    }*/
    if(stringProduct==""){
        alert(Translator.translate("Xin Vui Lòng Chọn Sản Phẩm Cần Thêm"));
        return false;
    }
    
    stringProduct=cutString(stringProduct);
    stringSoLuong=cutString(stringSoLuong);
    stringMin=cutString(stringMin);
    stringMax=cutString(stringMax);
    new Ajax.Request(url,{
                method:"post",
                parameters:{productIds:stringProduct,stringsoluong:stringSoLuong,stringmin:stringMin,stringmax:stringMax,id_chuong_trinh:id_chuong_trinh,id_don_vi:id_don_vi},
                onSuccess: function(response) {
                    console.log(response.responseText);
                }
    });
}
function xoa_san_pham_cho_don_vi_tra_gop(url,id_chuong_trinh){
    var stringProduct="";
    $$(".check_don_vi_san_pham_da_add").each(function(e){
        if(e.checked==true){
               stringProduct+=e.value+"|";
        }
    });
    if(stringProduct==""){
        alert(Translator.translate("Xin Vui Lòng Chọn Sản Phẩm Cần Xóa"));
        return false;
    }
    stringProduct=cutString(stringProduct);
    var id_don_vi=$("id_don_vi_tra_gop_san_pham").value;
    new Ajax.Request(url,{
                method:"post",
                parameters:{productIds:stringProduct,id_chuong_trinh:id_chuong_trinh,id_don_vi:id_don_vi},
                onSuccess: function(response) {
                    console.log(response.responseText);
                }
    });
}
//code for đơn vị trả góp
function saveDonVi(url,urlBack,urlUpload,isContinue){
    $("loading-mask").setAttribute("style","");
    
    var ten_don_vi=$("ten_don_vi").value;
    var dia_chi=$("dia_chi").value;
    var nguoi_lien_lac=$("nguoi_lien_lac").value;
    var email=$("email").value;
    var so_thang_vay=$("so_thang_vay").value;
    var so_luong_gioi_han=$("so_luong_gioi_han").value;
    var tinh_trang=0;
    var i=0;
    for(i=0;i<$("tinh_trang").length;i++){
        if($("tinh_trang")[i].selected){
            tinh_trang=$("tinh_trang")[i].value;
            break;
        }
    }
    new Ajax.Request(urlUpload,{
        method:"post",
        parameters: new FormData($("edit_form")),
        onSuccess: function(response) {
            $("loading-mask").setAttribute("style","");
        }
    });
            
    new Ajax.Request(url, {
        method: 'post',
        parameters: {ten_don_vi:ten_don_vi,dia_chi:dia_chi,nguoi_lien_lac:nguoi_lien_lac,email:email,so_thang_vay:so_thang_vay,so_luong_gioi_han:so_luong_gioi_han,tinh_trang:tinh_trang},
        asynchronous: false,
        onCreate:function(){
        },
        onSuccess: function() {
            $("loading-mask").setAttribute("style","");
            if(isContinue==false){
                window.location=urlBack;
            }
        },
        onComplete: function(){
            $("loading-mask").setAttribute("style","");
        }
    });
    
}
function convertToOrder(url){
     $("loading-mask").setAttribute("style","");
    //block customer
    var firstName=$("ho_ten").text;
    var lastName=$("ho_ten").text;
    var email=$("email").text;
    //end block customer
    
    //block billing address
    var duong=$("duong").innerText;
    var quan=$("quan").innerText;
    var thanh_pho=$("thanh_pho").innerText;
    var region="Viet Nam";
    var dien_thoai=$("dien_thoai").innerText;
    //block shipping address
    //product
    var ma_don_hang=$("ma_don_hang").value;
    new Ajax.Request(url, {
        method: 'post',
        //parameters: {ten_don_vi:ten_don_vi,dia_chi:dia_chi,nguoi_lien_lac:nguoi_lien_lac,email:email,so_thang_vay:so_thang_vay,so_luong_gioi_han:so_luong_gioi_han,tinh_trang:tinh_trang},
        parameters:{
            email:email, 
            firstName:firstName, 
            lastName:lastName,
            duong:duong,
            quan:quan,
            thanh_pho:thanh_pho,
            dien_thoai:dien_thoai,
            ma_don_hang:ma_don_hang
        },
        asynchronous: false,
        onCreate:function(){
            $("loading-mask").setAttribute("style","");
        },
        onSuccess: function(result) {
            $("loading-mask").setAttribute("style","");
            $("duong").innerHTML=result.responseText;
        },
        onComplete: function(){
            $("loading-mask").setAttribute("style","display:none");
        }
    });
}
