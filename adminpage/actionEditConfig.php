<?php include_once("../Message.php") ?>
<?php include_once("../Config.php") ?>
<?php
$config = new ConfigGlobal("config","config_id");
$hasMessage="";
$idConfig=$_REQUEST["id_config"];
$dataCurrentConfig=$config->load($idConfig);
if(isset($_REQUEST["action"])){
    $action=$_REQUEST["action"];
    switch ($action){
        case "save":{
            $config->setData("config_id",$idConfig);
            $config->setData("config_name",$_POST["config_name"]);
            $config->setData("value",$_POST["config_value"]);
            $config->setData("notes",$_POST["notes"]);
            $config->updateRow();
            ?>
            <script>
                top.location="managerConfig.php";
            </script>
            <?php
            break;
        }
        case "delete":{
            $config->deleteRow($idConfig);
            ?>
            <script>
                top.location="managerConfig.php";
            </script>
            <?php
            break;
        }
        case "saveandcontinue":{
            $config->setData("config_id",$idConfig);
            $config->setData("config_name",$_POST["config_name"]);
            $config->setData("value",$_POST["config_value"]);
            $config->setData("notes",$_POST["notes"]);
            $config->updateRow();
            $message = new Message();
            $hasMessage=$message->addSuccess("Cập Nhật Config Thành Công");
            $dataCurrentConfig=$config->load($idConfig);
            break;
        }

    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Quản Lý Cấu Hình</title>
    <link rel="icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <?php include_once("head.php"); ?>
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>

<body id="html-body" class=" adminhtml-catalog-category-edit">
<div class="wrapper">
    <?php include_once("header.php"); ?>
    <div class="notification-global">
    <span class="f-right">
        Adminpanel Cho Bếp Chia Sẻ
    </span>
        <strong class="label">Latest Message:</strong>Trài Nghiệm Phiên Bản MappingDatabase Mới tại website thiekesmartweb.com<a href="http://thiekesmartweb.com" onclick="this.target='_blank';">Read details</a>
    </div>
    <div class="middle" id="anchor-content">
        <div id="page:main-container">
            <div class="columns ">
                <div class="side-col" id="page:left">
                    <h3>Thông Tin Cấu Hình</h3>
                    <ul id="page_tabs" class="tabs">
                        <li>
                            <a href="#" id="page_tabs_main_section" name="main_section" title="Page Information" class="tab-item-link active">
                                <span><span class="changed" title="The information in this tab has been changed."></span><span class="error" title="This tab contains invalid data. Please solve the problem before saving."></span>Thông Tin Chung</span>
                            </a>

                        </li>
                    </ul>
                    <script type="text/javascript">
                        page_tabsJsTabs = new varienTabs('page_tabs', 'edit_form', 'page_tabs_main_section', []);
                        function save(){
                            jQuery("#edit_form").attr("action","?action=save&id_config=<?php echo $idConfig; ?>");
                            jQuery("#edit_form").submit();
                        }
                        function saveContinueEdit(){
                            jQuery("#edit_form").attr("action","?action=saveandcontinue&id_config=<?php echo $idConfig; ?>");
                            jQuery("#edit_form").submit();
                        }
                        function actiondelete(){
                            var agree=confirm("Bạn Chắc Muốn Xóa Cấu Hình Này");
                            if(agree){
                                jQuery("#edit_form").attr("action","?action=delete&id_config=<?php echo $idConfig; ?>");
                                jQuery("#edit_form").submit();
                            }
                        }
                    </script>
                </div>
                <div class="main-col" id="content">
                    <div class="main-col-inner">
                        <?php echo $hasMessage; ?>
                        <div class="content-header">
                            <h3 class="icon-head head-cms-page">Edit Page '<?php echo $dataCurrentConfig->getconfig_name(); ?>'</h3>
                            <p class="form-buttons"><button id="id_be8293536b8ff4ac1bd2995affe48db1" title="Back" type="button" class="scalable back" onclick="top.location='managerConfig.php'" style=""><span><span><span>Back</span></span></span></button>
                                <button id="id_0fa38af4e160ee47789c3d2ee2649a7c" title="Reset" type="button" class="scalable " onclick="setLocation(window.location.href)" style=""><span><span><span>Reset</span></span></span></button>
                                <button id="id_0de035b29bb172d37b95c017409b200d" title="Delete Page" type="button" class="scalable delete" onclick="actiondelete();" style=""><span><span><span>Delete Page</span></span></span></button>
                                <button id="id_fd666ca87f6cd2f3eba21118e3787725" title="Save Page" type="button" class="scalable save" onclick="save();" style=""><span><span><span>Save Page</span></span></span></button>
                                <button id="id_8648b030bc24db35281905b99c241526" title="Save and Continue Edit" type="button" class="scalable save" onclick="saveContinueEdit()" style=""><span><span><span>Save and Continue Edit</span></span></span></button></p>
                        </div>
                        <div class="entry-edit">
                            <form id="edit_form" action="#" method="post"><div>
                                    <div class="entry-edit-head">
                                        <h4 class="icon-head head-edit-form fieldset-legend">Thông Tin Cấu Hình</h4>
                                        <div class="form-buttons"></div>
                                    </div>
                                    <div class="fieldset " id="page_base_fieldset">
                                        <div class="hor-scroll">
                                            <table cellspacing="0" class="form-list">
                                                <tbody>
                                                <tr>
                                                    <td colspan="2" class="hidden">
                                                        <input id="page_page_id" name="page_id" value="<?php echo $idConfig ?>" type="hidden"></td>
                                                </tr>
                                                <tr>
                                                    <td class="label"><label for="page_title">Tên Cấu Hình <span class="required">*</span></label></td>
                                                    <td class="value">
                                                        <input id="config_name" name="config_name" value="<?php echo $dataCurrentConfig->getconfig_name(); ?>" title="Page Title" type="text" class=" input-text required-entry">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label"><label for="page_identifier">Giá Trị<span class="required">*</span></label></td>
                                                    <td class="value" style="width: 100%">
                                                        <textarea name="config_value" title="" id="config_value" class="textarea  required-entry" style="height:36em;" rows="2" cols="15"><?php echo $dataCurrentConfig->getvalue(); ?></textarea>
                                                        <script type="text/javascript">
                                                            if(CKEDITOR.instances["config_value"]){

                                                            }else{
                                                                CKEDITOR.replace("config_value");
                                                            }
                                                        </script>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label"><label for="page_title">Ghi Chú</label></td>
                                                    <td class="value">
                                                        <input id="notes" name="notes" value="<?php echo $dataCurrentConfig->getnotes(); ?>" title="Ghi Chú" type="text" class=" input-text">
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div id="page_tabs_content_section_content" style="display: none;"><div class="entry-edit">
                                <div class="entry-edit-head">
                                    <h4 class="icon-head head-edit-form fieldset-legend">Nội Dung</h4>
                                    <div class="form-buttons"></div>
                                </div>
                                <div class="fieldset fieldset-wide" id="page_content_fieldset">
                                    <div class="hor-scroll">
                                        <table cellspacing="0" class="form-list">
                                            <tbody>
                                            <tr>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="page_tabs_design_section_content" style="display: none;"><div class="entry-edit">
                                <div class="entry-edit-head">
                                    <h4 class="icon-head head-edit-form fieldset-legend">Page Layout</h4>
                                    <div class="form-buttons"></div>
                                </div>
                                <div class="fieldset fieldset-wide" id="page_layout_fieldset">
                                    <div class="hor-scroll">
                                        <table cellspacing="0" class="form-list">
                                            <tbody>
                                            <tr>
                                                <td class="label"><label for="page_root_template">Layout <span class="required">*</span></label></td>
                                                <td class="value">
                                                    <select id="page_root_template" name="root_template" class=" required-entry select">
                                                        <option value="empty">Empty</option>
                                                        <option value="one_column">1 column</option>
                                                        <option value="two_columns_left" selected="selected">2 columns with left bar</option>
                                                        <option value="two_columns_right">2 columns with right bar</option>
                                                        <option value="three_columns">3 columns</option>
                                                    </select>            </td>
                                            </tr>
                                            <tr>
                                                <td class="label"><label for="page_layout_update_xml">Layout Update XML</label></td>
                                                <td class="value">
                                                    <textarea id="page_layout_update_xml" name="layout_update_xml" style="height:24em;" rows="2" cols="15" class=" textarea"></textarea>            </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="entry-edit-head">
                                    <h4 class="icon-head head-edit-form fieldset-legend">Custom Design</h4>
                                    <div class="form-buttons"></div>
                                </div>
                                <div class="fieldset fieldset-wide" id="page_design_fieldset">
                                    <div class="hor-scroll">
                                        <table cellspacing="0" class="form-list">
                                            <tbody>
                                            <tr>
                                                <td class="label"><label for="page_custom_theme_from">Custom Design From</label></td>
                                                <td class="value">
                                                    <input name="custom_theme_from" id="page_custom_theme_from" value="" class="validate-date validate-date-range date-range-custom_theme-from input-text" type="text" style="width:110px !important;"> <img src="http://www.ma_1_9_2_data.com/skin/adminhtml/default/default/images/grid-cal.gif" alt="" class="v-middle" id="page_custom_theme_from_trig" title="Select Date" style="">
                                                    <script type="text/javascript">
                                                        //<![CDATA[
                                                        Calendar.setup({
                                                            inputField: "page_custom_theme_from",
                                                            ifFormat: "%m/%e/%Y",
                                                            showsTime: false,
                                                            button: "page_custom_theme_from_trig",
                                                            align: "Bl",
                                                            singleClick : true
                                                        });
                                                        //]]>
                                                    </script>            </td>
                                            </tr>
                                            <tr>
                                                <td class="label"><label for="page_custom_theme_to">Custom Design To</label></td>
                                                <td class="value">
                                                    <input name="custom_theme_to" id="page_custom_theme_to" value="" class="validate-date validate-date-range date-range-custom_theme-to input-text" type="text" style="width:110px !important;"> <img src="http://www.ma_1_9_2_data.com/skin/adminhtml/default/default/images/grid-cal.gif" alt="" class="v-middle" id="page_custom_theme_to_trig" title="Select Date" style="">
                                                    <script type="text/javascript">
                                                        //<![CDATA[
                                                        Calendar.setup({
                                                            inputField: "page_custom_theme_to",
                                                            ifFormat: "%m/%e/%Y",
                                                            showsTime: false,
                                                            button: "page_custom_theme_to_trig",
                                                            align: "Bl",
                                                            singleClick : true
                                                        });
                                                        //]]>
                                                    </script>            </td>
                                            </tr>
                                            <tr>
                                                <td class="label"><label for="page_custom_theme">Custom Theme</label></td>
                                                <td class="value">
                                                    <select id="page_custom_theme" name="custom_theme" class=" select">
                                                        <option value="" selected="selected">-- Please Select --</option>
                                                        <optgroup label="base">
                                                            <option value="base/default">default</option>
                                                        </optgroup>
                                                        <optgroup label="default">
                                                            <option value="default/blank">blank</option>
                                                            <option value="default/default">default</option>
                                                            <option value="default/iphone">iphone</option>
                                                            <option value="default/modern">modern</option>
                                                        </optgroup>
                                                        <optgroup label="qmobile">
                                                            <option value="qmobile/default">default</option>
                                                        </optgroup>
                                                        <optgroup label="rwd">
                                                            <option value="rwd/default">default</option>
                                                        </optgroup>
                                                    </select>            </td>
                                            </tr>
                                            <tr>
                                                <td class="label"><label for="page_custom_root_template">Custom Layout</label></td>
                                                <td class="value">
                                                    <select id="page_custom_root_template" name="custom_root_template" class=" select">
                                                        <option value="" selected="selected">-- Please Select --</option>
                                                        <option value="empty">Empty</option>
                                                        <option value="one_column">1 column</option>
                                                        <option value="two_columns_left">2 columns with left bar</option>
                                                        <option value="two_columns_right">2 columns with right bar</option>
                                                        <option value="three_columns">3 columns</option>
                                                    </select>            </td>
                                            </tr>
                                            <tr>
                                                <td class="label"><label for="page_custom_layout_update_xml">Custom Layout Update XML</label></td>
                                                <td class="value">
                                                    <textarea id="page_custom_layout_update_xml" name="custom_layout_update_xml" style="height:24em;" rows="2" cols="15" class=" textarea"></textarea>            </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div><div id="page_tabs_meta_section_content" style="display: none;"><div class="entry-edit">
                                <div class="entry-edit-head">
                                    <h4 class="icon-head head-edit-form fieldset-legend">Meta Data</h4>
                                    <div class="form-buttons"></div>
                                </div>
                                <div class="fieldset fieldset-wide" id="page_meta_fieldset">
                                    <div class="hor-scroll">
                                        <table cellspacing="0" class="form-list">
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div></form></div>
                    <script type="text/javascript">
                        editForm = new varienForm('edit_form', '');
                    </script>
                    <script type="text/javascript">
                        function toggleEditor() {
                            if (tinyMCE.getInstanceById('page_content') == null) {
                                tinyMCE.execCommand('mceAddControl', false, 'page_content');
                            } else {
                                tinyMCE.execCommand('mceRemoveControl', false, 'page_content');
                            }
                        }

                        function saveAndContinueEdit(urlTemplate) {
                            var tabsIdValue = page_tabsJsTabs.activeTab.id;
                            var tabsBlockPrefix = 'page_tabs_';
                            if (tabsIdValue.startsWith(tabsBlockPrefix)) {
                                tabsIdValue = tabsIdValue.substr(tabsBlockPrefix.length)
                            }
                            var template = new Template(urlTemplate, /(^|.|\r|\n)({{(\w+)}})/);
                            var url = template.evaluate({tab_id:tabsIdValue});
                            editForm.submit(url);
                        }
                    </script>                        </div>
            </div>
        </div>

    </div>

</div>
</div>
<?php include_once ("footer.php")?>
</div>
<div id="loading-mask" style="display:none">
    <p class="loader" id="loading_mask_loader"><img src="skin/adminhtml/default/default/images/ajax-loader-tr.gif" alt="Loading..."/><br/>Please wait...</p>
</div>


</body>
</html>
