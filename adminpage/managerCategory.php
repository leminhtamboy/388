<?php include_once("../Message.php") ?>
<?php include_once("../Category.php") ?>
<?php
$category = new Category("category","id");
$dataCategory=$category->getCollection();
$hasMessage="";
$showTitle="Danh Mục Hiện Tại";
$categoryId=$dataCategory[0]->getid();
if(isset($_REQUEST["category_id"])){
    $categoryId=$_REQUEST["category_id"];
}
if(isset($_REQUEST["action"])){
    $action=$_REQUEST["action"];
    switch ($action){
        case "add" : {
            $showTitle="Thêm Mới Danh Mục";
            $categoryId=-1;
            break;
        }
        case "save":{
            if($_POST["id"]==-1) {
                $message = new Message();
                $category->setData("id","NULL");
                $category->setData("title",$_POST["title"]);
                $category->setData("is_active",$_POST["is_active"]);
                $category->setData("link",$_POST["link"]);
                $numRows=$category->inserRow();
                if($numRows!=0){
                    $hasMessage=$message->addSuccess("Thêm Danh Mục Thành Công");
                }
                ?>
                <script>
                    top.location="managerCategory.php";
                </script>
                <?php
            }else{
                $message = new Message();
                $categoryId=$_POST["id"];
                $category->setData("id",$categoryId);
                $category->setData("title",$_POST["title"]);
                $category->setData("is_active",$_POST["is_active"]);
                $category->setData("link",$_POST["link"]);
                $category->updateRow();
                $hasMessage=$message->addSuccess("Cập Nhật Danh Mục Thành Công");
                $dataCategory=$category->getCollection();
            }
            break;
        }
        case "delete":{
            $category->deleteRow($_POST["id"]);
            $message = new Message();
            $hasMessage=$message->addSuccess("Xóa Danh Mục Thành Công");
            $dataCategory=$category->getCollection();
            break;
        }
    }
}
$dataCurrentCategory=$category->load($categoryId);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Quản Lý Danh Mục</title>
    <link rel="icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <?php include_once("head.php"); ?>
</head>

<body id="html-body" class=" adminhtml-catalog-category-edit">
<div class="wrapper">
    <noscript>
        <div class="noscript">
            <div class="noscript-inner">
                <p><strong>JavaScript seems to be disabled in your browser.</strong></p>
                <p>You must have JavaScript enabled in your browser to utilize the functionality of this website.</p>
            </div>
        </div>
    </noscript>
    <?php include_once("header.php"); ?>
    <div class="notification-global">

    <span class="f-right">
                You have <span class="critical"><strong>13</strong> critical</span>, <strong>6</strong> major, <strong>19</strong> minor and <strong>199</strong> notice unread message(s). <a href="index.php/admin/notification/index/key/cc95c7f8bb0de047610eee786f6488ac/">Go to messages inbox</a>
    </span>
        <strong class="label">

            Latest Message:</strong> Increase your sales and productivity, while simplifying PCI compliance with exciting new Magento Community Edition 2.1 features.            <a href="https://magento.com/blog/magento-news/magento-enterprise-edition-21-unleashes-power-marketers-and-merchandisers " onclick="this.target='_blank';">Read details</a>
    </div>
    <div class="middle" id="anchor-content">
        <div id="page:main-container">

            <div class="columns ">
                <div class="side-col" id="page:left">
                    <div class="categories-side-col">
                        <div class="content-header">
                            <h3 class="icon-head head-categories">Danh Mục</h3>
                            <button  id="add_root_category_button" title="Add Root Category" type="button" class="scalable add" onclick="top.location='?action=add'" style=""><span><span><span>Thêm Danh Mục</span></span></span></button><br />
                        </div>

                        <div class="tree-holder">
                            <div id="tree-div" style="width:100%; overflow:auto;" class=" x-tree">
                                <ul class="x-tree-root-ct x-tree-lines" id="ext-gen5">
                                    <div class="x-tree-root-node">
                                        <li class="x-tree-node">
                                            <div class="x-tree-node-el folder active-category x-tree-node-expanded" id="extdd-1">
                                                <span class="x-tree-node-indent"></span>
                                                <img src="js/spacer.gif" class="x-tree-ec-icon x-tree-elbow-end-minus" id="ext-gen20">
                                                <img src="js/spacer.gif" class="x-tree-node-icon" unselectable="on" id="ext-gen17">
                                                <a hidefocus="on" href="#" tabindex="1" id="ext-gen14"><span unselectable="on" id="extdd-2">Danh Sách Danh Mục (<?php echo count($dataCategory); ?>)</span></a>
                                            </div>
                                            <ul class="x-tree-node-ct">
                                                <?php foreach($dataCategory as $cate){ ?>
                                                    <?php
                                                    $categoryIds=$cate->getid();
                                                    $nameCategory=$cate->gettitle();
                                                    ?>
                                                    <li class="x-tree-node">
                                                        <div class="x-tree-node-el folder active-category x-tree-node-collapsed  x-tree-node-leaf x-tree-selected" id="extdd-3">
                                                        <span class="x-tree-node-indent">
                                                            <img src="js/spacer.gif" class="x-tree-icon"></span>
                                                            <img src="js/spacer.gif" class="x-tree-ec-icon x-tree-elbow" id="ext-gen27">
                                                            <img src="js/spacer.gif" class="x-tree-node-icon" unselectable="on" id="ext-gen24">
                                                            <a hidefocus="on" href="?category_id=<?php echo $categoryIds; ?>" tabindex="1" id="ext-gen21"><span unselectable="on" id="extdd-4"><?php echo $nameCategory; ?></span></a>
                                                        </div>
                                                        <ul class="x-tree-node-ct"></ul>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-col" id="content">
                    <div class="main-col-inner">
                        <?php echo $hasMessage; ?>
                        <form action="?action=save" method="post" name="edit_form" id="edit_form" enctype="multipart/form-data">
                    <div id="category-edit-container" class="category-content">
                        <div class="content-header">
                            <h3 class="icon-head head-categories"><?php echo $showTitle; ?></h3>
                            <p class="content-buttons form-buttons">
                                <?php if($categoryId!=-1) { ?>
                                    <button id="id_41d388dbc923c5cbcf41dbf778b20693" title="Delete Menu" type="button" class="scalable delete" onclick="var con=confirm('Bạn Thực Sự Muốn Xóa');if(con==true){jQuery('#edit_form').attr('action','?action=delete');jQuery('#edit_form').submit();}" style=""><span><span><span>Xóa Danh Mục</span></span></span></button>
                                <?php } ?>
                                <button  id="id_362ad19a3fa1872766901734acc7619b" title="Save Category" type="button" class="scalable save" onclick="jQuery('#edit_form').submit();" style=""><span><span><span>Lưu Danh Mục</span></span></span></button>        </p>
                        </div>
                        <ul id="category_info_tabs" class="tabs-horiz">
                            <li>
                                <a href="#" id="category_info_tabs_group_4" title="Thông Tin Chung" class="tab-item-link ">
                                    <span><span class="changed" title="The information in this tab has been changed."></span><span class="error" title="This tab contains invalid data. Please solve the problem before saving."></span>Thông Tin Chung</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" id="category_info_tabs_products" title="Danh Sách Bài Viết" class="tab-item-link">
                                    <span><span class="changed" title="The information in this tab has been changed."></span><span class="error" title="This tab contains invalid data. Please solve the problem before saving."></span>Danh Sách Bài Viết</span>
                                </a>
                            </li>
                        </ul>
                        <iframe name="iframeSave" style="display:none; width:100%;" src="http://www.ma_1_9_2_data.com/js/blank.html"></iframe>
                        <form target="iframeSave" id="category_edit_form" action="#" method="post" enctype="multipart/form-data">
                            <div id="category_tab_content">
                                <div id="category_info_tabs_group_4_content" style="display:none"><div class="entry-edit">
                                        <div class="entry-edit-head">
                                            <h4 class="icon-head head-edit-form fieldset-legend">Thông Tin Chi Tiết</h4>
                                            <div class="form-buttons"></div>
                                        </div>
                                        <div class="fieldset fieldset-wide" id="group_4fieldset_group_4">
                                            <div class="hor-scroll">
                                                <table cellspacing="0" class="form-list">
                                                    <tbody>
                                                    <tr>
                                                        <td class="hidden" colspan="100">
                                                            <input id="" name="id" value="<?php echo $categoryId ?>" type="hidden"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="label"><label for="group_4name">Name <span class="required">*</span></label></td>
                                                        <td class="value">
                                                            <input id="group_4name" name="title" value="<?php
                                                            if($dataCurrentCategory!=null){
                                                                echo $dataCurrentCategory->gettitle();
                                                            }
                                                            ?>" class=" required-entry input-text required-entry" type="text"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="label"><label for="group_4is_active">Is Active <span class="required">*</span></label></td>
                                                        <td class="value">
                                                            <select id="group_4is_active" name="is_active" class=" required-entry required-entry select">
                                                                <option value="1" <?php
                                                                if($dataCurrentCategory!=null){
                                                                    $active=$dataCurrentCategory->getis_active();
                                                                    if($active==1){
                                                                        echo 'selected="selected"';
                                                                    }
                                                                }
                                                                ?>>Yes</option>
                                                                <option value="0" <?php
                                                                if($dataCurrentCategory!=null){
                                                                    $active=$dataCurrentCategory->getis_active();
                                                                    if($active==0){
                                                                        echo 'selected="selected"';
                                                                    }
                                                                }
                                                                ?> >No</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="label"><label for="group_4name">Seo Link <span class="required">*</span></label></td>
                                                        <td class="value">
                                                            <input id="group_4name" name="link" value="<?php
                                                            if($dataCurrentCategory!=null){
                                                                echo $dataCurrentCategory->getlink();
                                                            }
                                                            ?>" class=" required-entry input-text required-entry" type="text"/>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div></div>
                                <div id="category_info_tabs_products_content" style="display: none;">
                                    <div id="catalog_category_products">
                                        <table cellspacing="0" class="actions">
                                            <tbody><tr>
                                                <td class="pager">
                                                    Page
                                                    <img src="skin/adminhtml/default/default/images/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow">
                                                    <input type="text" name="page" value="1" class="input-text page" onkeypress="catalog_category_productsJsObject.inputPage(event, '30')">

                                                    <a href="#" title="Next page" onclick="catalog_category_productsJsObject.setPage('2');return false;"><img src="skin/adminhtml/default/default/images/pager_arrow_right.gif" alt="Go to Next page" class="arrow"></a>

                                                    of 30 pages            <span class="separator">|</span>
                                                    View            <select name="limit" onchange="catalog_category_productsJsObject.loadByElement(this)">
                                                        <option value="20" selected="selected">20</option>
                                                        <option value="30">30</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                        <option value="200">200</option>
                                                    </select>
                                                    per page<span class="separator">|</span>
                                                    Total 582 records found<span id="catalog_category_products-total-count" class="no-display">582</span>
                                                </td>
                                                <td class="filter-actions a-right">
                                                    <button id="" title="Reset Filter" type="button" class="scalable " onclick="catalog_category_productsJsObject.resetFilter()" style=""><span><span><span>Reset Filter</span></span></span></button>
                                                    <button id="" title="Search" type="button" class="scalable task" onclick="catalog_category_productsJsObject.doFilter()" style=""><span><span><span>Search</span></span></span></button>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                        <div class="grid">
                                            <div class="hor-scroll">
                                                <table cellspacing="0" class="data" id="catalog_category_products_table">
                                                    <colgroup><col width="55">
                                                        <col width="60">
                                                        <col>
                                                        <col width="80">
                                                        <col width="1">
                                                        <col width="1">
                                                    </colgroup><thead>
                                                    <tr class="headings">
                                                        <th class="a-center"><span class="nobr"><input type="checkbox" name="" onclick="catalog_category_productsJsObject.checkCheckboxes(this)" class="checkbox" title="Select All"></span></th>
                                                        <th><span class="nobr"><a href="#" name="entity_id" title="asc" class="sort-arrow-desc"><span class="sort-title">ID</span></a></span></th>
                                                        <th><span class="nobr"><a href="#" name="name" title="asc" class="not-sort"><span class="sort-title">Tiêu Đề Bài Viết</span></a></span></th>
                                                        <th><span class="nobr"><a href="#" name="sku" title="asc" class="not-sort"><span class="sort-title">Định Danh</span></a></span></th>
                                                    </tr>
                                                    <tr class="filter">
                                                        <th class="a-center"><span class="head-massaction"><select name="in_category" id="catalog_category_products_filter_in_category" class="no-changes"><option value="">Any</option><option value="1">Yes</option><option value="0">No</option></select></span></th>
                                                        <th><div class="field-100"><input type="text" name="entity_id" id="catalog_category_products_filter_entity_id" value="" class="input-text no-changes"></div></th>
                                                        <th><div class="field-100"><input type="text" name="name" id="catalog_category_products_filter_name" value="" class="input-text no-changes"></div></th>
                                                        <th><div class="field-100"><input type="text" name="sku" id="catalog_category_products_filter_sku" value="" class="input-text no-changes"></div></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr title="#" class="even pointer">
                                                            <td class="a-center "><input type="checkbox" name="" value="888" class="checkbox"></td>
                                                            <td class=" ">888</td>
                                                            <td class=" ">My Configurable Product-Black-10</td>
                                                            <td class=" ">&nbsp;</td>
                                                        </tr>
                                                        <tr title="#" class="pointer">
                                                            <td class="a-center "><input type="checkbox" name="" value="887" class="checkbox"></td>
                                                            <td class=" ">887</td>
                                                            <td class=" ">My Bundle Product</td>
                                                            <td class=" ">ABC 234</td>
                                                        </tr>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                        </form>
                        <script type="text/javascript">
                            category_info_tabsJsTabs = new varienTabs('category_info_tabs', 'category_tab_content', 'category_info_tabs_group_4','category_info_tabs_products',[]);
                        </script>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php include_once ("footer.php")?>
</div>
<div id="loading-mask" style="display:none">
    <p class="loader" id="loading_mask_loader"><img src="skin/adminhtml/default/default/images/ajax-loader-tr.gif" alt="Loading..."/><br/>Please wait...</p>
</div>


</body>
</html>
