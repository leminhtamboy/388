<div class="header">
    <div class="header-top">
        <a href="dashboard.php"><img src="../image/logoDagCap.png" alt="Super AI Logo" class="logo"/></a>
        <div class="header-right">
            <p class="super">
                Logged in as admin<span class="separator">|</span>Thursday, June 30, 2016<span class="separator">|</span><a href="http://www.ma_1_9_2_data.com/index.php/admin/index/logout/key/193ea7f9057e54a40fc14e303f1dd602/" class="link-logout">Log Out</a>
            </p>
        </div>
    </div>
    <div class="clear"></div>

    <div class="nav-bar">
        <!-- menu start -->
        <ul id="nav">
            <li  class=" active  level0"> <a href="dashboard.php"   class="active"><span>Dashboard</span></a>
            </li>
            <li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level0"> <a href="#"  onclick="return false" class=""><span>Danh Mục</span></a>
                <ul >
                    <li  class="  level1"> <a href="managerArticle.php"   class=""><span>Quản Lý Bài Viết</span></a>
                    </li>
                    <li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level1"> <a href="#"  onclick="return false" class=""><span>Thuộc Tính Bài Viết</span></a>
                        <ul >
                            <li  class="  level2"> <a href="managerAttribute.php" class=""><span>Quản Lý Các Thuộc Tính</span></a>
                            </li>
                            <li  class="  last level2"> <a href="#"   class=""><span>Quản Lý Tập Thuộc Tính</span></a>
                            </li>
                        </ul>
                    </li>
                    <li  class="  level1"> <a href="managerCategory.php"   class=""><span>Quản Lý Danh Mục</span></a>
                    </li>
                    <li  class="  level1"> <a href="managerMenu.php"   class=""><span>Quản Lý Menu Trang Chủ</span></a>
                    </li>
                </ul>
            </li>
            <!--<li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level0"> <a href="#"  onclick="return false" class=""><span>Customers</span></a>
                <ul >
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/customer/index/key/d66bccf1c1e58d190c60b0c5ab5aa4c2/"   class=""><span>Manage Customers</span></a>
                    </li>
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/customer_group/index/key/58ce16da9718f7f02ac0739b29ae4bd9/"   class=""><span>Customer Groups</span></a>
                    </li>
                    <li  class="  last level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/customer_online/index/key/f03a1c81d7b156b4aa0f9dfc328ea3d2/"   class=""><span>Online Customers</span></a>
                    </li>
                </ul>
            </li>
            <li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level0"> <a href="#"  onclick="return false" class=""><span>Newsletter</span></a>
                <ul >
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/newsletter_template/index/key/923e516a1d00e3b93f025cc9dc353393/"   class=""><span>Newsletter Templates</span></a>
                    </li>
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/newsletter_queue/index/key/2818c4d07b35e9a84834146bdf3bf004/"   class=""><span>Newsletter Queue</span></a>
                    </li>
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/newsletter_subscriber/index/key/c8040294f956fa21303a0caf9fdef3d3/"   class=""><span>Newsletter Subscribers</span></a>
                    </li>
                    <li  class="  last level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/newsletter_problem/index/key/2d1c8d65991981d6aeb1b2b2bcb24b64/"   class=""><span>Newsletter Problem Reports</span></a>
                    </li>
                </ul>
            </li>-->
            <li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level0"> <a href="#"  onclick="return false" class=""><span>CMS</span></a>
                <ul >
                    <li  class="  level1"> <a href="managerPage.php"   class=""><span>Pages</span></a>
                    </li>
                    <!--<li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/cms_block/index/key/bb0f0bc1280c964f69fb976a77c503df/"   class=""><span>Static Blocks</span></a>
                    </li>
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/widget_instance/index/key/5cd2e35bdb5d159591c8f26097198d92/"   class=""><span>Widgets</span></a>
                    </li>
                    <li  class="  last level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/poll/index/key/00e219d2bc817873335342c7da9091da/"   class=""><span>Polls</span></a>
                    </li>-->
                </ul>
            </li>
            <li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level0"> <a href="#"  onclick="return false" class=""><span>System</span></a>
                <ul>
                    <!--<li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level1"> <a href="#"  onclick="return false" class=""><span>Magestore Extensions</span></a>
                        <ul >
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/system_config/edit/section/magenotificationcerts/key/b7ab3845099e37660f842b9a95343e38/"   class=""><span>License Certificates</span></a>
                            </li>
                            <li  class="  last level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/magenotification_feedback/index/key/2e5b39e9c2b7e256d7ecb39bafbca42b/"   class=""><span>Extension Feedbacks</span></a>
                            </li>
                        </ul>
                    </li>
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/system_account/index/key/8cba33dbd245cc31e06fa6e3ac28e748/"   class=""><span>My Account</span></a>
                    </li>
                    <li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/notification/index/key/cc95c7f8bb0de047610eee786f6488ac/"   class=""><span>Notifications</span></a>
                        <ul >
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/magenotification_magenotification/index/key/0569987094b9ea26b5558dcd2c2be7de/"   class=""><span>Magestore</span></a>
                            </li>
                            <li  class="  last level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/notification/index/key/cc95c7f8bb0de047610eee786f6488ac/"   class=""><span>System</span></a>
                            </li>
                        </ul>
                    </li>
                    <li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level1"> <a href="#"  onclick="return false" class=""><span>Tools</span></a>
                        <ul >
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/system_backup/index/key/c79615b124cd1903e4ed9a626ad75359/"   class=""><span>Backups</span></a>
                            </li>
                            <li  class="  last level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/compiler_process/index/key/697b40ccc8bfb294388b5202ca9f1981/"   class=""><span>Compilation</span></a>
                            </li>
                        </ul>
                    </li>
                    <li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level1"> <a href="#"  onclick="return false" class=""><span>Web Services</span></a>
                        <ul >
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/api_user/index/key/4770b4fedede77b62a25b8870e6b5c84/"   class=""><span>SOAP/XML-RPC - Users</span></a>
                            </li>
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/api_role/index/key/4e18ef347cf7b8c3c68d239a6fbeef11/"   class=""><span>SOAP/XML-RPC - Roles</span></a>
                            </li>
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/api2_role/index/key/55b2686933ba7754ead8886ac72b4fb4/"   class=""><span>REST - Roles</span></a>
                            </li>
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/api2_attribute/index/key/04b68e18c94b6917459230b6828b25e6/"   class=""><span>REST - Attributes</span></a>
                            </li>
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/oauth_consumer/index/key/29019f49bda6bf8c94a88f700b80b6a5/"   class=""><span>REST - OAuth Consumers</span></a>
                            </li>
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/oauth_authorizedTokens/index/key/2538b710550587867d583fabbe00d1eb/"   class=""><span>REST - OAuth Authorized Tokens</span></a>
                            </li>
                            <li  class="  last level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/oauth_admin_token/index/key/9ceb40e5cf4e5849c1f6c06613a89680/"   class=""><span>REST - My Apps</span></a>
                            </li>
                        </ul>
                    </li>
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/system_design/index/key/b937a7568b647d9ca39dec6f917e8818/"   class=""><span>Design</span></a>
                    </li>
                    <li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level1"> <a href="#"  onclick="return false" class=""><span>Import/Export</span></a>
                        <ul >
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/import/index/key/fb8cd6ff327f4e23668f942223905e1f/"   class=""><span>Import</span></a>
                            </li>
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/export/index/key/797d1aba9d81981371132551fe67bf25/"   class=""><span>Export</span></a>
                            </li>
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/system_convert_gui/index/key/810148e074d034b54417e86f48b37d4c/"   class=""><span>Dataflow - Profiles</span></a>
                            </li>
                            <li  class="  last level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/system_convert_profile/index/key/291fa798c5a9a12c631a358cf05d25bc/"   class=""><span>Dataflow - Advanced Profiles</span></a>
                            </li>
                        </ul>
                    </li>
                    <li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/system_currency/index/key/b0d2b4be5f117477bd1bee57a3c678b9/"   class=""><span>Manage Currency</span></a>
                        <ul >
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/system_currency/index/key/b0d2b4be5f117477bd1bee57a3c678b9/"   class=""><span>Rates</span></a>
                            </li>
                            <li  class="  last level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/system_currencysymbol/index/key/06bb968e43c4a8123f22c7eb3ca2200e/"   class=""><span>Symbols</span></a>
                            </li>
                        </ul>
                    </li>
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/system_email_template/index/key/7802e135eb8a3e73cfcb7553d52c6041/"   class=""><span>Transactional Emails</span></a>
                    </li>
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/system_variable/index/key/096b7f5a23b7a96b593566fa9a4372a7/"   class=""><span>Custom Variables</span></a>
                    </li>
                    <li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level1"> <a href="#"  onclick="return false" class=""><span>Permissions</span></a>
                        <ul >
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/permissions_user/index/key/2742d55b3029e2b7ef31941508661905/"   class=""><span>Users</span></a>
                            </li>
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/permissions_role/index/key/9c9d00eb469a51e73e5506dcd5f4c16d/"   class=""><span>Roles</span></a>
                            </li>
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/permissions_variable/index/key/56954b781e5b59d87d0e38b5e7875241/"   class=""><span>Variables</span></a>
                            </li>
                            <li  class="  last level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/permissions_block/index/key/6acaa34c9a319d6ff275e18c357a12f9/"   class=""><span>Blocks</span></a>
                            </li>
                        </ul>
                    </li>
                    <li onmouseover="Element.addClassName(this,'over')" onmouseout="Element.removeClassName(this,'over')" class="  parent level1"> <a href="#"  onclick="return false" class=""><span>Magento Connect</span></a>
                        <ul >
                            <li  class="  level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/extension_local/index/key/f26c27fc4f5599dc83312bf02ab92c0a/"   class=""><span>Magento Connect Manager</span></a>
                            </li>
                            <li  class="  last level2"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/extension_custom/index/key/e1bfad66d8dff610c34f07d422e6a312/"   class=""><span>Package Extensions</span></a>
                            </li>
                        </ul>
                    </li>
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/cache/index/key/a49697bf32a6a9e25a15253d7dd15d59/"   class=""><span>Cache Management</span></a>
                    </li>
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/process/list/key/766ecafb80a25eac8a53d89323c08322/"   class=""><span>Index Management</span></a>
                    </li>
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/system_store/index/key/8eee9607ad70aa5196834c50afb86b1f/"   class=""><span>Manage Stores</span></a>
                    </li>
                    <li  class="  level1"> <a href="http://www.ma_1_9_2_data.com/index.php/admin/sales_order_status/index/key/c3f85b7dfb1e96c1c417bf9744333e3a/"   class=""><span>Order Statuses</span></a>
                    </li>-->
                    <li  class="  last level1"> <a href="managerConfig.php"   class=""><span>Configuration</span></a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- menu end -->

        <a id="page-help-link" href="http://thietkesmartweb.com">Get help for this page</a>
        <script type="text/javascript">$('page-help-link').target = 'magento_page_help'</script>

    </div>
</div>