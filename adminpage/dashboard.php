
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Dashboard / Super AI</title>
    <link rel="icon" href="http://www.ma_1_9_2_data.com/skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="http://www.ma_1_9_2_data.com/skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>

    <?php include_once("head.php"); ?>
</head>

<body id="html-body" class=" adminhtml-dashboard-index">
<div class="wrapper">
    <noscript>
        <div class="noscript">
            <div class="noscript-inner">
                <p><strong>JavaScript seems to be disabled in your browser.</strong></p>
                <p>You must have JavaScript enabled in your browser to utilize the functionality of this website.</p>
            </div>
        </div>
    </noscript>
    <?php include_once("header.php"); ?>
    <div class="notification-global">

    <span class="f-right">
                You have <span class="critical"><strong>13</strong> critical</span>, <strong>6</strong> major, <strong>19</strong> minor and <strong>199</strong> notice unread message(s). <a href="http://www.ma_1_9_2_data.com/index.php/admin/notification/index/key/cc95c7f8bb0de047610eee786f6488ac/">Go to messages inbox</a>
    </span>
        <strong class="label">

            Latest Message:</strong> Increase your sales and productivity, while simplifying PCI compliance with exciting new Magento Community Edition 2.1 features.            <a href="https://magento.com/blog/magento-news/magento-enterprise-edition-21-unleashes-power-marketers-and-merchandisers " onclick="this.target='_blank';">Read details</a>
    </div>




    <div class="middle" id="anchor-content">
        <div id="page:main-container">
            <div id="messages"></div>

            <script type="text/javascript">
                //<![CDATA[
                function changeDiagramsPeriod(periodObj) {
                    periodParam = periodObj.value ? 'period/' + periodObj.value + '/' : '';
                    ajaxBlockParam = 'block/tab_orders/';
                    ajaxBlockUrl = 'http://www.ma_1_9_2_data.com/index.php/admin/dashboard/ajaxBlock/key/fa25448b00adda0e1d3160b0a69464a2/' + ajaxBlockParam + periodParam;
                    new Ajax.Request(ajaxBlockUrl, {
                        parameters: {isAjax: 'true', form_key: FORM_KEY},
                        onSuccess: function(transport) {
                            tabContentElementId = 'diagram_tab_orders_content';
                            try {
                                if (transport.responseText.isJSON()) {
                                    var response = transport.responseText.evalJSON()
                                    if (response.error) {
                                        alert(response.message);
                                    }
                                    if(response.ajaxExpired && response.ajaxRedirect) {
                                        setLocation(response.ajaxRedirect);
                                    }
                                } else {
                                    $(tabContentElementId).update(transport.responseText);
                                }
                            }
                            catch (e) {
                                $(tabContentElementId).update(transport.responseText);
                            }
                        }
                    });
                    ajaxBlockParam = 'block/tab_amounts/';
                    ajaxBlockUrl = 'http://www.ma_1_9_2_data.com/index.php/admin/dashboard/ajaxBlock/key/fa25448b00adda0e1d3160b0a69464a2/' + ajaxBlockParam + periodParam;
                    new Ajax.Request(ajaxBlockUrl, {
                        parameters: {isAjax: 'true', form_key: FORM_KEY},
                        onSuccess: function(transport) {
                            tabContentElementId = 'diagram_tab_amounts_content';
                            try {
                                if (transport.responseText.isJSON()) {
                                    var response = transport.responseText.evalJSON()
                                    if (response.error) {
                                        alert(response.message);
                                    }
                                    if(response.ajaxExpired && response.ajaxRedirect) {
                                        setLocation(response.ajaxRedirect);
                                    }
                                } else {
                                    $(tabContentElementId).update(transport.responseText);
                                }
                            }
                            catch (e) {
                                $(tabContentElementId).update(transport.responseText);
                            }
                        }
                    });
                    ajaxBlockUrl = 'http://www.ma_1_9_2_data.com/index.php/admin/dashboard/ajaxBlock/block/totals/key/fa25448b00adda0e1d3160b0a69464a2/' + periodParam;
                    new Ajax.Request(ajaxBlockUrl, {
                        parameters: {isAjax: 'true', form_key: FORM_KEY},
                        onSuccess: function(transport) {
                            tabContentElementId = 'dashboard_diagram_totals';
                            try {
                                if (transport.responseText.isJSON()) {
                                    var response = transport.responseText.evalJSON()
                                    if (response.error) {
                                        alert(response.message);
                                    }
                                    if(response.ajaxExpired && response.ajaxRedirect) {
                                        setLocation(response.ajaxRedirect);
                                    }
                                } else {
                                    $(tabContentElementId).replace(transport.responseText);
                                }
                            }
                            catch (e) {
                                $(tabContentElementId).replace(transport.responseText);
                            }
                        }
                    });
                }

                function toggleCal(id) {
                    $('dashboard_'+id+'_cal_div').toggle();
                    $('dashboard_'+id+'_range_div').toggle();
                }
                //]]>
            </script>
            <div class="content-header">
                <table cellspacing="0">
                    <tr>
                        <td><h3 class="head-dashboard">Dashboard</h3></td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-container">
                <p class="switcher"><label for="store_switcher">Choose Store View:</label>
                    <select name="store_switcher" id="store_switcher" onchange="return switchStore(this);">
                        <option value="">All Store Views</option>
                        <optgroup label="Main Website"></optgroup>
                        <optgroup label="&nbsp;&nbsp;&nbsp;Madison Island">
                            <option value="1">&nbsp;&nbsp;&nbsp;&nbsp;English</option>
                            <option value="2">&nbsp;&nbsp;&nbsp;&nbsp;French</option>
                            <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;German</option>
                        </optgroup>
                    </select>
                    <a href="http://www.magentocommerce.com/knowledge-base/entry/understanding-store-scopes" onclick="this.target='_blank'" title="What is this?" class="link-store-scope">What is this?</a></p>
                <script type="text/javascript">
                    function switchStore(obj) {
                        var storeParam = obj.value ? 'store/' + obj.value + '/' : '';
                        if (obj.switchParams) {
                            storeParam += obj.switchParams;
                        }
                        setLocation('http://www.ma_1_9_2_data.com/index.php/admin/dashboard/index/key/e4c98bba9b3a7bb2fe34a1441997e56e/' + storeParam);
                    }
                </script>
                <table cellspacing="25" width="100%">
                    <tr>
                        <td>                        <div class="entry-edit">
                                <div class="entry-edit-head"><h4>Lifetime Sales</h4></div>
                                <fieldset class="a-center bold">
                                    <span class="nowrap" style="font-size:18px;"><span class="price">$39,763.08</span><span style="font-size:14px; color:#686868;"></span></span>
                                </fieldset>
                            </div>
                            <div class="entry-edit">
                                <div class="entry-edit-head"><h4>Average Orders</h4></div>
                                <fieldset class="a-center bold">
                                    <span class="nowrap" style="font-size:18px;"><span class="price">$1,988.15</span><span style="font-size:14px; color:#686868;"></span></span>
                                </fieldset>
                            </div>
                            <div class="entry-edit">
                                <div class="entry-edit-head"><h4>Last 5 Orders</h4></div>
                                <fieldset class="np"><div class="grid np">
                                        <table cellspacing="0" style="border:0;" id="lastOrdersGrid_table">
                                            <col  />
                                            <col  width="100" />
                                            <col  width="100" />
                                            <thead>
                                            <tr class="headings">
                                                <th  class=" no-link"><span class="nobr">Customer</span></th>
                                                <th  class=" no-link"><span class="nobr">Items</span></th>
                                                <th  class=" no-link last"><span class="nobr">Grand Total</span></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/sales_order/view/order_id/209/key/d06d5e3554ee89588215b7a31dc0cd62/">
                                                <td class=" ">Jane Doe</td>
                                                <td class="a-right a-right ">2</td>
                                                <td class="a-right a-right last">&nbsp;</td>
                                            </tr>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/sales_order/view/order_id/208/key/d06d5e3554ee89588215b7a31dc0cd62/">
                                                <td class=" ">Jane Doe</td>
                                                <td class="a-right a-right ">2</td>
                                                <td class="a-right a-right last">&nbsp;</td>
                                            </tr>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/sales_order/view/order_id/207/key/d06d5e3554ee89588215b7a31dc0cd62/">
                                                <td class=" ">Jane Doe</td>
                                                <td class="a-right a-right ">2</td>
                                                <td class="a-right a-right last">&nbsp;</td>
                                            </tr>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/sales_order/view/order_id/206/key/d06d5e3554ee89588215b7a31dc0cd62/">
                                                <td class=" ">Jane Doe</td>
                                                <td class="a-right a-right ">2</td>
                                                <td class="a-right a-right last">&nbsp;</td>
                                            </tr>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/sales_order/view/order_id/205/key/d06d5e3554ee89588215b7a31dc0cd62/">
                                                <td class=" ">Jane Doe</td>
                                                <td class="a-right a-right ">2</td>
                                                <td class="a-right a-right last">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        lastOrdersGridJsObject = new varienGrid('lastOrdersGrid', 'http://www.ma_1_9_2_data.com/index.php/admin/dashboard/index/key/e4c98bba9b3a7bb2fe34a1441997e56e/', 'page', 'sort', 'dir', 'filter');
                                        lastOrdersGridJsObject.useAjax = '';
                                        lastOrdersGridJsObject.rowClickCallback = openGridRow;
                                        //]]>
                                    </script>
                                </fieldset>
                            </div>
                            <div class="entry-edit">
                                <div class="entry-edit-head"><h4>Last 5 Search Terms</h4></div>
                                <fieldset class="np"><div class="grid np">
                                        <table cellspacing="0" style="border:0;" id="lastSearchGrid_table">
                                            <col  />
                                            <col  width="100" />
                                            <col  width="100" />
                                            <thead>
                                            <tr class="headings">
                                                <th  class=" no-link"><span class="nobr">Search Term</span></th>
                                                <th  class=" no-link"><span class="nobr">Results</span></th>
                                                <th  class=" no-link last"><span class="nobr">Number of Uses</span></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_search/edit/id/263/key/a5bccd7ce1f766739fb9466aa65b63fa/">
                                                <td class=" "><span title="Madison Island VIP Membership - 1 Year">Madison Island VIP Membersh...</span></td>
                                                <td class=" a-right ">85</td>
                                                <td class=" a-right last">4</td>
                                            </tr>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_search/edit/id/264/key/a5bccd7ce1f766739fb9466aa65b63fa/">
                                                <td class=" ">Camera Travel Set</td>
                                                <td class=" a-right ">22</td>
                                                <td class=" a-right last">1</td>
                                            </tr>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_search/edit/id/265/key/a5bccd7ce1f766739fb9466aa65b63fa/">
                                                <td class=" ">women</td>
                                                <td class=" a-right ">1</td>
                                                <td class=" a-right last">1</td>
                                            </tr>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_search/edit/id/266/key/a5bccd7ce1f766739fb9466aa65b63fa/">
                                                <td class=" ">men</td>
                                                <td class=" a-right ">16</td>
                                                <td class=" a-right last">1</td>
                                            </tr>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_search/edit/id/61/key/a5bccd7ce1f766739fb9466aa65b63fa/">
                                                <td class=" ">shirt</td>
                                                <td class=" a-right ">4</td>
                                                <td class=" a-right last">6</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        lastSearchGridJsObject = new varienGrid('lastSearchGrid', 'http://www.ma_1_9_2_data.com/index.php/admin/dashboard/index/key/e4c98bba9b3a7bb2fe34a1441997e56e/', 'page', 'sort', 'dir', 'filter');
                                        lastSearchGridJsObject.useAjax = '';
                                        lastSearchGridJsObject.rowClickCallback = openGridRow;
                                        //]]>
                                    </script>
                                </fieldset>
                            </div>
                            <div class="entry-edit">
                                <div class="entry-edit-head"><h4>Top 5 Search Terms</h4></div>
                                <fieldset class="np"><div class="grid np">
                                        <table cellspacing="0" style="border:0;" id="topSearchGrid_table">
                                            <col  />
                                            <col  width="100" />
                                            <col  width="100" />
                                            <thead>
                                            <tr class="headings">
                                                <th  class=" no-link"><span class="nobr">Search Term</span></th>
                                                <th  class=" no-link"><span class="nobr">Results</span></th>
                                                <th  class=" no-link last"><span class="nobr">Number of Uses</span></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_search/edit/key/a5bccd7ce1f766739fb9466aa65b63fa/">
                                                <td class=" ">nolita</td>
                                                <td class=" a-right ">1</td>
                                                <td class=" a-right last">8</td>
                                            </tr>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_search/edit/key/a5bccd7ce1f766739fb9466aa65b63fa/">
                                                <td class=" ">24&quot; Pearl Strand Necklace</td>
                                                <td class=" a-right ">7</td>
                                                <td class=" a-right last">6</td>
                                            </tr>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_search/edit/key/a5bccd7ce1f766739fb9466aa65b63fa/">
                                                <td class=" ">shirt</td>
                                                <td class=" a-right ">4</td>
                                                <td class=" a-right last">6</td>
                                            </tr>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_search/edit/key/a5bccd7ce1f766739fb9466aa65b63fa/">
                                                <td class=" ">Ellis Flat</td>
                                                <td class=" a-right ">1</td>
                                                <td class=" a-right last">4</td>
                                            </tr>
                                            <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_search/edit/key/a5bccd7ce1f766739fb9466aa65b63fa/">
                                                <td class=" "><span title="Madison Island VIP Membership - 1 Year">Madison Island VIP Membersh...</span></td>
                                                <td class=" a-right ">85</td>
                                                <td class=" a-right last">4</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        topSearchGridJsObject = new varienGrid('topSearchGrid', 'http://www.ma_1_9_2_data.com/index.php/admin/dashboard/index/key/e4c98bba9b3a7bb2fe34a1441997e56e/', 'page', 'sort', 'dir', 'filter');
                                        topSearchGridJsObject.useAjax = '';
                                        topSearchGridJsObject.rowClickCallback = openGridRow;
                                        //]]>
                                    </script>
                                </fieldset>
                            </div>
                        </td>
                        <td>
                            <div class="entry-edit" style="border:1px solid #ccc;">
                                <!--  -->
                                <ul id="diagram_tab" class="tabs-horiz">
                                    <li>
                                        <a href="#" id="diagram_tab_orders" title="Orders" class="tab-item-link ">
                                            <span><span class="changed" title="The information in this tab has been changed."></span><span class="error" title="This tab contains invalid data. Please solve the problem before saving."></span>Orders</span>
                                        </a>
                                        <div id="diagram_tab_orders_content" style="display:none"><div style="margin:20px;">
                                                <p class="switcher a-right" style="padding:5px 10px;">Select Range:
                                                    <select name="period" id="order_orders_period" onchange="changeDiagramsPeriod(this);">
                                                        <option value="24h" >Last 24 Hours</option>
                                                        <option value="7d" >Last 7 Days</option>
                                                        <option value="1m" >Current Month</option>
                                                        <option value="1y" >YTD</option>
                                                        <option value="2y" >2YTD</option>
                                                    </select></p><br/>
                                                <p class="a-center" style="width:587px;height:300px; margin:0 auto;">No Data Found</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="#" id="diagram_tab_amounts" title="Amounts" class="tab-item-link ">
                                            <span><span class="changed" title="The information in this tab has been changed."></span><span class="error" title="This tab contains invalid data. Please solve the problem before saving."></span>Amounts</span>
                                        </a>
                                        <div id="diagram_tab_amounts_content" style="display:none"><div style="margin:20px;">
                                                <p class="switcher a-right" style="padding:5px 10px;">Select Range:
                                                    <select name="period" id="order_amounts_period" onchange="changeDiagramsPeriod(this);">
                                                        <option value="24h" >Last 24 Hours</option>
                                                        <option value="7d" >Last 7 Days</option>
                                                        <option value="1m" >Current Month</option>
                                                        <option value="1y" >YTD</option>
                                                        <option value="2y" >2YTD</option>
                                                    </select></p><br/>
                                                <p class="a-center" style="width:587px;height:300px; margin:0 auto;">No Data Found</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <script type="text/javascript">

                                    diagram_tabJsTabs = new varienTabs('diagram_tab', 'diagram_tab_content', 'diagram_tab_orders', []);
                                </script>
                                <div id="diagram_tab_content"></div>
                                <div style="margin:20px;">
                                    <div class="box" id="dashboard_diagram_totals">
                                        <div class="entry-edit">
                                            <table cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="a-center bold">
                                                        <span>Revenue</span><br />
                                                        <span class="nowrap" style="font-size:18px; color:#EA7601;"><span class="price">$0.00</span><span style="font-size:14px; color:#DE8946;"></span></span>
                                                    </td>
                                                    <td class="a-center bold">
                                                        <span>Tax</span><br />
                                                        <span class="nowrap" style="font-size:18px; color:#EA7601;"><span class="price">$0.00</span><span style="font-size:14px; color:#DE8946;"></span></span>
                                                    </td>
                                                    <td class="a-center bold">
                                                        <span>Shipping</span><br />
                                                        <span class="nowrap" style="font-size:18px; color:#EA7601;"><span class="price">$0.00</span><span style="font-size:14px; color:#DE8946;"></span></span>
                                                    </td>
                                                    <td class="a-center bold">
                                                        <span>Quantity</span><br />
                                                        <span class="nowrap" style="font-size:18px; color:#EA7601;">0<span style="font-size:14px; color:#DE8946;"></span></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin:20px;">
                                    <!--  -->
                                    <ul id="grid_tab" class="tabs-horiz">
                                        <li>
                                            <a href="#" id="grid_tab_ordered_products" title="Bestsellers" class="tab-item-link ">
                                                <span><span class="changed" title="The information in this tab has been changed."></span><span class="error" title="This tab contains invalid data. Please solve the problem before saving."></span>Bestsellers</span>
                                            </a>
                                            <div id="grid_tab_ordered_products_content" style="display:none"><div class="grid np">
                                                    <table cellspacing="0" style="border:0;" id="productsOrderedGrid_table">
                                                        <col  />
                                                        <col  width="120" />
                                                        <col  width="120" />
                                                        <thead>
                                                        <tr class="headings">
                                                            <th  class=" no-link"><span class="nobr">Product Name</span></th>
                                                            <th  class=" no-link"><span class="nobr">Price</span></th>
                                                            <th  class=" no-link last"><span class="nobr">Quantity Ordered</span></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_product/edit/id/311/key/f521edcfeb206239aa960b03f247eb47/">
                                                            <td class=" ">Convertible Dress</td>
                                                            <td class=" a-right ">$340.00</td>
                                                            <td class="a-right a-right last">35</td>
                                                        </tr>
                                                        <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_product/edit/id/285/key/f521edcfeb206239aa960b03f247eb47/">
                                                            <td class=" ">Tori Tank</td>
                                                            <td class=" a-right ">$60.00</td>
                                                            <td class="a-right a-right last">31</td>
                                                        </tr>
                                                        <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_product/edit/id/240/key/f521edcfeb206239aa960b03f247eb47/">
                                                            <td class=" ">Sullivan Sport Coat</td>
                                                            <td class=" a-right ">$510.00</td>
                                                            <td class="a-right a-right last">25</td>
                                                        </tr>
                                                        <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_product/edit/id/371/key/f521edcfeb206239aa960b03f247eb47/">
                                                            <td class=" ">Florentine Satchel Handbag</td>
                                                            <td class=" a-right ">$625.00</td>
                                                            <td class="a-right a-right last">20</td>
                                                        </tr>
                                                        <tr title="http://www.ma_1_9_2_data.com/index.php/admin/catalog_product/edit/id/400/key/f521edcfeb206239aa960b03f247eb47/">
                                                            <td class=" ">Compact mp3 Player</td>
                                                            <td class=" a-right ">$40.00</td>
                                                            <td class="a-right a-right last">20</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <script type="text/javascript">
                                                    //<![CDATA[
                                                    productsOrderedGridJsObject = new varienGrid('productsOrderedGrid', 'http://www.ma_1_9_2_data.com/index.php/admin/dashboard/index/key/e4c98bba9b3a7bb2fe34a1441997e56e/', 'page', 'sort', 'dir', 'filter');
                                                    productsOrderedGridJsObject.useAjax = '';
                                                    productsOrderedGridJsObject.rowClickCallback = openGridRow;
                                                    //]]>
                                                </script>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="http://www.ma_1_9_2_data.com/index.php/admin/dashboard/productsViewed/key/e0dafc90df2a0b628be54455619c46c1/" id="grid_tab_reviewed_products" title="Most Viewed Products" class="tab-item-link ajax notloaded">
                                                <span><span class="changed" title="The information in this tab has been changed."></span><span class="error" title="This tab contains invalid data. Please solve the problem before saving."></span>Most Viewed Products</span>
                                            </a>
                                            <div id="grid_tab_reviewed_products_content" style="display:none"></div>
                                        </li>
                                        <li>
                                            <a href="http://www.ma_1_9_2_data.com/index.php/admin/dashboard/customersNewest/key/d8ddd172da8313b442b9793955cdd9ec/" id="grid_tab_new_customers" title="New Customers" class="tab-item-link ajax notloaded">
                                                <span><span class="changed" title="The information in this tab has been changed."></span><span class="error" title="This tab contains invalid data. Please solve the problem before saving."></span>New Customers</span>
                                            </a>
                                            <div id="grid_tab_new_customers_content" style="display:none"></div>
                                        </li>
                                        <li>
                                            <a href="http://www.ma_1_9_2_data.com/index.php/admin/dashboard/customersMost/key/06a91b48c5bdf5f26de548ee444319af/" id="grid_tab_customers" title="Customers" class="tab-item-link ajax notloaded">
                                                <span><span class="changed" title="The information in this tab has been changed."></span><span class="error" title="This tab contains invalid data. Please solve the problem before saving."></span>Customers</span>
                                            </a>
                                            <div id="grid_tab_customers_content" style="display:none"></div>
                                        </li>
                                    </ul>
                                    <script type="text/javascript">

                                        grid_tabJsTabs = new varienTabs('grid_tab', 'grid_tab_content', 'grid_tab_ordered_products', []);
                                    </script>
                                    <div id="grid_tab_content"></div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="footer">
        <p class="bug-report">
            <a href="http://www.magentocommerce.com/bug-tracking" id="footer_bug_tracking">Help Us Keep Magento Healthy - Report All Bugs</a><br/>
            Interface Locale: <select name="locale" id="interface_locale" class="" title="Interface Language" style="width:200px"><option value="af_ZA" >Afrikaans (Suid-Afrika) / Afrikaans (South Africa)</option><option value="az_AZ" >Azərbaycan (Azərbaycan) / Azerbaijani (Azerbaijan)</option><option value="id_ID" >Bahasa Indonesia (Indonesia) / Indonesian (Indonesia)</option><option value="ms_MY" >Bahasa Melayu (Malaysia) / Malay (Malaysia)</option><option value="bs_BA" >Bosanski (Bosna i Hercegovina) / Bosnian (Bosnia and Herzegovina)</option><option value="ca_ES" >Català (Espanya) / Catalan (Spain)</option><option value="cy_GB" >Cymraeg (Y Deyrnas Unedig) / Welsh (United Kingdom)</option><option value="da_DK" >Dansk (Danmark) / Danish (Denmark)</option><option value="de_DE" >Deutsch (Deutschland) / German (Germany)</option><option value="de_CH" >Deutsch (Schweiz) / German (Switzerland)</option><option value="de_AT" >Deutsch (Österreich) / German (Austria)</option><option value="et_EE" >Eesti (Eesti) / Estonian (Estonia)</option><option value="en_AU" >English (Australia) / English (Australia)</option><option value="en_CA" >English (Canada) / English (Canada)</option><option value="en_IE" >English (Ireland) / English (Ireland)</option><option value="en_NZ" >English (New Zealand) / English (New Zealand)</option><option value="en_GB" >English (United Kingdom) / English (United Kingdom)</option><option value="en_US" selected="selected" >English (United States) / English (United States)</option><option value="es_AR" >Español (Argentina) / Spanish (Argentina)</option><option value="es_CL" >Español (Chile) / Spanish (Chile)</option><option value="es_CO" >Español (Colombia) / Spanish (Colombia)</option><option value="es_CR" >Español (Costa Rica) / Spanish (Costa Rica)</option><option value="es_ES" >Español (España) / Spanish (Spain)</option><option value="es_MX" >Español (México) / Spanish (Mexico)</option><option value="es_PA" >Español (Panamá) / Spanish (Panama)</option><option value="es_PE" >Español (Perú) / Spanish (Peru)</option><option value="es_VE" >Español (Venezuela) / Spanish (Venezuela)</option><option value="fil_PH" >Filipino (Pilipinas) / Filipino (Philippines)</option><option value="fr_CA" >Français (Canada) / French (Canada)</option><option value="fr_FR" >Français (France) / French (France)</option><option value="gl_ES" >Galego (España) / Galician (Spain)</option><option value="hr_HR" >Hrvatski (Hrvatska) / Croatian (Croatia)</option><option value="it_IT" >Italiano (Italia) / Italian (Italy)</option><option value="it_CH" >Italiano (Svizzera) / Italian (Switzerland)</option><option value="sw_KE" >Kiswahili (Kenya) / Swahili (Kenya)</option><option value="lv_LV" >Latviešu (Latvija) / Latvian (Latvia)</option><option value="lt_LT" >Lietuvių (Lietuva) / Lithuanian (Lithuania)</option><option value="hu_HU" >Magyar (Magyarország) / Hungarian (Hungary)</option><option value="nl_NL" >Nederlands (Nederland) / Dutch (Netherlands)</option><option value="nb_NO" >Norsk Bokmål (Norge) / Norwegian Bokmål (Norway)</option><option value="nn_NO" >Nynorsk (Noreg) / Norwegian Nynorsk (Norway)</option><option value="pl_PL" >Polski (Polska) / Polish (Poland)</option><option value="pt_BR" >Português (Brasil) / Portuguese (Brazil)</option><option value="pt_PT" >Português (Portugal) / Portuguese (Portugal)</option><option value="ro_RO" >Română (România) / Romanian (Romania)</option><option value="sq_AL" >Shqip (Shqipëri) / Albanian (Albania)</option><option value="sk_SK" >Slovenčina (Slovensko) / Slovak (Slovakia)</option><option value="sl_SI" >Slovenščina (Slovenija) / Slovenian (Slovenia)</option><option value="sr_RS" >Srpski (Srbija) / Serbian (Serbia)</option><option value="fi_FI" >Suomi (Suomi) / Finnish (Finland)</option><option value="sv_SE" >Svenska (Sverige) / Swedish (Sweden)</option><option value="vi_VN" >Tiếng Việt (Việt Nam) / Vietnamese (Vietnam)</option><option value="tr_TR" >Türkçe (Türkiye) / Turkish (Turkey)</option><option value="hi_IN" ></option><option value="bn_BD" ></option><option value="gu_IN" ></option><option value="th_TH" ></option><option value="lo_LA" ></option><option value="ka_GE" ></option><option value="km_KH" ></option><option value="is_IS" >íslenska (Ísland) / Icelandic (Iceland)</option><option value="cs_CZ" >čeština (Česká republika) / Czech (Czech Republic)</option><option value="zh_CN" ></option><option value="zh_HK" ></option><option value="zh_TW" ></option><option value="ja_JP" ></option><option value="ko_KR" ></option><option value="el_GR" >Ελληνικά (Ελλάδα) / Greek (Greece)</option><option value="be_BY" >беларуская (Беларусь) / Belarusian (Belarus)</option><option value="bg_BG" >български (България) / Bulgarian (Bulgaria)</option><option value="mk_MK" >македонски (Македонија) / Macedonian (Macedonia)</option><option value="mn_MN" >монгол (Монгол) / Mongolian (Mongolia)</option><option value="ru_RU" >русский (Россия) / Russian (Russia)</option><option value="uk_UA" >українська (Україна) / Ukrainian (Ukraine)</option><option value="he_IL" >עברית (ישראל) / Hebrew (Israel)</option><option value="ar_DZ" >العربية (الجزائر) / Arabic (Algeria)</option><option value="ar_KW" >العربية (الكويت) / Arabic (Kuwait)</option><option value="ar_MA" >العربية (المغرب) / Arabic (Morocco)</option><option value="ar_SA" >العربية (المملكة العربية السعودية) / Arabic (Saudi Arabia)</option><option value="ar_EG" >العربية (مصر) / Arabic (Egypt)</option><option value="fa_IR" >فارسی (ایران) / Persian (Iran)</option></select></p>
        <p class="legality">
            <a href="http://magento.com/resources/technical" id="footer_connect">Connect with the Magento Community</a><br/>
            <img src="http://www.ma_1_9_2_data.com/skin/adminhtml/default/default/images/varien_logo.gif" class="v-middle" alt="" />&nbsp;&nbsp;
            Magento&trade; is a trademark of Magento Inc.<br/>Copyright &copy; 2016 Magento Inc.</p>
        Magento ver. 1.9.2.3<script type="text/javascript">
            $('footer_bug_tracking').target = 'Varien_External';
            $('footer_connect').target = 'Varien_External';
            function setInterfaceLanguage(evt){
                var elem = Event.element(evt);
                if(elem){
                    setLocation('http://www.ma_1_9_2_data.com/index.php/admin/index/changeLocale/key/98f426e58fc1313918138bf1783ddc1b/locale/'+elem.value+'/uenc/aHR0cDovL3d3dy5tYV8xXzlfMl9kYXRhLmNvbS9pbmRleC5waHAvYWRtaW4vZGFzaGJvYXJkL2luZGV4L2tleS9lNGM5OGJiYTliM2E3YmIyZmUzNGExNDQxOTk3ZTU2ZS8,');
                }
            }
            Event.observe('interface_locale', 'change', setInterfaceLanguage)
        </script>
    </div>
</div>
<div id="loading-mask" style="display:none">
    <p class="loader" id="loading_mask_loader"><img src="http://www.ma_1_9_2_data.com/skin/adminhtml/default/default/images/ajax-loader-tr.gif" alt="Loading..."/><br/>Please wait...</p>
</div>


</body>
</html>
