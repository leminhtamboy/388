<?php include_once("../Attribute.php") ?>
<?php
$attribute = new Attribute("entity_attribute","id");
$dataAttribute=$attribute->getCollection();
//$dataCurrentCategory=$category->load($categoryId);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Quản Lý Thuộc Tính</title>
    <link rel="icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <?php include_once("head.php"); ?>
</head>

<body id="html-body" class=" adminhtml-catalog-category-edit">
<div class="wrapper">
    <noscript>
        <div class="noscript">
            <div class="noscript-inner">
                <p><strong>JavaScript seems to be disabled in your browser.</strong></p>
                <p>You must have JavaScript enabled in your browser to utilize the functionality of this website.</p>
            </div>
        </div>
    </noscript>
    <?php include_once("header.php"); ?>
    <div class="notification-global">

    <span class="f-right">
                You have <span class="critical"><strong>13</strong> critical</span>, <strong>6</strong> major, <strong>19</strong> minor and <strong>199</strong> notice unread message(s). <a href="index.php/admin/notification/index/key/cc95c7f8bb0de047610eee786f6488ac/">Go to messages inbox</a>
    </span>
        <strong class="label">

            Latest Message:</strong> Increase your sales and productivity, while simplifying PCI compliance with exciting new Magento Community Edition 2.1 features.            <a href="https://magento.com/blog/magento-news/magento-enterprise-edition-21-unleashes-power-marketers-and-merchandisers " onclick="this.target='_blank';">Read details</a>
    </div>
    <div class="middle" id="anchor-content">
        <div id="page:main-container">
            <div id="messages"></div>
            <div class="content-header">
                <table cellspacing="0">
                    <tbody><tr>
                        <td style="width:50%;"><h3 class="icon-head head-catalog-product-attribute">Quản Lý Thuộc Tính</h3></td>
                        <td class="form-buttons"><button id="id_6517f49021c1b4cd5f41858dfdd0c011" title="Add New Attribute" type="button" class="scalable add" onclick="setLocation('http://www.ma_1_9_2_data.com/index.php/admin/catalog_product_attribute/new/key/33e0f0486db4fca4501f061a25c98e53/')" style=""><span><span><span>Thêm Mới Thuộc Tính</span></span></span></button></td>
                    </tr>
                    </tbody></table>
            </div>
            <div>
                <div id="attributeGrid">
                    <table cellspacing="0" class="actions">
                        <tbody><tr>
                            <td class="pager">
                                Page
                                <img src="http://www.ma_1_9_2_data.com/skin/adminhtml/default/default/images/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow">

                                <input type="text" name="page" value="1" class="input-text page" onkeypress="attributeGridJsObject.inputPage(event, '5')">

                                <a href="#" title="Next page" onclick="attributeGridJsObject.setPage('2');return false;"><img src="http://www.ma_1_9_2_data.com/skin/adminhtml/default/default/images/pager_arrow_right.gif" alt="Go to Next page" class="arrow"></a>

                                of 5 pages            <span class="separator">|</span>
                                View            <select name="limit" onchange="attributeGridJsObject.loadByElement(this)">
                                    <option value="20" selected="selected">20</option>
                                    <option value="30">30</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="200">200</option>
                                </select>
                                per page<span class="separator">|</span>
                                Total 82 records found            <span id="attributeGrid-total-count" class="no-display">82</span>
                            </td>
                            <td class="filter-actions a-right">
                                <button id="id_a376c7181b217258f953d227576d3f17" title="Reset Filter" type="button" class="scalable " onclick="attributeGridJsObject.resetFilter()" style=""><span><span><span>Reset Filter</span></span></span></button><button id="id_eebadb6df156c61af08f864416f3f15f" title="Search" type="button" class="scalable task" onclick="attributeGridJsObject.doFilter()" style=""><span><span><span>Search</span></span></span></button>        </td>
                        </tr>
                        </tbody></table>
                    <div class="grid">
                        <div class="hor-scroll">
                            <table cellspacing="0" class="data" id="attributeGrid_table">
                                <colgroup><col>
                                    <col>
                                    <col>
                                    <col>
                                    <col>
                                    <col>
                                    <col>
                                    <col>
                                    <col>
                                </colgroup><thead>
                                <tr class="headings">
                                    <th><span class="nobr"><a href="#" name="attribute_code" title="desc" class="sort-arrow-asc"><span class="sort-title">Mã Thuộc Tính</span></a></span></th>
                                    <th><span class="nobr"><a href="#" name="frontend_label" title="asc" class="not-sort"><span class="sort-title">Tên Thuộc Tính</span></a></span></th>
                                    <th><span class="nobr"><a href="#" name="is_visible" title="asc" class="not-sort"><span class="sort-title">Ẩn</span></a></span></th>
                                    <th><span class="nobr"><a href="#" name="is_user_defined" title="asc" class="not-sort"><span class="sort-title">Vị Trí</span></a></span></th>
                                </tr>
                                <tr class="filter">
                                    <th><div class="field-100"><input type="text" name="attribute_code" id="attributeGrid_filter_attribute_code" value="" class="input-text no-changes"></div></th>
                                    <th><div class="field-100"><input type="text" name="frontend_label" id="attributeGrid_filter_frontend_label" value="" class="input-text no-changes"></div></th>
                                    <th><select name="is_user_defined" id="attributeGrid_filter_is_user_defined" class="no-changes"><option value=""></option><option value="0">Yes</option><option value="1">No</option></select></th>
                                    <th><select name="is_visible" id="attributeGrid_filter_is_visible" class="no-changes"><option value=""></option><option value="1">Yes</option><option value="0">No</option></select></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($dataAttribute as $att){ ?>
                                        <?php
                                            $isVisble="No";
                                            if($att->getvisible()==1){
                                                $isVisble="Yes";
                                            }
                                        ?>
                                    <tr title="#edit" class="even pointer">
                                        <td class=" "><?php echo $att->getattribute_code(); ?></td>
                                        <td class=" "><?php echo $att->getattribute_name(); ?></td>
                                        <td class="a-center "><?php echo $isVisble ?></td>
                                        <td class="a-center "><?php echo $att->getposition(); ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    //<![CDATA[
                    attributeGridJsObject = new varienGrid('attributeGrid', 'http://www.ma_1_9_2_data.com/index.php/admin/catalog_product_attribute/index/key/12bb15aa2a59b522a6384305f46239ff/', 'page', 'sort', 'dir', 'filter');
                    attributeGridJsObject.useAjax = '';
                    attributeGridJsObject.rowClickCallback = openGridRow;
                    //]]>
                </script>
            </div>
        </div>

    </div>
</div>
<?php include_once ("footer.php")?>
</div>
<div id="loading-mask" style="display:none">
    <p class="loader" id="loading_mask_loader"><img src="skin/adminhtml/default/default/images/ajax-loader-tr.gif" alt="Loading..."/><br/>Please wait...</p>
</div>


</body>
</html>
