<?php include_once("../Message.php") ?>
<?php include_once("../Page.php") ?>
<?php include_once("../Config.php") ?>
<?php
$page = new Page("page","id");
$dataPage=$page->getCollection();
$hasMessage="";
$showTitle="Page Hiện Tại";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Quản Lý Page</title>
    <link rel="icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <?php include_once("head.php"); ?>
</head>

<body id="html-body" class=" adminhtml-catalog-category-edit">
<div class="wrapper">
    <?php include_once("header.php"); ?>
    <div class="notification-global">
    <span class="f-right">
        Adminpanel Cho Bếp Chia Sẻ
    </span>
        <strong class="label">Latest Message:</strong>Trài Nghiệm Phiên Bản MappingDatabase Mới tại website thiekesmartweb.com<a href="http://thiekesmartweb.com" onclick="this.target='_blank';">Read details</a>
    </div>
    <div class="middle" id="anchor-content">
        <div id="page:main-container">
            <div id="messages"></div>
            <div class="content-header">
                <table cellspacing="0">
                    <tbody><tr>
                        <td style="width:50%;"><h3 class="icon-head head-cms-page">Quản Lý Page</h3></td>
                        <td class="form-buttons"><button id="id_3c4de529dc02473fa7bcb2644213013e" title="Add New Page" type="button" class="scalable add" onclick="top.location='actionAddPage.php'" style=""><span><span><span>Add New Page</span></span></span></button></td>
                    </tr>
                    </tbody></table>
            </div>
            <div>

                <div id="cmsPageGrid">
                    <table cellspacing="0" class="actions">
                        <tbody><tr>
                            <td class="filter-actions a-right">
                                <button id="id_2297c6976bb3a7be8b9f3ff3fe963a75" title="Reset Filter" type="button" class="scalable " onclick="alert('Tính năng này đang phát triển. Chúng tôi sẽ cập nhật sau');" style=""><span><span><span>Reset Filter</span></span></span></button>
                                <button id="id_75990f3eae4e440bc75b028330e385e8" title="Search" type="button" class="scalable task" onclick="alert('Tính năng này đang phát triển. Chúng tôi sẽ cập nhật sau');" style=""><span><span><span>Search</span></span></span></button>
                            </td>
                        </tr>
                        </tbody></table>
                    <div class="grid">
                        <div class="hor-scroll">
                            <table cellspacing="0" class="data" id="cmsPageGrid_table">
                                <colgroup><col>
                                    <col>
                                    <col>
                                    <col>
                                    <col>
                                    <col>
                                    <col>
                                    <col width="10">
                                </colgroup><thead>
                                <tr class="headings">
                                    <th><span class="nobr"><a href="#" name="title" title="asc" class="not-sort"><span class="sort-title">Title</span></a></span></th>
                                    <th><span class="nobr"><a href="#" name="identifier" title="desc" class="sort-arrow-asc"><span class="sort-title">URL Key</span></a></span></th>
                                    <th><span class="nobr"><a href="#" name="is_active" title="asc" class="not-sort"><span class="sort-title">Status</span></a></span></th>
                                    <th class=" no-link last"><span class="nobr">Action</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($dataPage as $pa){ ?>
                                <?php
                                    $idPage=$pa->getid_page();
                                    $identifier=$pa->getidentifier();
                                    $titlePage=$pa->gettitle();
                                    $showStatus=0;
                                    $status=$pa->getstatus();
                                    if($status==0){
                                        $showStatus="Chưa Kích Hoạt";
                                    }else{
                                        $showStatus="Kích Hoạt";
                                    }
                                    $linkPreview=ConfigGlobal::$realPath."/".$identifier.".html";
                                ?>
                                <tr title="actionEditPage.php?id_page=<?php echo $idPage ?>" class="even pointer">
                                    <td class="a-left "><?php echo $titlePage ?></td>
                                    <td class="a-left "><?php echo $identifier ?></td>
                                    <td class=" "><?php echo $showStatus ?></td>
                                    <td class=" last"><a href="<?php echo $linkPreview ?>" target="_blank">Preview</a></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    //<![CDATA[
                    cmsPageGridJsObject = new varienGrid('cmsPageGrid', 'http://www.ma_1_9_2_data.com/index.php/admin/cms_page/index/key/7f326586189fea1b411bada0168301c4/', 'page', 'sort', 'dir', 'filter');
                    cmsPageGridJsObject.useAjax = '';
                    cmsPageGridJsObject.rowClickCallback = openGridRow;
                    //]]>
                </script>
            </div>
        </div>

    </div>
</div>
<?php include_once ("footer.php")?>
</div>
<div id="loading-mask" style="display:none">
    <p class="loader" id="loading_mask_loader"><img src="skin/adminhtml/default/default/images/ajax-loader-tr.gif" alt="Loading..."/><br/>Please wait...</p>
</div>


</body>
</html>
