<?php include("../Article.php") ?>
<?php include("../Attribute.php") ?>
<?php include("../AttributeValue.php") ?>
<?php
$article = new Article ("article","id_article");
$dataArticle=$article->getCollection();
$attribute= new Attribute("entity_attribute","attribute_id");
$dataAttributeOnGrid=$attribute->getCollectionAttributeBySql();
$valueAttribute= new AttributeValue("entity_attribute_value_varchar","id_eav_vachar");
//$dataValueAttribute
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Quản Lý Bài Viết</title>
    <link rel="icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <?php include_once("head.php"); ?>
</head>

<body id="html-body" class=" adminhtml-catalog-category-edit">
<div class="wrapper">
    <noscript>
        <div class="noscript">
            <div class="noscript-inner">
                <p><strong>JavaScript seems to be disabled in your browser.</strong></p>
                <p>You must have JavaScript enabled in your browser to utilize the functionality of this website.</p>
            </div>
        </div>
    </noscript>
    <?php include_once("header.php"); ?>
    <div class="notification-global">

    <span class="f-right">
                You have <span class="critical"><strong>13</strong> critical</span>, <strong>6</strong> major, <strong>19</strong> minor and <strong>199</strong> notice unread message(s). <a href="index.php/admin/notification/index/key/cc95c7f8bb0de047610eee786f6488ac/">Go to messages inbox</a>
    </span>
        <strong class="label">

            Latest Message:</strong> Increase your sales and productivity, while simplifying PCI compliance with exciting new Magento Community Edition 2.1 features.            <a href="https://magento.com/blog/magento-news/magento-enterprise-edition-21-unleashes-power-marketers-and-merchandisers " onclick="this.target='_blank';">Read details</a>
    </div>
    <div class="middle" id="anchor-content">
        <div id="page:main-container">
            <div id="messages"></div>
            <div class="content-header">
                <table cellspacing="0">
                    <tbody><tr>
                        <td style="width:50%;"><h3 class="icon-head head-products">Quản Lý Bài Viết</h3></td>
                        <td class="a-right">
                            <button id="id_0188ca1b5ca7607f68c9e063f8e77b6d" title="Add Product" type="button" class="scalable add" onclick="setLocation('actionAddAticle.php')" style=""><span><span><span>Thêm Bài Viết</span></span></span></button></td>
                    </tr>
                    </tbody></table>
            </div>
            <p class="switcher"><label for="store_switcher">Choose Store View:</label>
                <select name="store_switcher" id="store_switcher" onchange="return switchStore(this);">
                    <option value="">All Store Views</option>
                    <optgroup label="Main Website"></optgroup>
                    <optgroup label="&nbsp;&nbsp;&nbsp;Madison Island">
                        <option value="1">&nbsp;&nbsp;&nbsp;&nbsp;English</option>
                        <option value="2">&nbsp;&nbsp;&nbsp;&nbsp;French</option>
                        <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;German</option>
                    </optgroup>
                </select>
                <a href="http://www.magentocommerce.com/knowledge-base/entry/understanding-store-scopes" onclick="this.target='_blank'" title="What is this?" class="link-store-scope">What is this?</a>
            <div>

                <div id="productGrid">
                    <table cellspacing="0" class="actions">
                        <tbody><tr>
                            <td class="pager">
                                Page
                                <img src="http://www.ma_1_9_2_data.com/skin/adminhtml/default/default/images/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow">

                                <input type="text" name="page" value="1" class="input-text page" onkeypress="productGridJsObject.inputPage(event, '30')">

                                <a href="#" title="Next page" onclick="productGridJsObject.setPage('2');return false;"><img src="http://www.ma_1_9_2_data.com/skin/adminhtml/default/default/images/pager_arrow_right.gif" alt="Go to Next page" class="arrow"></a>

                                of 30 pages            <span class="separator">|</span>
                                View            <select name="limit" onchange="productGridJsObject.loadByElement(this)">
                                    <option value="20" selected="selected">20</option>
                                    <option value="30">30</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="200">200</option>
                                </select>
                                per page<span class="separator">|</span>
                                Total 582 records found            <span id="productGrid-total-count" class="no-display">582</span>
                                <span class="separator">|</span><a href="http://www.ma_1_9_2_data.com/index.php/rss/catalog/notifystock/" class="link-feed">Notify Low Stock RSS</a>
                            </td>
                            <td class="filter-actions a-right">
                                <button id="id_554d514c860c76a415bb929c3dd459ae" title="Reset Filter" type="button" class="scalable " onclick="productGridJsObject.resetFilter()" style=""><span><span><span>Reset Filter</span></span></span></button><button id="id_0ff13173a438c0aa92f5dad20ec2b74c" title="Search" type="button" class="scalable task" onclick="productGridJsObject.doFilter()" style=""><span><span><span>Search</span></span></span></button></td>
                        </tr>
                        </tbody></table>
                    <div id="productGrid_massaction">
                        <table cellspacing="0" cellpadding="0" class="massaction">
                            <tbody><tr>
                                <td>
                                    <a href="#" onclick="return productGrid_massactionJsObject.selectAll()">Select All</a>
                                    <span class="separator">|</span>
                                    <a href="#" onclick="return productGrid_massactionJsObject.unselectAll()">Unselect All</a>
                                    <span class="separator">|</span>
                                    <a href="#" onclick="return productGrid_massactionJsObject.selectVisible()">Select Visible</a>
                                    <span class="separator">|</span>
                                    <a href="#" onclick="return productGrid_massactionJsObject.unselectVisible()">Unselect Visible</a>
                                    <span class="separator">|</span>
                                    <strong id="productGrid_massaction-count">0</strong> items selected    </td>
                                <td>
                                    <div class="right">
                                        <div class="entry-edit">
                                            <form action="" id="productGrid_massaction-form" method="post">
                                                <div><input name="form_key" type="hidden" value="S7lUW36T8QEp8ars"></div>
                                                <fieldset>
                        <span class="field-row">
                            <label>Actions</label>
                            <select id="productGrid_massaction-select" class="required-entry select absolute-advice local-validation">
                                <option value=""></option>
                                                                    <option value="delete">Delete</option>
                                                                    <option value="status">Change status</option>
                                                                    <option value="attributes">Update Attributes</option>
                                                            </select>
                        </span>
                                                    <span class="outer-span" id="productGrid_massaction-form-hiddens"></span>
                                                    <span class="outer-span" id="productGrid_massaction-form-additional"></span>
                        <span class="field-row">
                            <button id="id_5608f7f48c5fff4f5f9de3d5c74b8d6d" title="Submit" type="button" class="scalable " onclick="productGrid_massactionJsObject.apply()" style=""><span><span><span>Submit</span></span></span></button>                        </span>
                                                </fieldset>
                                            </form>
                                        </div>

                                        <div class="no-display">
                                            <div id="productGrid_massaction-item-delete-block">
                                            </div>
                                            <div id="productGrid_massaction-item-status-block">
                                                <div class="entry-edit">
    <span class="field-row">
<label for="visibility">Status</label>
<select id="visibility" name="status" class="required-entry absolute-advice select">
<option value="" selected="selected"></option>
<option value="1">Enabled</option>
<option value="2">Disabled</option>
</select>
</span>
                                                </div>
                                            </div>
                                            <div id="productGrid_massaction-item-attributes-block">
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody></table>
                    </div>
                    <div class="grid">
                        <div class="hor-scroll">
                            <table cellspacing="0" class="data" id="productGrid_table">
                                <colgroup><col width="20" class="a-center">
                                    <col width="50">
                                    <col>
                                    <col width="60">
                                    <col width="100">
                                    <col width="80">
                                    <col width="100">
                                    <col width="100">
                                    <col width="70">
                                    <col width="70">
                                    <col width="100">
                                    <col width="50">
                                </colgroup><thead>
                                <tr class="headings">
                                    <th><span class="nobr">&nbsp;</span></th>
                                    <th><span class="nobr"><a href="#" name="entity_id" title="asc" class="sort-arrow-desc"><span class="sort-title">ID</span></a></span></th>
                                    <th><span class="nobr"><a href="#" name="title" title="asc" class="not-sort"><span class="sort-title">Title</span></a></span></th>
                                    <th><span class="nobr"><a href="#" name="category" title="asc" class="not-sort"><span class="sort-title">Category</span></a></span></th>
                                    <?php foreach($dataAttributeOnGrid as $att){ ?>
                                        <th><span class="nobr"><a href="#" name="<?php echo $att->getattribute_code(); ?>" title="asc" class="not-sort"><span class="sort-title"><?php echo $att->getattribute_name(); ?></span></a></span></th>
                                    <?php } ?>
                                </tr>
                                <tr class="filter">
                                    <th><span class="head-massaction"></span></th>
                                    <th><div class="range"><div class="range-line"><span class="label">From:</span> <input type="text" name="entity_id[from]" id="productGrid_product_filter_entity_id_from" value="" class="input-text no-changes"></div><div class="range-line"><span class="label">To : </span><input type="text" name="entity_id[to]" id="productGrid_product_filter_entity_id_to" value="" class="input-text no-changes"></div></div></th>
                                    <th><div class="field-100"><input type="text" name="title" id="title" value="" class="input-text no-changes"></div></th>
                                    <th><div class="field-100"><input type="text" name="category" id="category" value="" class="input-text no-changes"></div></th>
                                    <?php foreach($dataAttributeOnGrid as $att){ ?>
                                        <th><div class="field-100"><input type="text" name="<?php echo $att->getattribute_code(); ?>" id="<?php echo $att->getattribute_code(); ?>" value="" class="input-text no-changes"></div></th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($dataArticle as $art){ ?>
                                    <?php $idAticle=$art->getid_article(); ?>
                                    <tr title="actionEditArticle.php?id_article=<?php echo $idAticle ?>" class="even pointer">

                                        <td class="a-center "><input type="checkbox" name="product" value="888" class="massaction-checkbox"></td>
                                            <td class=" a-right "><?php  echo $idAticle; ?></td>
                                            <td class=" "><?php echo $art->gettitle(); ?></td>
                                            <td class=" "><?php echo $art->gethas_category(); ?></td>
                                        <?php foreach ($dataAttributeOnGrid as $attr){ ?>
                                            <?php
                                                $idAttribute=$attr->getattribute_id();
                                                $dataCurrentValue=$valueAttribute->getValueAttribuetOfEntityBySql($idAticle,$idAttribute);
                                                if($dataCurrentValue){
                                                    ?>
                                                        <td class=" "><?php echo $dataCurrentValue[0]->getvalue(); ?></td>
                                                    <?php
                                                }else{
                                                    ?>
                                                        <td class=" "></td>
                                                    <?php
                                                }
                                            ?>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    //<![CDATA[
                    productGridJsObject = new varienGrid('productGrid', 'http://www.ma_1_9_2_data.com/index.php/admin/catalog_product/grid/key/4ccf035d1dcaca9d497e328fb046e82f/', 'page', 'sort', 'dir', 'product_filter');
                    productGridJsObject.useAjax = '1';
                    productGridJsObject.rowClickCallback = openGridRow;
                    var productGrid_massactionJsObject = new varienGridMassaction('productGrid_massaction', productGridJsObject, '', 'internal_product', 'product');productGrid_massactionJsObject.setItems({"delete":{"label":"Delete","url":"http:\/\/www.ma_1_9_2_data.com\/index.php\/admin\/catalog_product\/massDelete\/key\/ac5acaead2e967298128effa585da72f\/","confirm":"Are you sure?","id":"delete"},"status":{"label":"Change status","url":"http:\/\/www.ma_1_9_2_data.com\/index.php\/admin\/catalog_product\/massStatus\/key\/59108bea29d2f4dd1249cf3c514c47b2\/","id":"status"},"attributes":{"label":"Update Attributes","url":"http:\/\/www.ma_1_9_2_data.com\/index.php\/admin\/catalog_product_action_attribute\/edit\/key\/230fd2fd5914cde1f4d0c0fdb86ba9f5\/","id":"attributes"}}); productGrid_massactionJsObject.setGridIds('231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,325,326,327,328,329,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,430,431,432,433,434,435,436,437,439,440,441,442,445,446,447,448,449,450,456,457,458,459,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,546,547,548,549,551,552,553,554,555,557,558,559,560,561,562,563,564,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634,635,636,637,638,639,640,641,642,643,644,645,646,647,648,649,650,651,652,653,654,655,656,657,658,659,660,661,662,663,664,665,666,667,668,669,670,671,672,673,674,675,676,677,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,700,701,702,703,704,705,706,707,708,709,710,711,712,713,714,715,716,717,718,719,720,721,722,723,724,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,740,741,742,743,744,745,746,747,748,749,750,751,752,753,754,755,756,757,758,759,760,761,762,763,764,765,766,767,768,769,770,771,772,773,774,775,776,777,778,779,780,781,782,783,784,785,786,787,788,789,790,791,792,793,794,795,796,797,798,799,800,801,802,803,804,805,806,807,808,809,810,811,812,813,814,815,816,817,818,819,820,821,822,823,824,825,826,827,828,829,830,831,832,833,834,835,836,837,838,863,864,865,866,867,868,869,870,871,872,873,874,875,877,878,879,880,881,882,883,884,885,886,887,888');productGrid_massactionJsObject.setUseSelectAll(true);productGrid_massactionJsObject.errorText = 'Please select items.';        //]]>
                </script>
            </div>
        </div>

    </div>
</div>
<?php include_once ("footer.php")?>
</div>
<div id="loading-mask" style="display:none">
    <p class="loader" id="loading_mask_loader"><img src="skin/adminhtml/default/default/images/ajax-loader-tr.gif" alt="Loading..."/><br/>Please wait...</p>
</div>


</body>
</html>
