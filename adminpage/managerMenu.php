<?php include_once("../Uploader.php") ?>
<?php include_once("../Message.php") ?>
<?php include_once("../Menu.php") ?>
<?php
$menu = new Menu("menu","id");
$dataMenu=$menu->getCollection();
$hasMessage="";
$showTitle="Menu Hiện Tại";
$menuId=$dataMenu[0]->getid();
if(isset($_REQUEST["menu_id"])){
    $menuId=$_REQUEST["menu_id"];
}
if(isset($_REQUEST["action"])){
    $action=$_REQUEST["action"];
    switch ($action){
        case "add" : {
            $showTitle="Thêm Mới Menu";
            $menuId=-1;
            break;
        }
        case "save":{
            if($_POST["id"]==-1) {
                $uploader=new UploadDer();
                $message = new Message();
                $uploader->_nameTagFile="image";
                $uploader->_mainFoler="imagesMenu";
                $urlImage=$uploader->uploadAction();
                $_POST["id"] = "NULL";
                $countPost = count($_POST);
                foreach ($_POST as $key => $value) {
                    $menu->setData($key, $value);
                }
                $menu->setData("image",$urlImage);
                $numRows=$menu->inserRow();
                if($numRows!=0){
                    $hasMessage=$message->addSuccess("Thêm Menu Thành Công");
                }
                $dataMenu=$menu->getCollection();
                $showTitle="Menu Hiện Tại";
            }else{
                $uploader=new UploadDer();
                $message = new Message();
                $uploader->_nameTagFile="image";
                $uploader->_mainFoler="imagesMenu";
                $menu->setData("id",$_POST["id"]);
                if($_FILES['image']['name']!=""){
                    $urlImage=$uploader->uploadAction();
                    $menu->setData("image",$urlImage);
                }
                $countPost = count($_POST);
                foreach ($_POST as $key => $value) {
                    $menu->setData($key, $value);
                }
                $menu->updateRow();
                $hasMessage=$message->addSuccess("Cập Nhật Menu Thành Công");
                $menuId=$_POST["id"];
                $dataMenu=$menu->getCollection();
            }
            break;
        }
        case "delete":{
            $menu->deleteRow($_POST["id"]);
            $message = new Message();
            $hasMessage=$message->addSuccess("Xóa Menu Thành Công");
            $dataMenu=$menu->getCollection();
            break;
        }
    }
}
$dataCurrentMenu=$menu->load($menuId);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>New Category / Manage Categories / Categories / Catalog / Magento Admin</title>
    <link rel="icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <?php include_once("head.php"); ?>
</head>

<body id="html-body" class=" adminhtml-catalog-category-edit">
<div class="wrapper">
    <noscript>
        <div class="noscript">
            <div class="noscript-inner">
                <p><strong>JavaScript seems to be disabled in your browser.</strong></p>
                <p>You must have JavaScript enabled in your browser to utilize the functionality of this website.</p>
            </div>
        </div>
    </noscript>
    <?php include_once("header.php"); ?>
    <div class="notification-global">
        
    <span class="f-right">
                You have <span class="critical"><strong>13</strong> critical</span>, <strong>6</strong> major, <strong>19</strong> minor and <strong>199</strong> notice unread message(s). <a href="index.php/admin/notification/index/key/cc95c7f8bb0de047610eee786f6488ac/">Go to messages inbox</a>
    </span>
        <strong class="label">

            Latest Message:</strong> Increase your sales and productivity, while simplifying PCI compliance with exciting new Magento Community Edition 2.1 features.            <a href="https://magento.com/blog/magento-news/magento-enterprise-edition-21-unleashes-power-marketers-and-merchandisers " onclick="this.target='_blank';">Read details</a>
    </div>
    <div class="middle" id="anchor-content">
        <div id="page:main-container">

            <div class="columns ">
                <div class="side-col" id="page:left">
                    <div class="categories-side-col">
                        <div class="content-header">
                            <h3 class="icon-head head-categories">Menu</h3>
                            <button  id="add_root_category_button" title="Add Root Category" type="button" class="scalable add" onclick="top.location='?action=add'" style=""><span><span><span>Thêm Menu</span></span></span></button><br />
                        </div>

                        <div class="tree-holder">
                            <div id="tree-div" style="width:100%; overflow:auto;" class=" x-tree">
                                <ul class="x-tree-root-ct x-tree-lines" id="ext-gen5">
                                    <div class="x-tree-root-node">
                                        <li class="x-tree-node">
                                            <div class="x-tree-node-el folder active-category x-tree-node-expanded" id="extdd-1">
                                                <span class="x-tree-node-indent"></span>
                                                <img src="js/spacer.gif" class="x-tree-ec-icon x-tree-elbow-end-minus" id="ext-gen20">
                                                <img src="js/spacer.gif" class="x-tree-node-icon" unselectable="on" id="ext-gen17">
                                                <a hidefocus="on" href="#" tabindex="1" id="ext-gen14"><span unselectable="on" id="extdd-2">Danh Sách Menu(<?php echo count($dataMenu); ?>)</span></a>
                                            </div>
                                            <ul class="x-tree-node-ct">
                                                <?php foreach($dataMenu as $mn){ ?>
                                                    <?php
                                                        $menuIds=$mn->getid();
                                                        $nameMenu=$mn->gettitle_menu();
                                                    ?>
                                                <li class="x-tree-node">
                                                    <div class="x-tree-node-el folder active-category x-tree-node-collapsed  x-tree-node-leaf x-tree-selected" id="extdd-3">
                                                        <span class="x-tree-node-indent">
                                                            <img src="js/spacer.gif" class="x-tree-icon"></span>
                                                        <img src="js/spacer.gif" class="x-tree-ec-icon x-tree-elbow" id="ext-gen27">
                                                        <img src="js/spacer.gif" class="x-tree-node-icon" unselectable="on" id="ext-gen24">
                                                        <a hidefocus="on" href="?menu_id=<?php echo $menuIds; ?>" tabindex="1" id="ext-gen21"><span unselectable="on" id="extdd-4"><?php echo $nameMenu; ?></span></a>
                                                    </div>
                                                    <ul class="x-tree-node-ct"></ul>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-col" id="content">
                    <div class="main-col-inner">
                        <?php echo $hasMessage; ?>
                        <form action="?action=save" method="post" name="edit_form" id="edit_form" enctype="multipart/form-data">
                        <div id="category-edit-container" class="category-content">

                            <div class="content-header">
                                <h3 class="icon-head head-categories"><?php echo $showTitle; ?></h3>
                                <p class="content-buttons form-buttons">
                                    <?php if($menuId!=-1) { ?>
                                        <button id="id_41d388dbc923c5cbcf41dbf778b20693" title="Delete Menu" type="button" class="scalable delete" onclick="var con=confirm('Bạn Thực Sự Muốn Xóa');if(con==true){jQuery('#edit_form').attr('action','?action=delete');jQuery('#edit_form').submit();}" style=""><span><span><span>Xóa Menu</span></span></span></button>
                                    <?php } ?>
                                    <button  id="id_362ad19a3fa1872766901734acc7619b" title="Save Category" type="button" class="scalable save" onclick="jQuery('#edit_form').submit();" style=""><span><span><span>Lưu Menu</span></span></span></button>        </p>
                            </div>
                            <ul id="category_info_tabs" class="tabs-horiz">
                                <li style="width:98%">
                                    <a href="#" id="category_info_tabs_group_4" title="General Information" class="tab-item-link ">
                                        <span><span class="changed" title="The information in this tab has been changed."></span><span class="error" title="This tab contains invalid data. Please solve the problem before saving."></span>General Information</span>
                                    </a>
                                    <div id="category_info_tabs_group_4_content" style="display:none"><div class="entry-edit">
                                            <div class="entry-edit-head">
                                                <h4 class="icon-head head-edit-form fieldset-legend">Thông Tin Chi Tiết</h4>
                                                <div class="form-buttons"></div>
                                            </div>
                                            <div class="fieldset fieldset-wide" id="group_4fieldset_group_4">
                                                <div class="hor-scroll">
                                                    <table cellspacing="0" class="form-list">
                                                        <tbody>
                                                        <tr>
                                                            <td class="hidden" colspan="100">
                                                                <input id="" name="id" value="<?php echo $menuId ?>" type="hidden"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="label"><label for="group_4name">Name <span class="required">*</span></label></td>
                                                            <td class="value">
                                                                <input id="group_4name" name="title_menu" value="<?php
                                                                    if($dataCurrentMenu!=null){
                                                                        echo $dataCurrentMenu->gettitle_menu();
                                                                    }
                                                                ?>" class=" required-entry input-text required-entry" type="text"/>
                                                            </td>
                                                            <td class="scope-label"><span class="nobr">[STORE VIEW]</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="label"><label for="group_4is_active">Is Active <span class="required">*</span></label></td>
                                                            <td class="value">
                                                                <select id="group_4is_active" name="is_active" class=" required-entry required-entry select">
                                                                    <option value="1" <?php
                                                                    if($dataCurrentMenu!=null){
                                                                        $active=$dataCurrentMenu->getis_active();
                                                                        if($active==1){
                                                                            echo 'selected="selected"';
                                                                        }
                                                                    }
                                                                    ?>>Yes</option>
                                                                    <option value="0" <?php
                                                                    if($dataCurrentMenu!=null){
                                                                        $active=$dataCurrentMenu->getis_active();
                                                                        if($active==0){
                                                                            echo 'selected="selected"';
                                                                        }
                                                                    }
                                                                    ?> >No</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="label"><label for="group_4thumbnail"></label></td>
                                                            <td class="value"><img class="img-thumbnail" src="<?php  if($dataCurrentMenu!=null){
                                                                    echo $dataCurrentMenu->getimage();
                                                                } ?>" width="150px" height="150px"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="label"><label for="group_4thumbnail">Thumbnail Image</label></td>
                                                            <td class="value">
                                                                <input id="group_4thumbnail" name="image" value="" class="input-file" type="file"/>
                                                            </td>
                                                            <td class="scope-label"><span class="nobr">[STORE VIEW]</span></td>
                                                        </tr>

                                                        <tr>
                                                            <td class="label"><label for="group_4description">Description</label></td>
                                                            <td class="value">
                                                                <textarea id="group_4description" name="description" class="textarea"><?php
                                                                    if($dataCurrentMenu!=null) {
                                                                        echo $dataCurrentMenu->getdescription();
                                                                    }
                                                                    ?>
                                                                </textarea>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            </form>
                            <script type="text/javascript">
                                category_info_tabsJsTabs = new varienTabs('category_info_tabs', 'category_tab_content', 'category_info_tabs_group_4', []);
                            </script>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php include_once ("footer.php")?>
</div>
<div id="loading-mask" style="display:none">
    <p class="loader" id="loading_mask_loader"><img src="skin/adminhtml/default/default/images/ajax-loader-tr.gif" alt="Loading..."/><br/>Please wait...</p>
</div>


</body>
</html>
