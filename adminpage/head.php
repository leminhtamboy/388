<script type="text/javascript">
    var BLANK_URL = 'js/blank.html';
    var BLANK_IMG = 'js/spacer.gif';
    var BASE_URL = 'index.php/admin/index/index/key/2ce038fa2c1e518b72a54c81f291e0c8/';
    var SKIN_URL = 'skin/adminhtml/default/default/';
    var FORM_KEY = 'ORnV1BV2LTII3FMC';
</script>

<link rel="stylesheet" type="text/css" href="js/calendar/calendar-win2k-1.css" />
<link rel="stylesheet" type="text/css" href="js/extjs/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="js/extjs/resources/css/ytheme-magento.css" />
<link rel="stylesheet" type="text/css" href="js/prototype/windows/themes/default.css" />
<link rel="stylesheet" type="text/css" href="skin/adminhtml/default/default/reset.css" media="all" />
<link rel="stylesheet" type="text/css" href="skin/adminhtml/default/default/boxes.css" media="all" />
<link rel="stylesheet" type="text/css" href="skin/adminhtml/default/default/custom.css" media="all" />
<link rel="stylesheet" type="text/css" href="skin/adminhtml/default/default/css/tinybox/style.css" media="all" />
<link rel="stylesheet" type="text/css" href="skin/adminhtml/default/default/css/magestore/magenotification.css" media="all" />
<link rel="stylesheet" type="text/css" href="skin/adminhtml/default/default/lib/prototype/windows/themes/magento.css" media="all" />
<link rel="stylesheet" type="text/css" href="skin/adminhtml/default/default/print.css" media="print" />
<link rel="stylesheet" type="text/css" href="skin/adminhtml/default/default/menu.css" media="screen, projection" />
<script type="text/javascript" src="js/prototype/prototype.js"></script>
<script type="text/javascript" src="js/extjs/fix-defer-before.js"></script>
<script type="text/javascript" src="js/prototype/window.js"></script>
<script type="text/javascript" src="js/scriptaculous/builder.js"></script>
<script type="text/javascript" src="js/scriptaculous/effects.js"></script>
<script type="text/javascript" src="js/scriptaculous/dragdrop.js"></script>
<script type="text/javascript" src="js/scriptaculous/controls.js"></script>
<script type="text/javascript" src="js/scriptaculous/slider.js"></script>
<script type="text/javascript" src="js/lib/ccard.js"></script>
<script type="text/javascript" src="js/prototype/validation.js"></script>
<script type="text/javascript" src="js/varien/js.js"></script>
<script type="text/javascript" src="js/mage/translate.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/hash.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/events.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/loader.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/grid.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/tabs.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/form.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/accordion.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/tools.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/uploader.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/product.js"></script>
<script type="text/javascript" src="js/calendar/calendar.js"></script>
<script type="text/javascript" src="js/calendar/calendar-setup.js"></script>
<script type="text/javascript" src="js/extjs/ext-tree.js"></script>
<script type="text/javascript" src="js/extjs/fix-defer.js"></script>
<script type="text/javascript" src="js/extjs/ext-tree-checkbox.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/wysiwyg/tiny_mce/setup.js"></script>
<script type="text/javascript" src="js/tinybox/tinybox.js"></script>
<script type="text/javascript" src="js/magestore/promotionalgift/jscolor/jscolor.js"></script>
<script type="text/javascript" src="js/qmobile/adminCustom.js"></script>
<script type="text/javascript" src="js/lib/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/lib/jquery/noconflict.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/variables.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/wysiwyg/widget.js"></script>
<script type="text/javascript" src="js/lib/flex.js"></script>
<script type="text/javascript" src="js/lib/FABridge.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/flexuploader.js"></script>
<script type="text/javascript" src="js/mage/adminhtml/browser.js"></script>
<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="skin/adminhtml/default/default/iestyles.css" media="all" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="skin/adminhtml/default/default/below_ie7.css" media="all" />
<script type="text/javascript" src="js/lib/ds-sleight.js" defer></script>
<script type="text/javascript" src="js/varien/iehover-fix.js"></script>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="skin/adminhtml/default/default/ie7.css" media="all" />
<![endif]-->

<script type="text/javascript">
    Ext.BLANK_IMAGE_URL = BLANK_IMG;
    Ext.UpdateManager.defaults.loadScripts = false;
    Ext.UpdateManager.defaults.disableCaching = true;
</script>

<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
    Fieldset.addToPrefix(6);
</script>

<script type="text/javascript">//<![CDATA[
    var Translator = new Translate([]);
    //]]></script>

<script type="text/javascript">
    //<![CDATA[
    enUS = {"m":{"wide":["January","February","March","April","May","June","July","August","September","October","November","December"],"abbr":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]}}; // en_US locale reference
    Calendar._DN = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]; // full day names
    Calendar._SDN = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]; // short day names
    Calendar._FD = 0; // First day of the week. "0" means display Sunday first, "1" means display Monday first, etc.
    Calendar._MN = ["January","February","March","April","May","June","July","August","September","October","November","December"]; // full month names
    Calendar._SMN = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]; // short month names
    Calendar._am = "AM"; // am/pm
    Calendar._pm = "PM";

    // tooltips
    Calendar._TT = {};
    Calendar._TT["INFO"] = "About the calendar";

    Calendar._TT["ABOUT"] =
        "DHTML Date/Time Selector\n" +
        "(c) dynarch.com 2002-2005 / Author: Mihai Bazon\n" +
        "For latest version visit: http://www.dynarch.com/projects/calendar/\n" +
        "Distributed under GNU LGPL. See http://gnu.org/licenses/lgpl.html for details." +
        "\n\n" +
        "Date selection:\n" +
        "- Use the \xab, \xbb buttons to select year\n" +
        "- Use the \u2039, \u203a buttons to select month\n" +
        "- Hold mouse button on any of the above buttons for faster selection.";
    Calendar._TT["ABOUT_TIME"] = "\n\n" +
        "Time selection:\n" +
        "- Click on any of the time parts to increase it\n" +
        "- or Shift-click to decrease it\n" +
        "- or click and drag for faster selection.";

    Calendar._TT["PREV_YEAR"] = "Prev. year (hold for menu)";
    Calendar._TT["PREV_MONTH"] = "Prev. month (hold for menu)";
    Calendar._TT["GO_TODAY"] = "Go Today";
    Calendar._TT["NEXT_MONTH"] = "Next month (hold for menu)";
    Calendar._TT["NEXT_YEAR"] = "Next year (hold for menu)";
    Calendar._TT["SEL_DATE"] = "Select date";
    Calendar._TT["DRAG_TO_MOVE"] = "Drag to move";
    Calendar._TT["PART_TODAY"] = ' (' + "today" + ')';

    // the following is to inform that "%s" is to be the first day of week
    Calendar._TT["DAY_FIRST"] = "Display %s first";

    // This may be locale-dependent. It specifies the week-end days, as an array
    // of comma-separated numbers. The numbers are from 0 to 6: 0 means Sunday, 1
    // means Monday, etc.
    Calendar._TT["WEEKEND"] = "0,6";

    Calendar._TT["CLOSE"] = "Close";
    Calendar._TT["TODAY"] = "today";
    Calendar._TT["TIME_PART"] = "(Shift-)Click or drag to change value";

    // date formats
    Calendar._TT["DEF_DATE_FORMAT"] = "%b %e, %Y";
    Calendar._TT["TT_DATE_FORMAT"] = "%B %e, %Y";

    Calendar._TT["WK"] = "Week";
    Calendar._TT["TIME"] = "Time:";

    CalendarDateObject._LOCAL_TIMZEONE_OFFSET_SECONDS = -25200;
    CalendarDateObject._SERVER_TIMZEONE_SECONDS = 1467272098;

    //]]>
</script>