<?php include("../Article.php") ?>
<?php include("../Attribute.php") ?>
<?php include("../AttributeValue.php") ?>
<?php include("../Category.php") ?>
<?php include_once("../Uploader.php") ?>
<?php include_once("../Message.php") ?>
<?php
$article = new Article ("article","id_article");
$attribute= new Attribute("entity_attribute","attribute_id");
// nếu cài nào là visible thì ẩn đi
$dataAttribute=$attribute->getCustomCollectionAttributeBySql("visible","eq",0);
$category = new Category("category","id");
$dataCategory=$category->getCollection();
//$dataValueAttribute
if(isset($_REQUEST["action"])){
    $action=$_REQUEST["action"];
    switch ($action){
        case "save":
            $uploader=new UploadDer();
            $uploader->_nameTagFile="main_image";
            $uploader->_mainFoler="imagesArticle";
            $urlImage=$uploader->uploadAction();
            $article->setData("id_article","NULL");
            $article->setData("title",$_POST["title"]);
            $article->setData("image",$urlImage);
            $article->setData("has_category",$_POST["category"]);
            $article->setData("is_active",$_POST["is_active"]);
            $lastInsertId=$article->inserRow();
            //insert attribute
            foreach($dataAttribute as $insertAttribute){
                $idAttribute=$insertAttribute->getattribute_id();
                $valueAttribute= new AttributeValue("entity_attribute_value_varchar","id_eav_vachar");
                $valueAttribute->setData("entity_id",$lastInsertId);
                $valueAttribute->setData("attribute_id",$idAttribute);
                $valueAttribute->setData("value",$_POST["$idAttribute"]);
                $valueAttribute->inserRow();
            }
            ?>
            <script>
                top.location="managerArticle.php";
            </script>
            <?php
            break;
        case "saveandedit":
            $uploader=new UploadDer();
            $uploader->_nameTagFile="main_image";
            $uploader->_mainFoler="imagesArticle";
            $urlImage=$uploader->uploadAction();
            $article->setData("id_article","NULL");
            $article->setData("title",$_POST["title"]);
            $article->setData("image",$urlImage);
            $article->setData("has_category",$_POST["category"]);
            $article->setData("is_active",$_POST["is_active"]);
            $lastInsertId=$article->inserRow();
            //insert attribute
            foreach($dataAttribute as $insertAttribute){
                $idAttribute=$insertAttribute->getattribute_id();
                $valueAttribute= new AttributeValue("entity_attribute_value_varchar","id_eav_vachar");
                $valueAttribute->setData("entity_id",$lastInsertId);
                $valueAttribute->setData("attribute_id",$idAttribute);
                $valueAttribute->setData("value",$_POST["$idAttribute"]);
                $valueAttribute->inserRow();
            }
            ?>
                <script>
                top.location="actionEditArticle.php?id_article=<?php echo $lastInsertId ?>";
                </script>
            <?php
            break;
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Quản Lý Bài Viết</title>
    <link rel="icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="skin/adminhtml/default/default/favicon.ico" type="image/x-icon"/>
    <?php include_once("head.php"); ?>
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>

<body id="html-body" class=" adminhtml-catalog-category-edit">
<div class="wrapper">
    <noscript>
        <div class="noscript">
            <div class="noscript-inner">
                <p><strong>JavaScript seems to be disabled in your browser.</strong></p>
                <p>You must have JavaScript enabled in your browser to utilize the functionality of this website.</p>
            </div>
        </div>
    </noscript>
    <?php include_once("header.php"); ?>
    <div class="notification-global">
    <span class="f-right">
                You have <span class="critical"><strong>13</strong> critical</span>, <strong>6</strong> major, <strong>19</strong> minor and <strong>199</strong> notice unread message(s). <a href="index.php/admin/notification/index/key/cc95c7f8bb0de047610eee786f6488ac/">Go to messages inbox</a>
    </span>
        <strong class="label">

            Latest Message:</strong> Increase your sales and productivity, while simplifying PCI compliance with exciting new Magento Community Edition 2.1 features.            <a href="https://magento.com/blog/magento-news/magento-enterprise-edition-21-unleashes-power-marketers-and-merchandisers " onclick="this.target='_blank';">Read details</a>
    </div>
    <div class="middle" id="anchor-content">
        <div id="page:main-container">
            <div class="columns ">
                <div class="side-col" id="page:left">
                    <h3>Thông Tin Bài Viết</h3>
                    <ul id="product_attribute_tabs" class="tabs">
                        <li>
                            <a href="#" id="product_attribute_tabs_main" name="main" title="Properties" class="tab-item-link active">
                                <span><span class="changed" title="The information in this tab has been changed."></span><span class="error" title="This tab contains invalid data. Please solve the problem before saving."></span>Thuộc Tính</span>
                            </a>

                        </li>
                        <li>
                            <a href="#" id="product_attribute_tabs_labels" name="labels" title="Manage Label / Options" class="tab-item-link">
                                <span><span class="changed" title="The information in this tab has been changed."></span><span class="error" title="This tab contains invalid data. Please solve the problem before saving."></span>Manage Label / Options</span>
                            </a>

                        </li>
                    </ul>
                    <script type="text/javascript">
                        product_attribute_tabsJsTabs = new varienTabs('product_attribute_tabs', 'edit_form', 'product_attribute_tabs_main', []);
                    </script>
                </div>
                <div class="main-col" id="content">
                    <div class="main-col-inner">
                        <div id="messages"></div>
                        <div class="content-header">
                            <h3 class="icon-head head-catalog-product-attribute">Bài Viết Mới</h3><p class="form-buttons"><button id="id_43c6ff26b4ae60115f4bc8e337082bb7" title="Back" type="button" class="scalable back" onclick="setLocation('managerArticle.php')" style=""><span><span><span>Trở Về</span></span></span></button>
                                <button id="id_e5c186eecf838a0b85ff73d67e63fb48" title="Reset" type="button" class="scalable " onclick="setLocation(window.location.href)" style=""><span><span><span>Reset</span></span></span></button>
                                <button id="id_e800e9310ad1d475a18282b6ac631da4" title="Save Attribute" type="button" class="scalable save" onclick="jQuery('#edit_form').attr('action','?action=save');jQuery('#edit_form').submit();" style=""><span><span><span>Lưu Bài Viết</span></span></span></button>
                                <button id="id_3a1b3db86044db7eded90050179df858" title="Save and Continue Edit" type="button" class="scalable save" onclick="jQuery('#edit_form').attr('action','?action=saveandedit');jQuery('#edit_form').submit();" style=""><span><span><span>Lưu và Chỉnh Sửa</span></span></span></button></p>
                        </div>
                        <div class="entry-edit">
                            <form id="edit_form" action="#" method="post" enctype="multipart/form-data"><div><input name="form_key" type="hidden" value="4W1kYElT1QSaUb5N"></div><div id="product_attribute_tabs_main_content"><div class="entry-edit">
                                        <div class="entry-edit-head">
                                            <h4 class="icon-head head-edit-form fieldset-legend">Thuộc Tính Bài Viết</h4>
                                            <div class="form-buttons"></div>
                                        </div>
                                        <div class="fieldset " id="base_fieldset">
                                            <div class="hor-scroll">
                                                <table cellspacing="0" class="form-list">
                                                    <tbody>
                                                    <tr>
                                                        <td class="label"><label for="attribute_code">Tiêu Đề<span class="required">*</span></label></td>
                                                        <td class="value">
                                                            <input id="title" name="title" value="" title="Tiêu Đề" class="validate-code validate-length maximum-length-30 input-text required-entry" type="text">
                                                            <!--<p class="note" id="note_attribute_code"><span>For internal use. Must be unique with no spaces. Maximum length of attribute code must be less then 30 symbols</span></p>-->
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="" width="90%" height="auto" />
                                                            <p class="note" id="note_attribute_code"><span>Hình Upload lên theo chuẩn 800x455 là tốt nhất.</span></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="label"><label for="attribute_code">Hình Đại Diện<span class="required">*</span></label></td>
                                                        <td class="value">
                                                            <input id="main_image" name="main_image" value="" title="Hình Đại Diện" class="validate-code validate-length maximum-length-30 input-text required-entry" type="file">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="label"><label for="category">Danh Mục</label></td>
                                                        <td class="value">
                                                            <select id="" name="category" title="Category" class=" select">
                                                                <?php  foreach ($dataCategory as $cate) { ?>
                                                                    <option value="<?php echo $cate->getid(); ?>"><?php echo $cate->gettitle(); ?></option>
                                                                <?php } ?>
                                                            </select>
                                                            <p class="note" id="note_is_global"><span>Danh Mục Chứa Các Bài Viết.</span></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="label"><label for="frontend_input">Đang Kích Hoạt</label></td>
                                                        <td class="value">
                                                            <select id="is_active" name="is_active" title="Đang Kích Hoạt" class=" select">
                                                                <option value="1">Có</option>
                                                                <option value="0">Không</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <?php foreach($dataAttribute as $attr){ ?>
                                                        <?php $idAttribute=$attr->getattribute_id(); ?>
                                                        <?php $nameAttribute=$attr->getattribute_name(); ?>
                                                        <?php $disable=$attr->getreadonly(); ?>
                                                        <?php $default_value=$attr->getdefault_value(); ?>
                                                        <?php $note=$attr->getnote(); ?>
                                                        <?php
                                                            $showDefaultValue="";
                                                            switch ($default_value){
                                                                case "date_time";
                                                                    date_default_timezone_set("Asia/Ho_Chi_Minh");
                                                                    $showDefaultValue=date("d/m/Y H:m:i");
                                                                    break;
                                                            }
                                                        ?>
                                                        <?php
                                                        $isDisable="";
                                                        if($disable==1){
                                                            $isDisable="readonly";
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td class="label"><label for="<?php echo $attr->getattribute_code(); ?>"><?php echo $nameAttribute; ?></label></td>
                                                            <?php //TO DO FOR STRING ?>
                                                            <td class="value" style="width:100%">
                                                                <?php $codeBackend=$attr->getattribute_backend(); ?>
                                                                <?php switch($codeBackend){
                                                                    case "textarea":
                                                                        ?>
                                                                        <textarea id="<?php echo $attr->getattribute_code(); ?>" name="<?php echo $idAttribute; ?>" value="" title="<?php echo $nameAttribute; ?>" class="<?php echo $attr->getattribute_backend(); ?>"></textarea>
                                                                        <p class="note" id="note_is_global"><span><?php echo $note ?></span></p>
                                                                        <script type="text/javascript">
                                                                            if(CKEDITOR.instances["<?php echo $attr->getattribute_code(); ?>"]){

                                                                            }else{
                                                                                CKEDITOR.replace("<?php echo $attr->getattribute_code(); ?>");
                                                                            }
                                                                        </script>
                                                                        <?php
                                                                        break;
                                                                    case "text":
                                                                        ?>
                                                                        <input id="<?php echo $attr->getattribute_code(); ?>" name="<?php echo $idAttribute; ?>" value="<?php echo $showDefaultValue ?>" title="<?php echo $nameAttribute; ?>" type="<?php echo $attr->getattribute_backend(); ?>" class="input-<?php echo $attr->getattribute_backend(); ?>" <?php echo $isDisable ?>>
                                                                        <p class="note" id="note_is_global"><span><?php echo $note ?></span></p>
                                                                        <?php
                                                                        break;
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript"> new FormElementDependenceController({"is_wysiwyg_enabled":{"frontend_input":"textarea"},"is_html_allowed_on_front":{"is_wysiwyg_enabled":"0"}}); </script></div><div id="product_attribute_tabs_labels_content" style="display: none;"><div>
                                        <ul class="messages">
                                            <li class="notice-msg">
                                                <ul>
                                                    <li>If you do not specify an option value for a specific store view then the default (Admin) value will be used.</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <script type="text/javascript">
                            editForm = new varienForm('edit_form', 'http://www.ma_1_9_2_data.com/index.php/admin/catalog_product_attribute/validate/key/4628a010bd18fdc5845ca980a97ddb2d/');
                        </script>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php include_once ("footer.php")?>
</div>
<div id="loading-mask" style="display:none">
    <p class="loader" id="loading_mask_loader"><img src="skin/adminhtml/default/default/images/ajax-loader-tr.gif" alt="Loading..."/><br/>Please wait...</p>
</div>


</body>
</html>
