-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 16, 2018 lúc 11:27 AM
-- Phiên bản máy phục vụ: 10.1.29-MariaDB
-- Phiên bản PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `388`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `article`
--

CREATE TABLE `article` (
  `id_article` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `has_category` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `thumbail` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `article`
--

INSERT INTO `article` (`id_article`, `title`, `image`, `has_category`, `is_active`, `thumbail`) VALUES
(6, 'cccccccccc', 'http://www.dev.bep.com/image/imagesArticle/product_screen_empty.png', 2, 0, ''),
(7, 'Bài Viết Test 3', 'http://www.dev.bep.com/image/imagesArticle/metro_green_angular_low_res.jpg', 1, 0, ''),
(8, 'aaaaaaa', 'http://www.dev.bep.com/image/imagesArticle/may-giat-aqua-s70kt-h-06.jpg', 2, 0, ''),
(9, 'Làm Bạn Với Màu Nước (Khóa học ngắn khai giảng 5/7)', 'http://www.dev.bep.com/image/imagesArticle/test.jpg', 2, 0, '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bang_do`
--

CREATE TABLE `bang_do` (
  `id_do` int(11) NOT NULL,
  `hien_thi_do` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gia_do` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `hoa_hong` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ghi_chu` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `has_id_menu` int(11) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `title`, `image`, `link`, `has_id_menu`, `is_active`) VALUES
(1, 'Danh Mục 1', '', 'danh-muc-1', 0, 1),
(2, 'bbbbbb', '', 'bbbbb', 0, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `config`
--

CREATE TABLE `config` (
  `config_id` int(11) NOT NULL,
  `config_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `notes` varchar(100) CHARACTER SET utf16 COLLATE utf16_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `config`
--

INSERT INTO `config` (`config_id`, `config_name`, `value`, `notes`) VALUES
(1, 'logo_home', '<p>logo_2.png</p>\r\n', 'header'),
(2, 'sologan', 'Bếp Chia Sẻ Nơi Mọi Người San Sẻ Yêu Thương.', 'header'),
(3, 'main_clause', 'Chúng tôi đã trở lại trường Tân Châu 2, Di Linh để khảo sát trước khi lắp đặt hệ thống nước sạch nhằm giảm học phí cho em trong năm học mới. Chúng tôi đã đến thăm các hộ nghèo xung quanh khu vực trường học, hoàn cảnh của gia đình các em vô cùng khó khăn đông con và một vài gia đình cận huyết. Gần như 20 phần nhu yếu phẩm chỉ giúp một phần nào cho các hộ dân. Chúng tôi mong rằng lần sau khi khánh thành hệ thống nước chúng tôi có thể giúp được nhiều hơn cho các em.\nGần cuối năm tỉ lệ các em học sinh dân tộc tại trường Tiểu Học Tân Châu 2, Huyện Di Linh, Tình Lâm Đồng bỏ học rất cao vì gia đình đưa các em đi trông rẫy cafe hoặc các em không có tiền đóng tiền nước uống, vệ sinh nên ngại với chúng bạn. Các thầy cô không dám nhắc dù nguồn quỹ năm nào cũng thâm hụt vì sợ các em bị bố mẹ cho thôi học. Chúng tôi mong muốn giới thiệu đến những mạnh thường quân về hoàn cảnh của các em nhỏ để mọi người đến với thành phố Đà Lạt có thể ghé qua giúp đỡ.', 'left_content'),
(4, 'background_color', 'background-color:rgb(122,162,65);', 'header_color'),
(5, 'background_image_category', 'http://www.dev.bep.com/image/category-banner.jpg', 'middle_content_category'),
(6, 'text_search', 'What article would you like to search ?', 'search_box'),
(7, 'left_introduce_title', 'ĐỘC LẬP<br />\r\nSÁNG TẠO<br />\r\nĐỒNG CẢM', 'left_introduce_title'),
(8, 'facebook', '#facebook', 'social_facebook'),
(9, 'youtube', '#youtube', 'social_youtube'),
(10, 'instagram', '#instagram', 'social_instagram'),
(11, '404_error_page', 'Page Was Removed By Admin . Please Contant Admin or view on footer to get more information.', '404_error_page'),
(13, 'sadsadas', '<p>dasdasdasd</p>\r\n', 'asdasdasdasdsd');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `entity`
--

CREATE TABLE `entity` (
  `id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `entity_name` varchar(100) CHARACTER SET ucs2 COLLATE ucs2_unicode_ci NOT NULL,
  `notes` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `entity_attribute`
--

CREATE TABLE `entity_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `attribute_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `attribute_frontend` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `attribute_backend` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `visible` int(11) NOT NULL,
  `position` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `show_on_grid` int(11) NOT NULL,
  `readonly` int(11) NOT NULL,
  `default_value` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `entity_attribute`
--

INSERT INTO `entity_attribute` (`attribute_id`, `attribute_name`, `attribute_code`, `attribute_frontend`, `attribute_backend`, `visible`, `position`, `show_on_grid`, `readonly`, `default_value`, `note`) VALUES
(1, 'Short Description', 'short_description', 'text', 'textarea', 0, 'content-1', 1, 0, '', ''),
(2, 'Long Description', 'long_description', 'text', 'textarea', 0, 'content-2', 1, 0, '', ''),
(3, 'Author', 'author', 'text', 'text', 0, 'footer-left-1', 1, 0, '', ''),
(4, 'Day Edit', 'day_edit', 'varchar', 'text', 0, 'header-1', 1, 1, 'date_time', 'Hiển thị ngày mặc định theo giờ GMT +7 của Việt Nam'),
(5, 'Is Feature', 'is_feature', 'varchar', 'text', 0, 'none', 0, 0, '', 'Thuộc Tính Bài Viết Nổi Bật'),
(6, 'Is New', 'is_new', 'varchar', 'text', 0, 'none', 0, 0, '', 'Thuộc Tính Bài Viết Mới Đăng'),
(7, 'Link SEO', 'link_seo', 'varchar', 'text', 0, 'none', 0, 0, '', 'Link SEO CHO BAI VIẾT'),
(8, 'Lượt Xem', 'luot_xem', 'varchar', 'text', 0, 'none', 0, 0, '', 'Lượt Xem Của Bài Viết');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `entity_attributeset`
--

CREATE TABLE `entity_attributeset` (
  `attributeset_id` int(11) NOT NULL,
  `attributeset_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `entity_attributeset`
--

INSERT INTO `entity_attributeset` (`attributeset_id`, `attributeset_name`, `notes`) VALUES
(1, 'Default', 'Mặc định của từng bài viết '),
(2, 'Article With Practice', 'Dạng bài viết có chương trình thực tập');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `entity_attributeset_attribute`
--

CREATE TABLE `entity_attributeset_attribute` (
  `id_attributeset_attribute` int(11) NOT NULL,
  `attributeset_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `entity_attributeset_attribute`
--

INSERT INTO `entity_attributeset_attribute` (`id_attributeset_attribute`, `attributeset_id`, `attribute_id`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `entity_attribute_value_varchar`
--

CREATE TABLE `entity_attribute_value_varchar` (
  `id_eav_varchar` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `origin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `entity_attribute_value_varchar`
--

INSERT INTO `entity_attribute_value_varchar` (`id_eav_varchar`, `entity_id`, `attribute_id`, `value`, `origin`) VALUES
(3, 6, 1, '<p>fkjshdfkshfkdshfksdhfksdh</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Phương thức thanh to&aacute;n</strong></p>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Vietcombank</td>\r\n			<td>918273981273972139</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Techcombak</td>\r\n			<td>324239874923874</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Dong &Aacute; Bank</td>\r\n			<td>12312321323</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><em><strong>Lời Ngỏ</strong></em></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 0),
(4, 6, 2, '<p>H&atilde;y đến với workshop Ng&agrave;y Nắng Ng&agrave;y Mưa để tự tay l&agrave;m một m&oacute;n đồ chơi của ri&ecirc;ng m&ugrave;a mưa n&agrave;y nh&eacute;</p>\r\n', 0),
(5, 6, 3, '2222222222222222222', 0),
(6, 6, 4, '05/07/2016 20:07:29', 0),
(7, 7, 1, '<p>H&atilde;y đến với workshop Ng&agrave;y Nắng Ng&agrave;y Mưa để tự tay l&agrave;m một m&oacute;n đồ chơi của ri&ecirc;ng m&ugrave;a mưa n&agrave;y nh&eacute;</p>\r\n', 0),
(8, 7, 2, '<pre>\r\nH&atilde;y đến với workshop Ng&agrave;y Nắng Ng&agrave;y Mưa để tự tay l&agrave;m một m&oacute;n đồ chơi của ri&ecirc;ng m&ugrave;a mưa n&agrave;y nh&eacute;.</pre>\r\n', 0),
(9, 7, 3, '1231231 ', 0),
(10, 7, 4, '05/07/2016 18:07:14', 0),
(11, 8, 1, '<p>asdasdasdasd</p>\r\n', 0),
(12, 8, 2, '<p>asdasdasd</p>\r\n', 0),
(13, 8, 3, '123123', 0),
(14, 8, 4, '05/07/2016 18:07:14', 0),
(15, 9, 1, '<h3>Giảng vi&ecirc;n của kh&oacute;a học</h3>\r\n\r\n<p><img class=\"img-responsive instructor-img\" src=\"http://toatau.com/wp-content/uploads/2016/06/12243288_10204472897937170_8640673562266066659_n-640x640.jpg\" /> <strong>Th&ugrave;y An</strong></p>\r\n\r\n<p><strong>Thường gọi l&agrave; Chim Xanh. C&ocirc; đ&atilde; theo học ng&agrave;nh Thiết Kế Thời Trang trước khi trở th&agrave;nh họa sĩ vẽ tranh minh họa v&agrave; giảng vi&ecirc;n lớp Kh&aacute;m Ph&aacute; H&igrave;nh Ảnh tại Toa T&agrave;u. Muốn biết c&ocirc; Chim Xanh đ&atilde; lấy l&ograve;ng trẻ nhỏ bằng tranh m&agrave;u nước như thế n&agrave;o, mời bạn xem tranh c&ocirc; đ&atilde; vẽ cho t&aacute;c phẩm L&agrave;ng quan họ của nh&agrave; xuất bản Kim Đồng tại www.behance.net/gallery/31346305/Lang-quan-h</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Thời gian</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table class=\"no-border table table-condensed\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Ng&agrave;y bắt đầu</td>\r\n			<td>05/07/2016</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ng&agrave;y kết th&uacute;c</td>\r\n			<td>28/07/2016</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Thời lượng</td>\r\n			<td>8 buổi</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Thời gian</td>\r\n			<td>\r\n			<p>Từ 9:00 &ndash; 11:00</p>\r\n\r\n			<p>S&aacute;ng thứ 3 v&agrave; s&aacute;ng thứ 5</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h3>Địa điểm</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Toa T&agrave;u</p>\r\n\r\n<p>632 Điện Bi&ecirc;n Phủ, B&igrave;nh Thạnh</p>\r\n\r\n<p>Hotline: 0917-961-071</p>\r\n\r\n<p>Email: info@toatau.com</p>\r\n\r\n<p>&nbsp;</p>\r\n', 0),
(16, 9, 2, '<p>Vẽ m&agrave;u nước l&agrave; một m&oacute;n &ldquo;kh&oacute; nhằn&rdquo; đ&ograve;i hỏi việc ki&ecirc;n nhẫn luyện tập. Nhưng đ&acirc;y lại l&agrave; m&ocirc;n nghệ thuật mang đến niềm vui v&agrave; hứng th&uacute; trong từng ph&uacute;t từng gi&acirc;y. Kể từ l&uacute;c bắt đầu tập l&agrave;m quen với c&aacute;c loại cọ, canh chỉnh lượng nước để pha m&agrave;u như &yacute;, đến l&uacute;c hồi hộp đi những nh&aacute;t cọ đầu ti&ecirc;n tr&ecirc;n giấy. V&agrave; nh&igrave;n thấy điều k&igrave; diệu diễn ra trước mắt. M&agrave;u nước trong tay con trẻ lại c&agrave;ng k&igrave; diệu hơn nữa, v&igrave; c&aacute;c em d&aacute;m vẽ hồn nhi&ecirc;n, vẽ tự do, vẽ &nbsp;kh&ocirc;ng sợ sai. M&agrave; trong việc vẽ m&agrave;u nước, mỗi thử nghiệm, mỗi lần sai, đều l&agrave; một lần tạo n&ecirc;n những điều bất ngờ.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Lớp học được hướng dẫn bởi c&ocirc; gi&aacute;o Chim Xanh &ndash; người say m&ecirc; v&agrave; kh&ocirc;ng ngừng t&igrave;m t&ograve;i nghệ thuật vẽ m&agrave;u nước, cũng l&agrave; họa sĩ vẽ tranh m&agrave;u nước cho nhiều quyển s&aacute;ch tranh được trẻ con y&ecirc;u mến. C&ocirc; nhắn nhủ rằng: &nbsp;&ldquo;Chỉ t&aacute;m buổi học cho m&ocirc;n n&agrave;y l&agrave; rất ngắn ngủi, kết quả cuối c&ugrave;ng phụ thuộc rất nhiều v&agrave;o sự cố gắng của tr&ograve;, nhưng c&ocirc; Chim đảm bảo c&aacute;c em chắc chắn sẽ c&oacute; được kĩ năng căn bản để c&oacute; thể vẽ được bức tranh m&agrave;u nước đ&uacute;ng nghĩa, v&agrave; hơn nữa c&oacute; thể minh họa lại c&acirc;u chuyện m&igrave;nh muốn kể bằng tranh m&agrave;u nước.&rdquo;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em><strong>Kh&oacute;a học ngắn</strong>&nbsp;của Toa T&agrave;u Trẻ Em l&agrave; kh&oacute;a học tập trung v&agrave;o một loại h&igrave;nh nghệ thuật hoặc chủ đề cụ thể. Kh&oacute;a học thi&ecirc;n về dạy kiến thức v&agrave; kỹ năng cơ bản để gi&uacute;p b&eacute; hiểu hơn v&agrave; dễ d&agrave;ng tiếp cận với m&ocirc;n nghệ thuật đ&oacute; hơn. Suốt thời gian học, b&eacute; sẽ trải qua qu&aacute; tr&igrave;nh nghi&ecirc;n cứu ,thực h&agrave;nh kiến thức v&agrave; thử nghiệm &yacute; tưởng để từng bước ho&agrave;n th&agrave;nh &lsquo;dự &aacute;n nhỏ&rsquo; của m&igrave;nh. Hiện nay, Toa T&agrave;u Trẻ Em c&oacute; 3 lớp cho kh&oacute;a học ngắn:&nbsp;<em>Xưởng Hoạt H&igrave;nh T&iacute; Hon, Vẽ M&agrave;u Nước, Vẽ C&ugrave;ng Hoạ Sĩ.</em></em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Ai n&ecirc;n học?</h3>\r\n\r\n<p>C&aacute;c b&eacute; từ 9 -14 tuổi, y&ecirc;u th&iacute;ch hoặc muốn thử sức với việc vẽ m&agrave;u nước hoặc cảm thấy th&iacute;ch th&iacute;ch n&ecirc;n đi học.</p>\r\n\r\n<h3>Học được g&igrave;?</h3>\r\n\r\n<p>&ndash; Kiến thức v&agrave; kĩ thuật cơ bản về việc sử dụng m&agrave;u nước.</p>\r\n\r\n<p>&ndash; Biết c&aacute;ch sử dụng cọ v&agrave; m&agrave;u để tạo n&ecirc;n c&aacute;c loại hiệu ứng kh&aacute;c nhau</p>\r\n\r\n<p>&ndash; Kết hợp m&agrave;u nước tr&ecirc;n c&aacute;c loại chất liệu kh&aacute;c nhau</p>\r\n\r\n<p>&ndash; Bố cục trong tranh m&agrave;u nước</p>\r\n\r\n<p>&ndash; Ph&aacute;t triển mỹ cảm &ndash; sự nhạy cảm với c&aacute;i đẹp</p>\r\n\r\n<p>&ndash; R&egrave;n luyện t&iacute;nh ki&ecirc;n nhẫn</p>\r\n\r\n<h3>Học thế n&agrave;o?</h3>\r\n\r\n<p>Buổi 1: L&agrave;m quen với m&agrave;u nước</p>\r\n\r\n<p>Buổi 2: C&aacute;ch pha m&agrave;u v&agrave; c&aacute;c hiệu ứng m&agrave;u</p>\r\n\r\n<p>Buổi 3: Hướng dẫn kĩ thuật &ldquo;wet on wet&rdquo; v&agrave; gặp gỡ họa sĩ kh&aacute;ch mời</p>\r\n\r\n<p>Buổi 4: Bố cục tranh v&agrave; m&agrave;u sắc</p>\r\n\r\n<p>Buổi 5:&nbsp; Hiệu ứng m&agrave;u v&agrave; c&aacute;ch ứng dụng v&agrave;o tranh</p>\r\n\r\n<p>Buổi 6: Palette v&agrave; Layer m&agrave;u nước</p>\r\n\r\n<p>Buổi 7 : Ứng dụng của m&agrave;u nước trong vẽ minh họa v&agrave; tr&ograve; chuyện với họa sĩ minh họa</p>\r\n\r\n<p>Buổi 8: Tổng kết</p>\r\n\r\n<h3>Học vi&ecirc;n n&oacute;i g&igrave;?</h3>\r\n\r\n<div class=\"testimonials\">&nbsp;</div>\r\n', 0),
(17, 9, 3, '123123', 0),
(18, 9, 4, '05/07/2016 19:07:58', 0),
(19, 9, 5, '1', 0),
(20, 8, 5, '1', 0),
(21, 7, 5, '1', 0),
(22, 6, 5, '0', 0),
(23, 6, 6, '1', 0),
(24, 8, 6, '1', 0),
(25, 8, 7, 'bai-viet-8', 0),
(26, 6, 7, 'bai-viet-6', 0),
(27, 7, 6, '', 0),
(28, 7, 7, 'bai-viet-7', 0),
(29, 9, 6, '', 0),
(30, 9, 7, 've-mau-nuoc-khoa-hoc-ngan-khai-giang', 0),
(31, 9, 8, '100', 0),
(32, 6, 8, '', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `giao_dich`
--

CREATE TABLE `giao_dich` (
  `id` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `ngay_gio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `loai_phieu` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ngan_hang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `so_tien` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `trang_thai` int(11) NOT NULL,
  `ghi_chu` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `giao_dich`
--

INSERT INTO `giao_dich` (`id`, `id_member`, `ngay_gio`, `loai_phieu`, `ngan_hang`, `so_tien`, `trang_thai`, `ghi_chu`) VALUES
(8, 0, '01/16/2018 08:58:19 AM', 'nap', 'Ngân Lượng', '100000', 0, ''),
(9, 16, '01/16/2018 09:00:03 AM', 'nap', 'Ngân Lượng', '100000', 0, '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `giao_dich_tai_khoan`
--

CREATE TABLE `giao_dich_tai_khoan` (
  `id_giao_dich_tai_khoan` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `id_tai_khoan` int(11) NOT NULL,
  `loai_giao_dich` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tinh_trang` int(11) NOT NULL,
  `so_tien` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `so_diem` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `sdt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mat_khau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `so_tien` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `member`
--

INSERT INTO `member` (`id`, `sdt`, `email`, `mat_khau`, `so_tien`) VALUES
(16, '01696317074', 'leminhtamboy@gmail.com', 'e852b422f0d565f166172445d8e3e58b', '800000');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `title_menu` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu`
--

INSERT INTO `menu` (`id`, `title_menu`, `image`, `description`, `is_active`) VALUES
(1, 'Home Page', 'http://www.dev.bep.com/image/imagesMenu/home.png', 'http://www.dev.bep.com/                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ', 1),
(16, 'Article', 'http://www.dev.bep.com/image/imagesMenu/download_s.png', 'category.html                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ', 1),
(17, 'Đóng góp', 'http://www.dev.bep.com/image/imagesMenu/questionmark.png', 'dong-gop.html                                                                          ', 1),
(18, 'About Us', 'http://www.dev.bep.com/image/imagesMenu/Picture of me 2.png', 'about-us.html', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `page`
--

CREATE TABLE `page` (
  `id_page` int(11) NOT NULL,
  `identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `metakeyword` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `page`
--

INSERT INTO `page` (`id_page`, `identifier`, `title`, `content`, `status`, `metakeyword`, `meta_description`) VALUES
(1, 'about-us', 'About Us', '<p>asdasdasd</p>\r\n', 1, 'aaaaa', 'bbbbbb'),
(2, 'dong-gop', 'Đóng Góp', '<p>Test Đ&oacute;ng G&oacute;p</p>\r\n', 0, '', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tai_khoan_ca_cuoc`
--

CREATE TABLE `tai_khoan_ca_cuoc` (
  `id_tai_khoan` int(11) NOT NULL,
  `ma_tai_khoan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ten_tai_khoan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_do` int(11) NOT NULL,
  `so_diem` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id_article`);

--
-- Chỉ mục cho bảng `bang_do`
--
ALTER TABLE `bang_do`
  ADD PRIMARY KEY (`id_do`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`config_id`);

--
-- Chỉ mục cho bảng `entity`
--
ALTER TABLE `entity`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `entity_attribute`
--
ALTER TABLE `entity_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Chỉ mục cho bảng `entity_attributeset`
--
ALTER TABLE `entity_attributeset`
  ADD PRIMARY KEY (`attributeset_id`);

--
-- Chỉ mục cho bảng `entity_attributeset_attribute`
--
ALTER TABLE `entity_attributeset_attribute`
  ADD PRIMARY KEY (`id_attributeset_attribute`);

--
-- Chỉ mục cho bảng `entity_attribute_value_varchar`
--
ALTER TABLE `entity_attribute_value_varchar`
  ADD PRIMARY KEY (`id_eav_varchar`);

--
-- Chỉ mục cho bảng `giao_dich`
--
ALTER TABLE `giao_dich`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `giao_dich_tai_khoan`
--
ALTER TABLE `giao_dich_tai_khoan`
  ADD PRIMARY KEY (`id_giao_dich_tai_khoan`);

--
-- Chỉ mục cho bảng `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id_page`);

--
-- Chỉ mục cho bảng `tai_khoan_ca_cuoc`
--
ALTER TABLE `tai_khoan_ca_cuoc`
  ADD PRIMARY KEY (`id_tai_khoan`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `article`
--
ALTER TABLE `article`
  MODIFY `id_article` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `bang_do`
--
ALTER TABLE `bang_do`
  MODIFY `id_do` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `config`
--
ALTER TABLE `config`
  MODIFY `config_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `entity`
--
ALTER TABLE `entity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `entity_attribute`
--
ALTER TABLE `entity_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `entity_attributeset`
--
ALTER TABLE `entity_attributeset`
  MODIFY `attributeset_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `entity_attributeset_attribute`
--
ALTER TABLE `entity_attributeset_attribute`
  MODIFY `id_attributeset_attribute` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `entity_attribute_value_varchar`
--
ALTER TABLE `entity_attribute_value_varchar`
  MODIFY `id_eav_varchar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT cho bảng `giao_dich`
--
ALTER TABLE `giao_dich`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `giao_dich_tai_khoan`
--
ALTER TABLE `giao_dich_tai_khoan`
  MODIFY `id_giao_dich_tai_khoan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT cho bảng `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT cho bảng `page`
--
ALTER TABLE `page`
  MODIFY `id_page` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tai_khoan_ca_cuoc`
--
ALTER TABLE `tai_khoan_ca_cuoc`
  MODIFY `id_tai_khoan` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
