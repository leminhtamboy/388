<?php
$header=array(
    "Accept:application/json,application/xhtml+xml,application/xml;q=0.9,image/webp,*//*;q=0.8",
    "Accept-Encoding:gzip, deflate, sdch,utf-8",
    "Accept-Language:en-US,en;q=0.8,vi;q=0.6,ko;q=0.4,fr;q=0.2",
    "Connection:keep-alive",
    "Upgrade-Insecure-Requests:1",
    "User-Agent:Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36",
);
$page = $_GET["page"];
$url = "https://musescore.com/sheetmusic?instruments=0&page=".$page;
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
$content = curl_exec($ch);
echo $content;