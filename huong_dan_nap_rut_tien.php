<?php
@session_start();
include_once("Article.php");
include_once("Menu.php");
include_once("Attribute.php");
include_once("AttributeValue.php");
include_once("processingImage.php");
include_once("Config.php");
include_once("Giaodich.php");
$config= new ConfigGlobal("config","config_id");
$menu  = new Menu("menu","id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$article = new Article("article","id_article");
$attributeValue= new AttributeValue("entity_attribute_value_varchar","id_eav_varchar");
?>
<!doctype html>
<!--[if lt IE 9]> <html class="no-js ie-old" lang="vi"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="vi"> <![endif]-->
<!--[if gt IE 8]> <html lang="vi"> <!--<![endif]-->
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<?php include_once("head.php"); ?>
<body>
<div class="home-page-edit">
    <?php include_once("header.php"); ?>
    <div class="main-content">
        <div class="container">
            <div id="pSport">
                <style>
                    .left_ct img{width:290px;margin-top:50px;}
                    strong {  font-weight:bold !important;}
                </style>
                <div class="right_ct" style="width: 100%">
                    <div class="breadcrumb-wrap">
                        <ul class="breadcrumb breadcrumb-cus">
                            <li><a href="<?php echo ConfigGlobal::$realPath; ?>">Trang chủ</a></li>
                            <li><a href="<?php echo  ConfigGlobal::$realPath ?>/huong-dan-nap-rut-tien.html">Hướng Dẫn Nạp Rút Tiền</a></li>
                        </ul>
                    </div>
                    <h1 class="content_header title-details">
                        <span class="triangle-left" style="left: -12px;"></span>
                        <span class="txt-title">HƯỚNG DẪN NẠP RÚT TIỀN</span>
                        <span class="triangle-right"></span>
                    </h1>
                    <div class="post-details-ext">
                        <p style="font-weight: bold"><b>HƯỚNG DẪN NẠP TIỀN</b></p>
                        <p><strong>Bước 1</strong>: Đăng nhập tài khoản vào <a href="<?php echo ConfigGlobal::$realPath ?>">https://www.88cuoc.com</a></p>
                        <center>
                            <p><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_1.png" style="height: auto; width: 600px;"></p>
                        </center>
                        <p><strong>Bước 2</strong>: Chọn vào nút Button &nbsp;&nbsp;<img alt="" src="https://img.388bet.com/images/mobi/Button-nap.png" style="height: 38px; width: 40px;">&nbsp;&nbsp;</p>
                        <p><strong>Bước 3</strong>: Sau đó nhấp vào Button Điền Thông Tin <img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_2.png" style="height: 29px; width: 100px;">. Một danh mục mới được hiện&nbsp;ra.</p>
                        <p><strong style="line-height: 1.6em;">Bước 4: </strong><span style="line-height: 1.6em;">&nbsp;Điền đầy đủ thông tin ở mục Nạp tiền:</span></p>
                        <center>
                            <p><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_3.png"></p>
                        </center>
                        <p><strong>- Điền số tiền để nạp tiền</strong> : Bạn điền chính xác số tiền đã gửi: Ví dụ bạn chuyển 6 triệu thì bạn điền là: 6.000.000.</p>
                        <p><strong style="line-height: 20.8px;">-&nbsp;</strong><strong>Tên người gửi</strong>: Bạn điền chính xác tên của người gửi tiền ( chủ thẻ gửi tiền )</p>
                        <p><strong style="line-height: 20.8px;">-&nbsp;</strong><strong>Chọn tài khoản cá cược</strong>&nbsp;: Bạn chọn hình thức tạo tài khoản cho mình. Nếu là dùng thử bạn chỉ cần điền số tiền nạp là 0 và chọn tài khoản cần dùng thử. Nếu muốn tài khoản thật thì chỉ cần điền số tiền và không cần chọn tài khoản cá cược.</p>
                        <p><strong>Ví dụ:</strong></p>
                        <p>&nbsp;- Bạn muốn tạo tài khoản dùng thử bong88: bạn điền số tiền nạp là 0, Điền tên người gửi, sau đó chọn 1 trong các tài khoản để chơi thử.</p>
                        <p>Sau khi điền đầy đủ thông tin ở trên thì bạn nhấp chọn <img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_4.png" style="height: 50px;">&nbsp; <br/> và vui lòng chờ trong ít phút. Hệ thống sẽ xử lý yêu cầu của bạn và chuyển bạn qua trang nạp tiền để tiến hành chọn:</p>
                            &nbsp;&nbsp;+ Chuyển tiền bằng thẻ tín dụng (Visa-MasterCard).
                            <br/>
                            &nbsp;&nbsp;+ Chuyển tiền online qua thẻ ATM.
                            <br/>
                            &nbsp;&nbsp;+ Chuyển khoản online qua Internet Banking.
                            <br/>
                            &nbsp;&nbsp;+ Nộp tiền mặt qua ATM hoặc tại ngân hàng.
                            <br/>
                        <p></p>
                        <p></p>
                        <center>
                            <p><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_5.png"></p>
                        </center>
                        <p><strong style="line-height: 20.8px;">-&nbsp;</strong><strong>Chọn tài khoản cần chuyển</strong>&nbsp;:
                            <br/>
                            &nbsp;&nbsp;+ Họ và tên: Bạn điền họ và tên bất kì bạn muốn.
                            <br/>
                            &nbsp;&nbsp;+ Địa chỉ Email: Bạn điền địa chỉ email bất kì bạn muốn.
                            <br/>
                            &nbsp;&nbsp;+ Số di động: Bạn điền <span style="font-weight: bold;text-transform: uppercase;">đúng số di động bạn sử dụng</span> để tiện cho việc gửi OTP xác nhận và giao dịch.
                            <br/>
                            Sau khi kiểm tra thông tin đầy đủ như trên hình nhấn tiếp tục hệ thống sẽ chuyển bạn qua trang chủ internet banking của các ngân hàng.</p>
                        <p></p>
                        <p><strong style="line-height: 20.8px;">-&nbsp;</strong>&nbsp;Sau khi chuyển tiền vào tài khoản thành công hệ thống sẽ chuyển bạn về lại trang thông tin thanh toán của 88cuoc.</p>
                        <center>
                            <p><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_6.png" style="width: 700px; height: auto;"></p>
                        </center>
                        <p><strong style="line-height: 20.8px;">-&nbsp;</strong>&nbsp;Cuối cùng hệ thống sẽ cập nhật số tiền của quý khách tại thông tin tài khoản bên trên cùng.</p>
                        <center>
                            <p><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_7.png" style="width: auto; height: auto"></p>
                        </center>
                        <p><strong style="line-height: 20.8px;">-&nbsp;</strong>&nbsp;Quý khách có thể xem giao dịch của mình bằng cách chọn lịch sử giao dịch để xem thông tin.</p>
                        <div style="text-align: center;"><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_8.png" style="width: 1024px;"></div>
                        <p style="text-align: center;">*** *** *** *** *** *** *** *** *** ***&nbsp;</p>
                        <!-- end -->
                        <p style="font-weight: bold"><b>HƯỚNG DẪN NẠP TIỀN TỪ 88CUOC VÀO TÀI KHOẢN CƯỢC HOẶC TẠO TÀI KHOẢN CƯỢC MỚI</b></p>
                        <p> Sau khi nạp tiền vào <strong>88Cuoc</strong> quý khách có lượng tiền trong số dư. Quý khách bắt đầu dùng tiền đó để tạo tài khoản hoặc nạp vào tài khoản cá cược</p>
                        <p><strong>Bước 1</strong>: Click vào menu <a target="_blank" href="#">Tạo yêu cầu</a></p>
                        <center>Menu tạo yêu cầu</center>
                        <center>
                            <p><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_9.png" style="height: auto;"></p>
                        </center>
                        <center>Trang tạo yêu cầu</center>
                        <div style="text-align: center;"><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_10.png" style="width: 700px;"></div>
                        <p><strong>Bước 2</strong>: Chọn vào nút Button &nbsp;&nbsp;<img alt="" src="https://img.388bet.com/images/mobi/Button-nap.png" style="height: 38px; width: 40px;">&nbsp;&nbsp;</p>
                        <p><strong>Bước 3</strong>: Sau đó nhấp vào Button Điền Thông Tin <img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_2.png" style="height: 29px; width: 100px;">. Một danh mục mới được hiện&nbsp;ra.</p>
                        <p><strong style="line-height: 1.6em;">Bước 4: </strong><span style="line-height: 1.6em;">&nbsp;Điền đầy đủ thông tin ở mục Nạp tiền:</span></p>
                        <center>
                            <p><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_11.png"></p>
                        </center>
                        <p><strong>- Điền số tiền để nạp tiền</strong> : Bạn điền chính xác số tiền đã gửi: Ví dụ bạn chuyển 6 triệu thì bạn điền là: 6.000.000.</p>
                        <p><strong style="line-height: 20.8px;">-&nbsp;</strong><strong>Chọn trang tài khoản cá cược</strong>&nbsp;: Ở đây có 2 hình thức nạp tiền:
                            <p></p>
                            &nbsp;&nbsp;&nbsp;(1) Bạn chưa có tài khoản bạn muốn tạo tài khoản mới thì chọn vào ô tài khoản mới.
                            <br/>
                            &nbsp;&nbsp;&nbsp;(2) Bạn đã có tài khoản và bạn muốn nạp thêm vào cho tài khoản đã có thì chọn đã có tài khoản.
                        </p>
                        <p><strong>Trường hợp chưa có tài khoản bạn cần tạo tài khoản mới:</strong></p>
                        <p>&nbsp;- Bạn muốn tạo tài khoản mới bên bong88: Chọn tạo cho tôi tài khoản bong88 mới, sau đó sẽ hiển thị giá đô cho bạn lựa chọn, chọn giá đô bạn muốn</p>
                        <center>
                            <p><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_12.png"></p>
                        </center>
                        <p>Sau khi điền đầy đủ thông tin ở trên thì bạn nhấp chọn <img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_4.png" style="height: 50px;">&nbsp; <br/> và vui lòng chờ trong ít phút. Hệ thống sẽ xử lý yêu cầu nạp của bạn và chúng tôi sẽ kiểm tra, tạo cho bạn tài khoản, sau đó vào mục tài khoản cá cược để xem thông tin tài khoản .</p>
                        <p><strong style="line-height: 20.8px;">-&nbsp;</strong>&nbsp;Bạn có thể xem yêu cầu của mình bằng cách chọn menu lịch sử chuyển tiền để xem thông tin.</p>
                        <div style="text-align: center;"><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_13.png" style="width: 900px; height: 363px;"></div>

                        <p><strong>Trường hợp bạn đã có tài khoản :</strong></p>
                        <p>&nbsp;- Bạn cần nạp tiền vào tài khoản đó: Chọn đã có tài khoản, sau đó sẽ hiển thị danh sách các trang cược có chứa tài khoản của bạn</p>
                        <p style="color: red"><b> Lưu ý :</b> Một phiếu nạp chỉ nạp cho đúng 1 tài khoản và của 1 trang chứ không nạp cùng lúc cho nhiều tài khoản của nhiều trang</p>
                        <center>
                            <p><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_14.png"></p>
                        </center>
                        <center>Danh sách các trang chứa tài khoản</center>
                        <center>
                            <p><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_15.png"></p>
                        </center>
                        <center>Chọn tài khoản bạn cần nạp</center>
                        <p>Sau khi điền đầy đủ thông tin và chọn tài khoản ở trên thì bạn nhấp chọn <img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_4.png" style="height: 50px;">&nbsp; <br/> và vui lòng chờ trong ít phút. Hệ thống sẽ xử lý yêu cầu nạp của bạn và chúng tôi sẽ kiểm tra và nạp tiền vào tài khoản của bạn, sau đó vào mục lịch sử chuyển tiền để xem trạng thái yêu cầu và vào trang cược để kiểm tra tài khoản của bạn được nạp hay chưa .</p>
                        <p><strong style="line-height: 20.8px;">-&nbsp;</strong>&nbsp;Bạn có thể xem yêu cầu của mình bằng cách chọn menu lịch sử chuyển tiền để xem thông tin.</p>
                        <div style="text-align: center;"><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_13.png" style="width: 900px; height: 363px;"></div>
                        <p style="text-align: center;">*** *** *** *** *** *** *** *** *** ***&nbsp;</p>
                        <p><strong>HƯỚNG DẪN RÚT TIỀN</strong></p>
                        <p><strong>Bước 1</strong>: Click vào menu <a href="<?php echo ConfigGlobal::$realPath ?>/tao-yeu-cau.html">Tạo yêu cầu</a></p>
                        <p><strong>Bước 2: </strong>&nbsp;Chọn loại phiếu Rút</p>
                        <center>
                            <p><img alt="phieu rut.png" src="<?php echo  ConfigGlobal::$realPath ?>/image/rut_1.png" style="width: auto; height: auto;"></p>
                        </center>
                        <p><strong>Bước 3</strong>: Sau đó nhấp vào Button Điền Thông Tin <img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/nap_2.png" style="height: 29px; width: 100px;">. Một danh mục mới được hiện&nbsp;ra.</p>
                        <p><strong>Bước 4: </strong>Điền đầy đủ thông tin vào các đề mục.</p>
                        <center>
                            <p><img alt="rut2.png" src="<?php echo  ConfigGlobal::$realPath ?>/image/rut_2.png" style="height: 603px; width: 638px;"></p>
                        </center>
                        <p>- <strong>Chọn ngân hàng: </strong>Click chọn ngân hàng mà bạn muốn rút tiền.</p>
                        <p><span style="line-height: 20.8px;">-&nbsp;</span><strong>Số tiền rút</strong>: Bạn điền đầy đủ số tiền cần rút. Ví dụ bạn rút 5 triệu thì điền vào là 5.000.000</p>
                        <p><span style="line-height: 20.8px;">-&nbsp;</span><strong>Tên người nhận</strong> : Bạn điền đúng tên chủ thẻ nhận tiền.</p>
                        <p><span style="line-height: 20.8px;">-&nbsp;</span><strong>Số tài khoản ngân hàng</strong>: Bạn điền chính xác số tài khoản ngân hàng nhận tiền.</p>
                        <p><strong>- Số điện thoại</strong>: Nếu có.</p>
                        <p><span style="line-height: 20.8px;">-&nbsp;</span><strong>Phương thức rút tiền</strong>: Bạn có thể lựa chọn rút tiền từ 88 cược hoặc , rút từ tài khoản cá cược.</p>
                        <p><strong>Trường hợp bạn rút từ 88 cuoc :</strong></p>
                        <p>Trong trượng hợp này bạn muốn rút trực tiếp từ số dư của bạn chỉ cần chọn Rút tiền từ 88 cược. Sau đó điền nội dung rút tiền. Chúng tôi sẽ sử lý và chuyển tiền cho bạn</p>
                        <center>
                            <p><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/rut_3.png"></p>
                        </center>
                        <p><strong>Trường hợp bạn rút từ tài khoản cược :</strong></p>
                        <p>Trong trượng hợp này bạn chọn rút từ tài khoản cá cược. Hiển thị danh sách các tài khoản cho các trang và bạn lựa chọn. Sau đó chúng tôi sẽ tiến hành rút tiền từ tài khoản bạn chọn và chuyển vào số dư của bạn hoặc là chuyển tiền trực tiếp vào ngân hàng của bạn, tùy thuộc vào nội dung mà bạn ghi</p>
                        <p style="color: red"><b> Lưu ý :</b> Một phiếu rút chỉ rút cho đúng 1 tài khoản và của 1 trang chứ không rút cùng lúc cho nhiều tài khoản của nhiều trang</p>
                        <center>
                            <p><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/rut_4.png"></p>
                        </center>
                        <center> Chọn tài khoản cần rút </center>
                        <center>
                            <p><img alt="" src="<?php echo  ConfigGlobal::$realPath ?>/image/rut_6.png"></p>
                        </center>
                        <center> Điền nội dung rút từ tài khoản </center>
                        <p>Sau khi điền đầy đủ thông tin bạn chọn nút Button <img alt="rút.png" src="<?php echo  ConfigGlobal::$realPath ?>/image/rut_7.png">&nbsp;. Chỉ vài phút sau là tiền đã vào trong tài khoản mà bạn cung cấp.</p>
                        <p><strong>Nếu có gì thắc mắc hoặc gặp trở ngại vui lòng liện hệ với chúng tôi hoặc chat trực tiếp với nhân viên. Chúng tôi sẽ hỗ trợ bạn ngay lập tức <span style="color:#0000CD;">CHAT</span></strong></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function(){
            $(function() {
                var act=0;
                if(window.location.hash) {
                    act =parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs({active: act});
            });
            $(window).on('hashchange', function() {
                var act=0;
                if(window.location.hash) {
                    act = parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs( "option", "active", act );
            });

        });
    </script>
    <?php include_once("footer.php") ?>
</div>
<script type="text/javascript">


    var imgArr=['<?php echo ConfigGlobal::$realPath ?>/image/home_jalan_vi-VN.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/ad_fastmarket.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/banner3_en.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/cock_fight.jpg'];
    var numberSilider =Math.floor((Math.random() * imgArr.length));
    //alert(imgArr);
    var url =imgArr[numberSilider];
    $('#imagesRandom').attr('src',url);

    $(document).ready(function(){
        $('.onNap').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            if(a == 'Nạp'){
                $('.onRut').text('Rút');
                $("#lb-naprut-caption").text("Rút tiền");

            }
            else{
                $('.onRut').text('Nạp');
                $("#lb-naprut-caption").text("Nạp tiền");
            }
            $('.chooseAction').toggle()

        })
        $('.onRut').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            //show_invoice_form();
            if(a == 'Nạp'){
                $(".form-controlCustom").show();
                $('#formChooseBankPull').hide();
                $('#formChooseBankPush').hide();
                $(".phieu-nap-text").hide();
                $(".phieu-rut-text").show();
                $('.onNap').text('Rút');
                $('.chooseAction').hide()
            }else{
                $(".form-controlCustom").show();
                $('#formChooseBankPush').hide();
                $('#formChooseBankPull').hide();
                $(".phieu-nap-text").show();
                $(".phieu-rut-text").hide();
                $('.onNap').text('Nạp');
                $('.chooseAction').hide()
            }

        });
        $('#basic').popup();
        $('#fade').popup();

        $('#phone_fix_dialog').popup();
        $('#bankinfo').popup({
            blur:false,
            beforeopen:function(){
                // $("#invoice-content").html("<p>content here!!!!</p>");

            }
        });



    });
    function show_invoice_form(){
        //$(".form-controlCustom").hide();
//    $(".frm-invoice-panel").show();
        var a = $('.onNap').text();
        if(a == 'Nạp'){
            $('#formChooseBankPull').hide();
            $('#formChooseBankPush').show();
        }else{
            $('#formChooseBankPush').hide ();
            $('#formChooseBankPull').show();
        }
    }
</script>
</body>
</html>
