<?php
@session_start();
include_once("Article.php");
include_once("Menu.php");
include_once("Attribute.php");
include_once("AttributeValue.php");
include_once("processingImage.php");
include_once("Config.php");
include_once("Giaodich.php");
include_once("Giao_Dich_Tai_Khoan.php");
include_once("Tai_Khoan_Ca_Cuoc.php");
include_once("BangDo.php");
$config= new ConfigGlobal("config","config_id");
$menu  = new Menu("menu","id");
$dataMenu=$menu->getCollection();
$logo=$config->loadByAttribute("config_name","eq","logo_home");
$article = new Article("article","id_article");
$attributeValue= new AttributeValue("entity_attribute_value_varchar","id_eav_varchar");
$modelGiaoDich = new GiaoDich("giao_dich","id");
$modelGiaoDichTaiKhoan = new Giao_Dich_Tai_Khoan("giao_dich_tai_khoan","id_giao_dich_tai_khoan");
$modelTaiKhoanCaCuoc = new Tai_Khoan_Ca_Cuoc("tai_khoan_ca_cuoc","id_tai_khoan");
$modelDoTaiKhoan = new BangDo("bang_do","id_do");
$id_member = $_SESSION["member"];
$_SESSION["allow_me"] = "0";
if(!isset($_SESSION["member"])){
    ?>
    <script>
        top.location="/404.html";
    </script>
<?php
}
$dataGiaoDichTaiKhaon = $modelGiaoDichTaiKhoan->loadByAttribute("id_member","eq",$id_member,0);
$dataGiaoDich = $modelGiaoDich->loadByAttribute("id_member","eq",$id_member,0);
$dataTaiKhoanCaCuoc =  $modelTaiKhoanCaCuoc->loadByAttribute("id_member","eq",$id_member,0);
?>
<!doctype html>
<!--[if lt IE 9]> <html class="no-js ie-old" lang="vi"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="vi"> <![endif]-->
<!--[if gt IE 8]> <html lang="vi"> <!--<![endif]-->
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<?php include_once("head.php"); ?>
<body>
<div class="home-page-edit">
    <?php include_once("header.php"); ?>
    <div class="main-content">
        <div class="container">
            <div id="info-account">
                <h3 class='title-page'>Lịch Sử Giao Dịch</h3>
                <ul>
                    <li><a href="#history-1">Lịch sử giao dịch</a></li>
                    <li><a href="#history-2">Lịch sử chuyển tiền </a></li>
                    <li><a href="#history-3">Tài khoản cá cược</a></li>
                </ul>
                <div id="history-1">
                    <table class="table-history-gd">
                        <tr>
                            <td>Ngày giờ </td>
                            <td>Loại phiếu  </td>
                            <td>Ngân hàng  </td>
                            <td>Số tiền</td>
                            <td>Trạng thái</td>
                        </tr>
                        <?php foreach($dataGiaoDich as $_giaodich){ ?>
                        <tr>
                            <?php
                                $ngay_ht = $_giaodich->getngay_gio();
                                $loai_phieu = $_giaodich->getloai_phieu();
                                $textLoaiPhieu = "Nạp Tiền";
                                if($loai_phieu == 'rut'){
                                    $textLoaiPhieu = "Rút Tiền";
                                }
                                $money = $config->filterMoney($_giaodich->getso_tien());
                                $status = $_giaodich->gettrang_thai();
                                $textStatus = "Đang Xử Lý";
                                switch ($status){
                                    case 0:
                                        $textStatus = ConfigGlobal::$_DANG_XU_LY;
                                        break;
                                    case 1:
                                        $textStatus = ConfigGlobal::$_HOAN_THANH;
                                        break;
                                    case 2:
                                        $textStatus = ConfigGlobal::$_TU_CHOI;
                                        break;
                                    case 3:
                                        $textStatus = ConfigGlobal::$_GIAO_DICH_THAT_BAI;
                                        break;
                                }
                            ?>
                            <td><?php echo $ngay_ht; ?></td>
                            <td><?php echo $textLoaiPhieu; ?></td>
                            <td>Ngân Lượng</td>
                            <td class='text-right-account'><?php echo $money; ?>đ</td>
                            <td class="error invoice_status dialog_191995_open"><?php echo $textStatus; ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                    <div id="dialog_191995"  style="max-width:645px;width:645px">
                        <a class="dialog_191995_close closeCustom"></a>
                        <h2 class='dialogContenth2'>Thông tin phiếu Nạp tiền</h2>
                        <div class="dialogContenth2-body">
                            <div class="header-invoice">Tài khoản nạp: Tạo tài khoản Bong88 mới</div><div class="header-invoice">Tên người gửi: TESTER</div><div class="header-invoice">Số tiền: 10000</div><div class="header-invoice">Ngân hàng: VCB</div><div class="header-invoice">Phương thức chuyển: Internet Banking</div><div class="header-invoice">Mã số gửi tiền: 12345456765</div><div style='clear:left;margin: -11px;'>&nbsp;</div><div style="float:left;width:50%;font-size: 15px;"><p style="color:#18A6EA;margin:0 7px;"> Ngày: 08-12-2017 08:26:35</p><p style="color:#5AC57C;margin:0 7px;">Ghi chú: 388BET kiểm tra chưa nhận được tiền. Quý khách vui lòng liên hệ nhân viên hỗ trợ để được tư vấn tham gia dịch vụ. Trân trọng. Điện thoại liên hệ: 0968.33.8888 </p></div><div style='clear:left;margin: -11px;'>&nbsp;</div>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function(){
                            $('#dialog_191995').popup();
                        });
                    </script>
                </div>
                <div id="history-2">
                    <table class="table-history-cc">
                        <tr>
                            <td>Ngày giờ </td>
                            <td>Nội Dung</td>
                            <td>Số tiền</td>
                            <td>Số điểm </td>
                            <td>Loại giao dịch</td>
                            <td>Tình trạng</td>
                        </tr>
                        <?php foreach($dataGiaoDichTaiKhaon as $_giaodichtaikhoan){ ?>
                            <?php
                                $ngay_gio = $_giaodichtaikhoan->getcreated_at();
                                $noi_dung = $_giaodichtaikhoan->getnoi_dung();
                                $so_tien = $_giaodichtaikhoan->getso_tien();
                                $so_diem = $_giaodichtaikhoan->getso_diem();
                                $loai_giao_dich_tai_khoan = $_giaodichtaikhoan->getloai_giao_dich();
                                $tinh_trang = $_giaodichtaikhoan->gettinh_trang();
                                $textStatus = "";
                                switch ($tinh_trang){
                                    case 0:
                                        $textStatus = ConfigGlobal::$_DANG_XU_LY;
                                        break;
                                    case 1:
                                        $textStatus = ConfigGlobal::$_HOAN_THANH;
                                        break;
                                    case 2:
                                        $textStatus = ConfigGlobal::$_TU_CHOI;
                                        break;
                                    case 3:
                                        $textStatus = ConfigGlobal::$_GIAO_DICH_THAT_BAI;
                                        break;
                                }
                                $textHienThiLoai = "Nạp";
                                if($loai_giao_dich_tai_khoan == "rut"){
                                    $textHienThiLoai = "Rút";
                                }
                            ?>
                        <tr>
                            <td><?php echo $ngay_gio ?></td>
                            <td><?php echo $noi_dung ?></td>
                            <td><?php echo $config->filterMoney($so_tien) ?></td>
                            <td><?php echo $so_diem ?></td>
                            <td><?php echo $textHienThiLoai ?></td>
                            <td><?php echo $textStatus ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
                <div id="history-3">
                    <table class="table-account-cc">
                        <tr>
                            <td>Loại tài khoản</td>
                            <td style="min-width: 180px;">Đô / Hoa Hồng</td>
                            <td>Tên tài khoản</td>
                        </tr>
                        <?php foreach($dataTaiKhoanCaCuoc as $_taikhoan){ ?>
                            <?php
                                $loai_tai_khoan = $_taikhoan->getloai_tai_khoan();
                                $id_do = $_taikhoan->getid_do();
                                $hien_thi_do = $modelDoTaiKhoan->load($id_do);
                                $ten_tai_khoan = $_taikhoan->getma_tai_khoan();
                            ?>
                        <tr>
                            <td><?php echo $loai_tai_khoan; ?></td>
                            <td style="min-width: 180px;"><?php echo $hien_thi_do->gethien_thi_do(); ?></td>
                            <td><?php echo $ten_tai_khoan; ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function(){
            $(function() {
                var act=0;
                if(window.location.hash) {
                    act =parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs({active: act});
            });
            $(window).on('hashchange', function() {
                var act=0;
                if(window.location.hash) {
                    act = parseInt(window.location.hash.substring(1))-1; //Puts hash in variable, and removes the # character
                    // hash found
                } else {
                    // No hash found
                }
                $( "#info-account" ).tabs( "option", "active", act );
            });

        });
    </script>
    <?php include_once("footer.php") ?>
</div>
<script type="text/javascript">


    var imgArr=['<?php echo ConfigGlobal::$realPath ?>/image/home_jalan_vi-VN.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/ad_fastmarket.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/banner3_en.jpg',
        '<?php echo ConfigGlobal::$realPath ?>/image/cock_fight.jpg'];
    var numberSilider =Math.floor((Math.random() * imgArr.length));
    //alert(imgArr);
    var url =imgArr[numberSilider];
    $('#imagesRandom').attr('src',url);

    $(document).ready(function(){
        $('.onNap').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            if(a == 'Nạp'){
                $('.onRut').text('Rút');
                $("#lb-naprut-caption").text("Rút tiền");

            }
            else{
                $('.onRut').text('Nạp');
                $("#lb-naprut-caption").text("Nạp tiền");
            }
            $('.chooseAction').toggle()

        })
        $('.onRut').on('click',function(event){
            event.preventDefault();
            var a = $('.onNap').text();
            //show_invoice_form();
            if(a == 'Nạp'){
                $(".form-controlCustom").show();
                $('#formChooseBankPull').hide();
                $('#formChooseBankPush').hide();
                $(".phieu-nap-text").hide();
                $(".phieu-rut-text").show();
                $('.onNap').text('Rút');
                $('.chooseAction').hide()
            }else{
                $(".form-controlCustom").show();
                $('#formChooseBankPush').hide();
                $('#formChooseBankPull').hide();
                $(".phieu-nap-text").show();
                $(".phieu-rut-text").hide();
                $('.onNap').text('Nạp');
                $('.chooseAction').hide()
            }

        });
        $('#basic').popup();
        $('#fade').popup();

        $('#phone_fix_dialog').popup();
        $('#bankinfo').popup({
            blur:false,
            beforeopen:function(){
                // $("#invoice-content").html("<p>content here!!!!</p>");

            }
        });



    });
    function show_invoice_form(){
        //$(".form-controlCustom").hide();
//    $(".frm-invoice-panel").show();
        var a = $('.onNap').text();
        if(a == 'Nạp'){
            $('#formChooseBankPull').hide();
            $('#formChooseBankPush').show();
        }else{
            $('#formChooseBankPush').hide ();
            $('#formChooseBankPull').show();
        }
    }
</script>
</body>
</html>
